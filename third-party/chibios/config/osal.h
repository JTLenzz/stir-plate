/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/**
 * @file    osal.h
 * @brief   OSAL module header.
 *
 * @addtogroup OSAL
 * @{
 */

#ifndef OSAL_H
#define OSAL_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "cmparams.h"
#include "event_groups.h"
#include "semphr.h"
#include "task.h"

/*===========================================================================*/
/* Module constants.                                                         */
/*===========================================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @name    Common constants
 * @{
 */
#if !defined(FALSE) || defined(__DOXYGEN__)
#define FALSE 0
#endif

#if !defined(TRUE) || defined(__DOXYGEN__)
#define TRUE 1
#endif

#define OSAL_SUCCESS false
#define OSAL_FAILED true
/** @} */

/**
 * @name    Messages
 * @{
 */
#define MSG_OK ((msg_t)0)
#define MSG_TIMEOUT ((msg_t)-1)
#define MSG_RESET ((msg_t)-2)
/** @} */

/**
 * @name    Special time constants
 * @{
 */
#define TIME_IMMEDIATE ((sysinterval_t)0)
#define TIME_INFINITE ((sysinterval_t)portMAX_DELAY)
/** @} */

/**
 * @name    Systick modes.
 * @{
 */
#define OSAL_ST_MODE_NONE 0
#define OSAL_ST_MODE_PERIODIC 1
#define OSAL_ST_MODE_FREERUNNING 2
/** @} */

/**
 * @name    Systick parameters.
 * @{
 */
/**
 * @brief   Size in bits of the @p systick_t type.
 */
#if configUSE_16_BIT_TICKS
#define OSAL_ST_RESOLUTION 16
#else
#define OSAL_ST_RESOLUTION 32
#endif

/**
 * @brief   Required systick frequency or resolution.
 */
#define OSAL_ST_FREQUENCY configTICK_RATE_HZ

/**
 * @brief   Systick mode required by the underlying OS.
 */
#define OSAL_ST_MODE OSAL_ST_MODE_NONE
/** @} */

/**
 * @name    IRQ-related constants
 * @{
 */
/**
 * @brief   Total priority levels.
 * @brief   Implementation not mandatory.
 */
#define OSAL_IRQ_PRIORITY_LEVELS (1 << configPRIO_BITS)

/**
 * @brief   Highest IRQ priority for HAL drivers.
 * @brief   Implementation not mandatory.
 */
#define OSAL_IRQ_MAXIMUM_PRIORITY configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY
/** @} */

/*===========================================================================*/
/* Module pre-compile time settings.                                         */
/*===========================================================================*/

/**
 * @brief   Enables OSAL assertions.
 */
#if !defined(OSAL_DBG_ENABLE_ASSERTS) || defined(__DOXYGEN__)
#define OSAL_DBG_ENABLE_ASSERTS FALSE
#endif

/**
 * @brief   Enables OSAL functions parameters checks.
 */
#if !defined(OSAL_DBG_ENABLE_CHECKS) || defined(__DOXYGEN__)
#define OSAL_DBG_ENABLE_CHECKS FALSE
#endif

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

#if !(OSAL_ST_MODE == OSAL_ST_MODE_NONE) && !(OSAL_ST_MODE == OSAL_ST_MODE_PERIODIC) && \
    !(OSAL_ST_MODE == OSAL_ST_MODE_FREERUNNING)
#error "invalid OSAL_ST_MODE setting in osal.h"
#endif

#if (OSAL_ST_RESOLUTION != 16) && (OSAL_ST_RESOLUTION != 32)
#error "invalid OSAL_ST_RESOLUTION, must be 16 or 32"
#endif

/*===========================================================================*/
/* Module data structures and types.                                         */
/*===========================================================================*/

/**
 * @brief   Type of a system status word.
 */
typedef UBaseType_t syssts_t;

/**
 * @brief   Type of a message.
 */
typedef int32_t msg_t;

/**
 * @brief   Type of system time counter.
 */
typedef TickType_t systime_t;

/**
 * @brief   Type of system time interval.
 */
typedef TickType_t sysinterval_t;

/**
 * @brief   Type of realtime counter.
 */
typedef uint32_t rtcnt_t;

/**
 * @brief   Type of a thread reference.
 */
typedef TaskHandle_t thread_reference_t;

/**
 * @brief   Type of an event flags mask.
 */
typedef EventBits_t eventflags_t;

/**
 * @brief   Type of an event flags object.
 * @note    The content of this structure is not part of the API and should
 *          not be relied upon. Implementers may define this structure in
 *          an entirely different way.
 * @note    Retrieval and clearing of the flags are not defined in this
 *          API and are implementation-dependent.
 */
typedef struct {
  EventGroupHandle_t handle;
  StaticEventGroup_t staticData;
} event_source_t;

/**
 * @brief   Type of an event source callback.
 * @note    This type is not part of the OSAL API and is provided
 *          exclusively as an example and for convenience.
 */
typedef void (*eventcallback_t)(event_source_t *esp);

/**
 * @brief   Events source object.
 * @note    The content of this structure is not part of the API and should
 *          not be relied upon. Implementers may define this structure in
 *          an entirely different way.
 * @note    Retrieval and clearing of the flags are not defined in this
 *          API and are implementation-dependent.
 */
struct event_source {
  volatile eventflags_t flags; /**< @brief Stored event flags.         */
  eventcallback_t cb;          /**< @brief Event source callback.      */
  void *param;                 /**< @brief User defined field.         */
};

/**
 * @brief   Type of a mutex.
 * @note    If the OS does not support mutexes or there is no OS then them
 *          mechanism can be simulated.
 */
typedef struct {
  SemaphoreHandle_t handle;
  StaticSemaphore_t staticData;
} mutex_t;

/**
 * @brief   Type of a thread queue.
 * @details A thread queue is a queue of sleeping threads, queued threads
 *          can be dequeued one at time or all together.
 * @note    If the OSAL is implemented on a bare metal machine without RTOS
 *          then the queue can be implemented as a single thread reference.
 */
typedef struct {
  SemaphoreHandle_t handle;
  StaticSemaphore_t staticData;
} threads_queue_t;

/*===========================================================================*/
/* Module macros.                                                            */
/*===========================================================================*/

/**
 * @name    Debug related macros
 * @{
 */
/**
 * @brief   Condition assertion.
 * @details If the condition check fails then the OSAL panics with a
 *          message and halts.
 * @note    The condition is tested only if the @p OSAL_ENABLE_ASSERTIONS
 *          switch is enabled.
 * @note    The remark string is not currently used except for putting a
 *          comment in the code about the assertion.
 *
 * @param[in] c         the condition to be verified to be true
 * @param[in] remark    a remark string
 *
 * @api
 */
#define osalDbgAssert(c, remark) configASSERT(c)

/**
 * @brief   Function parameters check.
 * @details If the condition check fails then the OSAL panics and halts.
 * @note    The condition is tested only if the @p OSAL_ENABLE_CHECKS switch
 *          is enabled.
 *
 * @param[in] c         the condition to be verified to be true
 *
 * @api
 */
#define osalDbgCheck(c) configASSERT(c)

/**
 * @brief   I-Class state check.
 * @note    Implementation is optional.
 */
#define osalDbgCheckClassI()

/**
 * @brief   S-Class state check.
 * @note    Implementation is optional.
 */
#define osalDbgCheckClassS()
/** @} */

/**
 * @name    IRQ service routines wrappers
 * @{
 */
/**
 * @brief   Priority level verification macro.
 */
#define OSAL_IRQ_IS_VALID_PRIORITY(n) \
  (((n) >= OSAL_IRQ_MAXIMUM_PRIORITY) && ((n) < OSAL_IRQ_PRIORITY_LEVELS))

/**
 * @brief   IRQ prologue code.
 * @details This macro must be inserted at the start of all IRQ handlers.
 */
#define OSAL_IRQ_PROLOGUE()                                       \
  extern uint32_t IsrShouldYield[];                               \
  traceISR_ENTER();                                               \
  const uint32_t isrNumber = SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk; \
  IsrShouldYield[isrNumber / 32] &= ~(UINT32_C(1) << (isrNumber % 32))

/**
 * @brief   IRQ epilogue code.
 * @details This macro must be inserted at the end of all IRQ handlers.
 */
#define OSAL_IRQ_EPILOGUE() \
  portYIELD_FROM_ISR((IsrShouldYield[isrNumber / 32] & (1 << (isrNumber % 32))))

/**
 * @brief   IRQ handler function declaration.
 * @details This macro hides the details of an ISR function declaration.
 *
 * @param[in] id        a vector name as defined in @p vectors.s
 */
#define OSAL_IRQ_HANDLER(id) void id(void)
/** @} */

/**
 * @name    Time conversion utilities
 * @{
 */
/**
 * @brief   Seconds to system ticks.
 * @details Converts from seconds to system ticks number.
 * @note    The result is rounded upward to the next tick boundary.
 *
 * @param[in] secs      number of seconds
 * @return              The number of ticks.
 *
 * @api
 */
#define OSAL_S2I(secs) ((sysinterval_t)((uint32_t)(secs) * (uint32_t)OSAL_ST_FREQUENCY))

/**
 * @brief   Milliseconds to system ticks.
 * @details Converts from milliseconds to system ticks number.
 * @note    The result is rounded upward to the next tick boundary.
 *
 * @param[in] msecs     number of milliseconds
 * @return              The number of ticks.
 *
 * @api
 */
#define OSAL_MS2I(msecs) \
  ((sysinterval_t)((((((uint32_t)(msecs)) * ((uint32_t)OSAL_ST_FREQUENCY)) - 1UL) / 1000UL) + 1UL))

/**
 * @brief   Microseconds to system ticks.
 * @details Converts from microseconds to system ticks number.
 * @note    The result is rounded upward to the next tick boundary.
 *
 * @param[in] usecs     number of microseconds
 * @return              The number of ticks.
 *
 * @api
 */
#define OSAL_US2I(usecs) \
  ((sysinterval_t)(      \
      (((((uint32_t)(usecs)) * ((uint32_t)OSAL_ST_FREQUENCY)) - 1UL) / 1000000UL) + 1UL))
/** @} */

/**
 * @name    Time conversion utilities for the realtime counter
 * @{
 */
/**
 * @brief   Seconds to realtime counter.
 * @details Converts from seconds to realtime counter cycles.
 * @note    The macro assumes that @p freq >= @p 1.
 *
 * @param[in] freq      clock frequency, in Hz, of the realtime counter
 * @param[in] sec       number of seconds
 * @return              The number of cycles.
 *
 * @api
 */
#define OSAL_S2RTC(freq, sec) ((freq) * (sec))

/**
 * @brief   Milliseconds to realtime counter.
 * @details Converts from milliseconds to realtime counter cycles.
 * @note    The result is rounded upward to the next millisecond boundary.
 * @note    The macro assumes that @p freq >= @p 1000.
 *
 * @param[in] freq      clock frequency, in Hz, of the realtime counter
 * @param[in] msec      number of milliseconds
 * @return              The number of cycles.
 *
 * @api
 */
#define OSAL_MS2RTC(freq, msec) (rtcnt_t)((((freq) + 999UL) / 1000UL) * (msec))

/**
 * @brief   Microseconds to realtime counter.
 * @details Converts from microseconds to realtime counter cycles.
 * @note    The result is rounded upward to the next microsecond boundary.
 * @note    The macro assumes that @p freq >= @p 1000000.
 *
 * @param[in] freq      clock frequency, in Hz, of the realtime counter
 * @param[in] usec      number of microseconds
 * @return              The number of cycles.
 *
 * @api
 */
#define OSAL_US2RTC(freq, usec) (rtcnt_t)((((freq) + 999999UL) / 1000000UL) * (usec))
/** @} */

/**
 * @name    Sleep macros using absolute time
 * @{
 */
/**
 * @brief   Delays the invoking thread for the specified number of seconds.
 * @note    The specified time is rounded up to a value allowed by the real
 *          system tick clock.
 * @note    The maximum specifiable value is implementation dependent.
 *
 * @param[in] secs      time in seconds, must be different from zero
 *
 * @api
 */
#define osalThreadSleepSeconds(secs) osalThreadSleep(OSAL_S2I(secs))

/**
 * @brief   Delays the invoking thread for the specified number of
 *          milliseconds.
 * @note    The specified time is rounded up to a value allowed by the real
 *          system tick clock.
 * @note    The maximum specifiable value is implementation dependent.
 *
 * @param[in] msecs     time in milliseconds, must be different from zero
 *
 * @api
 */
#define osalThreadSleepMilliseconds(msecs) osalThreadSleep(OSAL_MS2I(msecs))

/**
 * @brief   Delays the invoking thread for the specified number of
 *          microseconds.
 * @note    The specified time is rounded up to a value allowed by the real
 *          system tick clock.
 * @note    The maximum specifiable value is implementation dependent.
 *
 * @param[in] usecs     time in microseconds, must be different from zero
 *
 * @api
 */
#define osalThreadSleepMicroseconds(usecs) osalThreadSleep(OSAL_US2I(usecs))
/** @} */

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

extern const char *osal_halt_msg;
/**
 * @brief Used for storing the yield result for a particular interrupt vector.
 *
 * Since FreeRTOS IRQ APIs return the reschedule state, this allows these values to be stored in a
 * common location allowing it to be accessed inside nested function calls including I-class APIs.
 * There is 1 bit for each interrupt vector. The IRQ epilogue can then instruct the kernel to
 * reschedule as necessary (otherwise we will have to wait until the next tick to run any tasks
 * woken by the interrupt).
 *
 * @todo: Use bit-banding aliases or atomics to avoid potential race conditions in bit accesses. And
 * probably a more user-friendly API.
 */
extern uint32_t IsrShouldYield[(16 + CORTEX_NUM_VECTORS + 31) / 32];

#ifdef __cplusplus
extern "C" {
#endif
static inline void osalInit(void)
{
}

static inline void osalSysHalt(const char *reason)
{
  (void)reason;
  taskDISABLE_INTERRUPTS();
  for (;;) {
  }
}

void osalSysPolledDelayX(rtcnt_t cycles);
void osalOsRescheduleS(void);
static inline systime_t osalOsGetSystemTimeX(void)
{
  return xTaskGetTickCount();
}

static inline void osalThreadSleepS(sysinterval_t time)
{
  TickType_t now = xTaskGetTickCount();
  taskEXIT_CRITICAL();
  vTaskDelayUntil(&now, time);
  taskENTER_CRITICAL();
}

static inline void osalThreadSleep(sysinterval_t time)
{
  vTaskDelay(time);
}

msg_t osalThreadSuspendS(thread_reference_t *trp);
msg_t osalThreadSuspendTimeoutS(thread_reference_t *trp, sysinterval_t timeout);
void osalThreadResumeI(thread_reference_t *trp, msg_t msg);
void osalThreadResumeS(thread_reference_t *trp, msg_t msg);

static inline msg_t osalThreadEnqueueTimeoutS(threads_queue_t *tqp, sysinterval_t timeout)
{
  taskEXIT_CRITICAL();
  if (xSemaphoreTake(tqp->handle, timeout) == pdTRUE) {
    taskENTER_CRITICAL();
    return MSG_OK;
  }
  else {
    taskENTER_CRITICAL();
    return MSG_TIMEOUT;
  }
}

void osalThreadDequeueNextI(threads_queue_t *tqp, msg_t msg);
void osalThreadDequeueAllI(threads_queue_t *tqp, msg_t msg);
void osalEventBroadcastFlagsI(event_source_t *esp, eventflags_t flags);

static inline void osalEventBroadcastFlags(event_source_t *esp, eventflags_t flags)
{
  xEventGroupSetBits(esp->handle, flags);
}

void osalEventSetCallback(event_source_t *esp, eventcallback_t cb, void *param);
static inline void osalMutexLock(mutex_t *mp)
{
  xSemaphoreTake(mp->handle, TIME_INFINITE);
}

static inline void osalMutexUnlock(mutex_t *mp)
{
  xSemaphoreGive(mp->handle);
}

#ifdef __cplusplus
}
#endif

/*===========================================================================*/
/* Module inline functions.                                                  */
/*===========================================================================*/

/**
 * @brief   Disables interrupts globally.
 *
 * @special
 */
static inline void osalSysDisable(void)
{
  vTaskEndScheduler();
}

/**
 * @brief   Enables interrupts globally.
 *
 * @special
 */
static inline void osalSysEnable(void)
{
  vTaskStartScheduler();
}

/**
 * @brief   Enters a critical zone from thread context.
 * @note    This function cannot be used for reentrant critical zones.
 *
 * @special
 */
static inline void osalSysLock(void)
{
  taskENTER_CRITICAL();
}

/**
 * @brief   Leaves a critical zone from thread context.
 * @note    This function cannot be used for reentrant critical zones.
 *
 * @special
 */
static inline void osalSysUnlock(void)
{
  taskEXIT_CRITICAL();
}

/**
 * @brief   Enters a critical zone from ISR context.
 * @note    This function cannot be used for reentrant critical zones.
 *
 * @special
 */
#define osalSysLockFromISR() UBaseType_t uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR()

/**
 * @brief   Leaves a critical zone from ISR context.
 * @note    This function cannot be used for reentrant critical zones.
 *
 * @special
 */
#define osalSysUnlockFromISR() taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus)

/**
 * @brief   Returns the execution status and enters a critical zone.
 * @details This functions enters into a critical zone and can be called
 *          from any context. Because its flexibility it is less efficient
 *          than @p chSysLock() which is preferable when the calling context
 *          is known.
 * @post    The system is in a critical zone.
 *
 * @return              The previous system status, the encoding of this
 *                      status word is architecture-dependent and opaque.
 *
 * @xclass
 */
static inline syssts_t osalSysGetStatusAndLockX(void)
{
  return taskENTER_CRITICAL_FROM_ISR();
}

/**
 * @brief   Restores the specified execution status and leaves a critical zone.
 * @note    A call to @p chSchRescheduleS() is automatically performed
 *          if exiting the critical zone and if not in ISR context.
 *
 * @param[in] sts       the system status to be restored.
 *
 * @xclass
 */
static inline void osalSysRestoreStatusX(syssts_t sts)
{
  taskEXIT_CRITICAL_FROM_ISR(sts);
}

/**
 * @brief   Adds an interval to a system time returning a system time.
 *
 * @param[in] systime   base system time
 * @param[in] interval  interval to be added
 * @return              The new system time.
 *
 * @xclass
 */
static inline systime_t osalTimeAddX(systime_t systime, sysinterval_t interval)
{
  return systime + (systime_t)interval;
}

/**
 * @brief   Subtracts two system times returning an interval.
 *
 * @param[in] start     first system time
 * @param[in] end       second system time
 * @return              The interval representing the time difference.
 *
 * @xclass
 */
static inline sysinterval_t osalTimeDiffX(systime_t start, systime_t end)
{
  return (sysinterval_t)((systime_t)(end - start));
}

/**
 * @brief   Checks if the specified time is within the specified time window.
 * @note    When start==end then the function returns always true because the
 *          whole time range is specified.
 * @note    This function can be called from any context.
 *
 * @param[in] time      the time to be verified
 * @param[in] start     the start of the time window (inclusive)
 * @param[in] end       the end of the time window (non inclusive)
 * @retval true         current time within the specified time window.
 * @retval false        current time not within the specified time window.
 *
 * @xclass
 */
static inline bool osalTimeIsInRangeX(systime_t time, systime_t start, systime_t end)
{
  return (bool)((time - start) < (end - start));
}

/**
 * @brief   Initializes a threads queue object.
 *
 * @param[out] tqp      pointer to the threads queue object
 *
 * @init
 */
static inline void osalThreadQueueObjectInit(threads_queue_t *tqp)
{
  tqp->handle = xSemaphoreCreateCountingStatic(UINT32_MAX, 0, &tqp->staticData);
}

/**
 * @brief   Initializes an event source object.
 *
 * @param[out] esp      pointer to the event source object
 *
 * @init
 */
static inline void osalEventObjectInit(event_source_t *esp)
{
  osalDbgCheck(esp != NULL);

  esp->handle = xEventGroupCreateStatic(&esp->staticData);
}

/**
 * @brief   Initializes s @p mutex_t object.
 *
 * @param[out] mp       pointer to the @p mutex_t object
 *
 * @init
 */
static inline void osalMutexObjectInit(mutex_t *mp)
{
  osalDbgCheck(mp != NULL);

  mp->handle = xSemaphoreCreateMutexStatic(&mp->staticData);
}

#ifdef __cplusplus
}
#endif

#endif /* OSAL_H */

/** @} */
