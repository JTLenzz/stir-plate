#include "osal.h"

#include <limits>

uint32_t IsrShouldYield[(16 + CORTEX_NUM_VECTORS + 31) / 32];

extern "C" msg_t osalThreadSuspendS(thread_reference_t *trp)
{
  return osalThreadSuspendTimeoutS(trp, portMAX_DELAY);
}

extern "C" msg_t osalThreadSuspendTimeoutS(thread_reference_t *trp, sysinterval_t timeout)
{
  if (trp != nullptr) {
    *trp = xTaskGetCurrentTaskHandle();
  }

  osalSysUnlock();
  msg_t message;
  auto result(xTaskNotifyWait(std::numeric_limits<uint32_t>::max(),
      std::numeric_limits<uint32_t>::max(), reinterpret_cast<uint32_t *>(&message), timeout));
  osalSysLock();

  if (result != pdTRUE) {
    if (trp != nullptr) {
      *trp = nullptr;
    }
    message = MSG_TIMEOUT;
  }
  return message;
}

extern "C" void osalThreadResumeI(thread_reference_t *trp, msg_t msg)
{
  const auto isrNumber(SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk);
  BaseType_t shouldYield(pdFALSE);
  if (trp != nullptr) {
    xTaskNotifyFromISR(*trp, msg, eSetValueWithOverwrite, &shouldYield);
    trp = nullptr;
  }
  IsrShouldYield[isrNumber / 32] |= shouldYield << (isrNumber % 32);
}

extern "C" void osalThreadResumeS(thread_reference_t *trp, msg_t msg)
{
  if (trp != nullptr) {
    osalSysUnlock();
    xTaskNotify(*trp, msg, eSetValueWithOverwrite);
    osalSysLock();
    *trp = nullptr;
  }
}

extern "C" void osalThreadDequeueNextI(threads_queue_t *tqp, msg_t /* msg */)
{
  const auto isrNumber(SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk);
  BaseType_t shouldYield(pdFALSE);
  xSemaphoreGiveFromISR(tqp->handle, &shouldYield);
  IsrShouldYield[isrNumber / 32] |= (shouldYield << (isrNumber % 32));
}

extern "C" void osalThreadDequeueAllI(threads_queue_t *tqp, msg_t /* msg */)
{
  const auto isrNumber(SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk);
  BaseType_t shouldYield(pdFALSE);
  for (unsigned i = uxQueueMessagesWaitingFromISR(tqp->handle); i != 0; --i) {
    xSemaphoreGiveFromISR(tqp->handle, &shouldYield);
  }
  IsrShouldYield[isrNumber / 32] |= (shouldYield << (isrNumber % 32));
}

extern "C" void osalEventBroadcastFlagsI(event_source_t *esp, eventflags_t flags)
{
  const auto isrNumber(SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk);
  BaseType_t shouldYield(pdFALSE);
  xEventGroupSetBitsFromISR(esp->handle, flags, &shouldYield);
  IsrShouldYield[isrNumber / 32] |= (shouldYield << (isrNumber % 32));
}