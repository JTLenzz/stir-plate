/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

/*
 * This file has been automatically generated using ChibiStudio board
 * generator plugin. Do not edit manually.
 */

#ifndef BOARD_H
#define BOARD_H

/*===========================================================================*/
/* Driver constants.                                                         */
/*===========================================================================*/

/*
 * Setup for STMicroelectronics STM32 Nucleo32-L432KC board.
 */

/*
 * Board identifier.
 */
#define BOARD_NAME "Stir-plate"

/*
 * Board oscillators-related settings.
 * NOTE: HSE not fitted.
 */
#if !defined(STM32_LSECLK)
#define STM32_LSECLK 32768U
#endif

#define STM32_LSEDRV (2U << 3U)

#if !defined(STM32_HSECLK)
#define STM32_HSECLK 0U
#endif

/*
 * Board voltages.
 * Required for performance limits calculation.
 */
#define STM32_VDD 300U

/*
 * IO pins assignments.
 */
#define GPIOA_DISPLAY_RES 0U
#define GPIOA_FAN_TACH 1U
#define GPIOA_VCP_TX 2U
#define GPIOA_PIN3 3U
#define GPIOA_PIN4 4U
#define GPIOA_PIN5 5U
#define GPIOA_PIN6 6U
#define GPIOA_PIN7 7U
#define GPIOA_DISPLAY_CS 8U
#define GPIOA_FAN_CTRL 9U
#define GPIOA_BUTTON_UP 10U
#define GPIOA_PIN11 11U
#define GPIOA_PIN12 12U
#define GPIOA_SWDIO 13U
#define GPIOA_SWCLK 14U
#define GPIOA_VCP_RX 15U

#define GPIOB_BUTTON_LEFT 0U
#define GPIOB_BUTTON_DOWN 1U
#define GPIOB_PIN2 2U
#define GPIOB_DISPLAY_D0 3U
#define GPIOB_DISPLAY_DC 4U
#define GPIOB_DISPLAY_D1 5U
#define GPIOB_BUTTON_RIGHT 6U   // Net connected to PA6
#define GPIOB_BUTTON_CENTER 7U  // Net connected to PA5
#define GPIOB_PIN8 8U
#define GPIOB_PIN9 9U
#define GPIOB_PIN10 10U
#define GPIOB_PIN11 11U
#define GPIOB_PIN12 12U
#define GPIOB_PIN13 13U
#define GPIOB_PIN14 14U
#define GPIOB_PIN15 15U

#define GPIOC_PIN0 0U
#define GPIOC_PIN1 1U
#define GPIOC_PIN2 2U
#define GPIOC_PIN3 3U
#define GPIOC_PIN4 4U
#define GPIOC_PIN5 5U
#define GPIOC_PIN6 6U
#define GPIOC_PIN7 7U
#define GPIOC_PIN8 8U
#define GPIOC_PIN9 9U
#define GPIOC_PIN10 10U
#define GPIOC_PIN11 11U
#define GPIOC_PIN12 12U
#define GPIOC_PIN13 13U
#define GPIOC_ARD_D7 14U
#define GPIOC_ARD_D8 15U

#define GPIOD_PIN0 0U
#define GPIOD_PIN1 1U
#define GPIOD_PIN2 2U
#define GPIOD_PIN3 3U
#define GPIOD_PIN4 4U
#define GPIOD_PIN5 5U
#define GPIOD_PIN6 6U
#define GPIOD_PIN7 7U
#define GPIOD_PIN8 8U
#define GPIOD_PIN9 9U
#define GPIOD_PIN10 10U
#define GPIOD_PIN11 11U
#define GPIOD_PIN12 12U
#define GPIOD_PIN13 13U
#define GPIOD_PIN14 14U
#define GPIOD_PIN15 15U

#define GPIOE_PIN0 0U
#define GPIOE_PIN1 1U
#define GPIOE_PIN2 2U
#define GPIOE_PIN3 3U
#define GPIOE_PIN4 4U
#define GPIOE_PIN5 5U
#define GPIOE_PIN6 6U
#define GPIOE_PIN7 7U
#define GPIOE_PIN8 8U
#define GPIOE_PIN9 9U
#define GPIOE_PIN10 10U
#define GPIOE_PIN11 11U
#define GPIOE_PIN12 12U
#define GPIOE_PIN13 13U
#define GPIOE_PIN14 14U
#define GPIOE_PIN15 15U

#define GPIOF_PIN0 0U
#define GPIOF_PIN1 1U
#define GPIOF_PIN2 2U
#define GPIOF_PIN3 3U
#define GPIOF_PIN4 4U
#define GPIOF_PIN5 5U
#define GPIOF_PIN6 6U
#define GPIOF_PIN7 7U
#define GPIOF_PIN8 8U
#define GPIOF_PIN9 9U
#define GPIOF_PIN10 10U
#define GPIOF_PIN11 11U
#define GPIOF_PIN12 12U
#define GPIOF_PIN13 13U
#define GPIOF_PIN14 14U
#define GPIOF_PIN15 15U

#define GPIOG_PIN0 0U
#define GPIOG_PIN1 1U
#define GPIOG_PIN2 2U
#define GPIOG_PIN3 3U
#define GPIOG_PIN4 4U
#define GPIOG_PIN5 5U
#define GPIOG_PIN6 6U
#define GPIOG_PIN7 7U
#define GPIOG_PIN8 8U
#define GPIOG_PIN9 9U
#define GPIOG_PIN10 10U
#define GPIOG_PIN11 11U
#define GPIOG_PIN12 12U
#define GPIOG_PIN13 13U
#define GPIOG_PIN14 14U
#define GPIOG_PIN15 15U

#define GPIOH_PIN0 0U
#define GPIOH_PIN1 1U
#define GPIOH_PIN2 2U
#define GPIOH_PIN3 3U
#define GPIOH_PIN4 4U
#define GPIOH_PIN5 5U
#define GPIOH_PIN6 6U
#define GPIOH_PIN7 7U
#define GPIOH_PIN8 8U
#define GPIOH_PIN9 9U
#define GPIOH_PIN10 10U
#define GPIOH_PIN11 11U
#define GPIOH_PIN12 12U
#define GPIOH_PIN13 13U
#define GPIOH_PIN14 14U
#define GPIOH_PIN15 15U

/*===========================================================================*/
/* Driver pre-compile time settings.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Derived constants and error checks.                                       */
/*===========================================================================*/

/*===========================================================================*/
/* Driver data structures and types.                                         */
/*===========================================================================*/

/*===========================================================================*/
/* Driver macros.                                                            */
/*===========================================================================*/

/*
 * I/O ports initial setup, this configuration is established soon after reset
 * in the initialization code.
 * Please refer to the STM32 Reference Manual for details.
 */
#define PIN_MODE_INPUT(n) (0U << ((n)*2U))
#define PIN_MODE_OUTPUT(n) (1U << ((n)*2U))
#define PIN_MODE_ALTERNATE(n) (2U << ((n)*2U))
#define PIN_MODE_ANALOG(n) (3U << ((n)*2U))
#define PIN_ODR_LOW(n) (0U << (n))
#define PIN_ODR_HIGH(n) (1U << (n))
#define PIN_OTYPE_PUSHPULL(n) (0U << (n))
#define PIN_OTYPE_OPENDRAIN(n) (1U << (n))
#define PIN_OSPEED_VERYLOW(n) (0U << ((n)*2U))
#define PIN_OSPEED_LOW(n) (1U << ((n)*2U))
#define PIN_OSPEED_MEDIUM(n) (2U << ((n)*2U))
#define PIN_OSPEED_HIGH(n) (3U << ((n)*2U))
#define PIN_PUPDR_FLOATING(n) (0U << ((n)*2U))
#define PIN_PUPDR_PULLUP(n) (1U << ((n)*2U))
#define PIN_PUPDR_PULLDOWN(n) (2U << ((n)*2U))
#define PIN_AFIO_AF(n, v) ((v) << (((n) % 8U) * 4U))
#define PIN_ASCR_DISABLED(n) (0U << (n))
#define PIN_ASCR_ENABLED(n) (1U << (n))
#define PIN_LOCKR_DISABLED(n) (0U << (n))
#define PIN_LOCKR_ENABLED(n) (1U << (n))

/*
 * GPIOA setup:
 *
 * PA0  - Display Reset (output push-pull).
 * PA1  - Fan Tach      (open drain alternate 1).
 * PA2  - VCP_TX        (alternate 7).
 * PA3  - NC            (analog).
 * PA4  - NC            (analog).
 * PA5  - NC            (analog).
 * PA6  - NC            (analog).
 * PA7  - NC            (analog).
 * PA8  - Display CS    (output push-pull).
 * PA9  - Fan Control   (open-drain alternate 1).
 * PA10 - Button Up     (input pull-up).
 * PA11 - NC            (analog).
 * PA12 - NC            (analog).
 * PA13 - SWDIO         (alternate 0).
 * PA14 - SWCLK         (alternate 0).
 * PA15 - VCP_RX        (alternate 3).
 */
#define VAL_GPIOA_MODER                                                                         \
  (PIN_MODE_OUTPUT(GPIOA_DISPLAY_RES) | PIN_MODE_ALTERNATE(GPIOA_FAN_TACH) |                    \
      PIN_MODE_ALTERNATE(GPIOA_VCP_TX) | PIN_MODE_ANALOG(GPIOA_PIN3) |                          \
      PIN_MODE_ANALOG(GPIOA_PIN4) | PIN_MODE_ANALOG(GPIOA_PIN5) | PIN_MODE_ANALOG(GPIOA_PIN6) | \
      PIN_MODE_ANALOG(GPIOA_PIN7) | PIN_MODE_OUTPUT(GPIOA_DISPLAY_CS) |                         \
      PIN_MODE_ALTERNATE(GPIOA_FAN_CTRL) | PIN_MODE_INPUT(GPIOA_BUTTON_UP) |                    \
      PIN_MODE_ANALOG(GPIOA_PIN11) | PIN_MODE_ANALOG(GPIOA_PIN12) |                             \
      PIN_MODE_ALTERNATE(GPIOA_SWDIO) | PIN_MODE_ALTERNATE(GPIOA_SWCLK) |                       \
      PIN_MODE_ALTERNATE(GPIOA_VCP_RX))
#define VAL_GPIOA_OTYPER                                                           \
  (PIN_OTYPE_PUSHPULL(GPIOA_DISPLAY_RES) | PIN_OTYPE_OPENDRAIN(GPIOA_FAN_TACH) |   \
      PIN_OTYPE_PUSHPULL(GPIOA_VCP_TX) | PIN_OTYPE_PUSHPULL(GPIOA_PIN3) |          \
      PIN_OTYPE_PUSHPULL(GPIOA_PIN4) | PIN_OTYPE_PUSHPULL(GPIOA_PIN5) |            \
      PIN_OTYPE_PUSHPULL(GPIOA_PIN6) | PIN_OTYPE_PUSHPULL(GPIOA_PIN7) |            \
      PIN_OTYPE_PUSHPULL(GPIOA_DISPLAY_CS) | PIN_OTYPE_OPENDRAIN(GPIOA_FAN_CTRL) | \
      PIN_OTYPE_PUSHPULL(GPIOA_BUTTON_UP) | PIN_OTYPE_PUSHPULL(GPIOA_PIN11) |      \
      PIN_OTYPE_PUSHPULL(GPIOA_PIN12) | PIN_OTYPE_PUSHPULL(GPIOA_SWDIO) |          \
      PIN_OTYPE_PUSHPULL(GPIOA_SWCLK) | PIN_OTYPE_PUSHPULL(GPIOA_VCP_RX))
#define VAL_GPIOA_OSPEEDR                                                                         \
  (PIN_OSPEED_HIGH(GPIOA_DISPLAY_RES) | PIN_OSPEED_HIGH(GPIOA_FAN_TACH) |                         \
      PIN_OSPEED_MEDIUM(GPIOA_VCP_TX) | PIN_OSPEED_LOW(GPIOA_PIN3) | PIN_OSPEED_LOW(GPIOA_PIN4) | \
      PIN_OSPEED_LOW(GPIOA_PIN5) | PIN_OSPEED_LOW(GPIOA_PIN6) | PIN_OSPEED_LOW(GPIOA_PIN7) |      \
      PIN_OSPEED_HIGH(GPIOA_DISPLAY_CS) | PIN_OSPEED_HIGH(GPIOA_FAN_CTRL) |                       \
      PIN_OSPEED_LOW(GPIOA_BUTTON_UP) | PIN_OSPEED_HIGH(GPIOA_PIN11) |                            \
      PIN_OSPEED_LOW(GPIOA_PIN12) | PIN_OSPEED_HIGH(GPIOA_SWDIO) | PIN_OSPEED_HIGH(GPIOA_SWCLK) | \
      PIN_OSPEED_LOW(GPIOA_VCP_RX))
#define VAL_GPIOA_PUPDR                                                         \
  (PIN_PUPDR_FLOATING(GPIOA_DISPLAY_RES) | PIN_PUPDR_PULLUP(GPIOA_FAN_TACH) |   \
      PIN_PUPDR_FLOATING(GPIOA_VCP_TX) | PIN_PUPDR_FLOATING(GPIOA_PIN3) |       \
      PIN_PUPDR_FLOATING(GPIOA_PIN4) | PIN_PUPDR_FLOATING(GPIOA_PIN5) |         \
      PIN_PUPDR_FLOATING(GPIOA_PIN6) | PIN_PUPDR_FLOATING(GPIOA_PIN7) |         \
      PIN_PUPDR_FLOATING(GPIOA_DISPLAY_CS) | PIN_PUPDR_PULLUP(GPIOA_FAN_CTRL) | \
      PIN_PUPDR_PULLUP(GPIOA_BUTTON_UP) | PIN_PUPDR_FLOATING(GPIOA_PIN11) |     \
      PIN_PUPDR_FLOATING(GPIOA_PIN12) | PIN_PUPDR_PULLUP(GPIOA_SWDIO) |         \
      PIN_PUPDR_PULLDOWN(GPIOA_SWCLK) | PIN_PUPDR_PULLUP(GPIOA_VCP_RX))
#define VAL_GPIOA_ODR                                                                            \
  (PIN_ODR_HIGH(GPIOA_DISPLAY_RES) | PIN_ODR_HIGH(GPIOA_FAN_TACH) | PIN_ODR_HIGH(GPIOA_VCP_TX) | \
      PIN_ODR_HIGH(GPIOA_PIN3) | PIN_ODR_HIGH(GPIOA_PIN4) | PIN_ODR_HIGH(GPIOA_PIN5) |           \
      PIN_ODR_HIGH(GPIOA_PIN6) | PIN_ODR_HIGH(GPIOA_PIN7) | PIN_ODR_HIGH(GPIOA_DISPLAY_CS) |     \
      PIN_ODR_HIGH(GPIOA_FAN_CTRL) | PIN_ODR_LOW(GPIOA_BUTTON_UP) | PIN_ODR_HIGH(GPIOA_PIN11) |  \
      PIN_ODR_HIGH(GPIOA_PIN12) | PIN_ODR_HIGH(GPIOA_SWDIO) | PIN_ODR_HIGH(GPIOA_SWCLK) |        \
      PIN_ODR_HIGH(GPIOA_VCP_RX))
#define VAL_GPIOA_AFRL                                                                            \
  (PIN_AFIO_AF(GPIOA_DISPLAY_RES, 0U) | PIN_AFIO_AF(GPIOA_FAN_TACH, 1U) |                         \
      PIN_AFIO_AF(GPIOA_VCP_TX, 7U) | PIN_AFIO_AF(GPIOA_PIN3, 0U) | PIN_AFIO_AF(GPIOA_PIN4, 0U) | \
      PIN_AFIO_AF(GPIOA_PIN5, 0U) | PIN_AFIO_AF(GPIOA_PIN6, 0U) | PIN_AFIO_AF(GPIOA_PIN7, 0U))
#define VAL_GPIOA_AFRH                                                                             \
  (PIN_AFIO_AF(GPIOA_DISPLAY_CS, 0U) | PIN_AFIO_AF(GPIOA_FAN_CTRL, 1U) |                           \
      PIN_AFIO_AF(GPIOA_BUTTON_UP, 0U) | PIN_AFIO_AF(GPIOA_PIN11, 0U) |                            \
      PIN_AFIO_AF(GPIOA_PIN12, 0U) | PIN_AFIO_AF(GPIOA_SWDIO, 0U) | PIN_AFIO_AF(GPIOA_SWCLK, 0U) | \
      PIN_AFIO_AF(GPIOA_VCP_RX, 3U))
#define VAL_GPIOA_ASCR                                                          \
  (PIN_ASCR_DISABLED(GPIOA_DISPLAY_RES) | PIN_ASCR_DISABLED(GPIOA_FAN_TACH) |   \
      PIN_ASCR_DISABLED(GPIOA_VCP_TX) | PIN_ASCR_DISABLED(GPIOA_PIN3) |         \
      PIN_ASCR_DISABLED(GPIOA_PIN4) | PIN_ASCR_DISABLED(GPIOA_PIN5) |           \
      PIN_ASCR_DISABLED(GPIOA_PIN6) | PIN_ASCR_DISABLED(GPIOA_PIN7) |           \
      PIN_ASCR_DISABLED(GPIOA_DISPLAY_CS) | PIN_ASCR_DISABLED(GPIOA_FAN_CTRL) | \
      PIN_ASCR_DISABLED(GPIOA_BUTTON_UP) | PIN_ASCR_DISABLED(GPIOA_PIN11) |     \
      PIN_ASCR_DISABLED(GPIOA_PIN12) | PIN_ASCR_DISABLED(GPIOA_SWDIO) |         \
      PIN_ASCR_DISABLED(GPIOA_SWCLK) | PIN_ASCR_DISABLED(GPIOA_VCP_RX))
#define VAL_GPIOA_LOCKR                                                           \
  (PIN_LOCKR_DISABLED(GPIOA_DISPLAY_RES) | PIN_LOCKR_DISABLED(GPIOA_FAN_TACH) |   \
      PIN_LOCKR_DISABLED(GPIOA_VCP_TX) | PIN_LOCKR_DISABLED(GPIOA_PIN3) |         \
      PIN_LOCKR_DISABLED(GPIOA_PIN4) | PIN_LOCKR_DISABLED(GPIOA_PIN5) |           \
      PIN_LOCKR_DISABLED(GPIOA_PIN6) | PIN_LOCKR_DISABLED(GPIOA_PIN7) |           \
      PIN_LOCKR_DISABLED(GPIOA_DISPLAY_CS) | PIN_LOCKR_DISABLED(GPIOA_FAN_CTRL) | \
      PIN_LOCKR_DISABLED(GPIOA_BUTTON_UP) | PIN_LOCKR_DISABLED(GPIOA_PIN11) |     \
      PIN_LOCKR_DISABLED(GPIOA_PIN12) | PIN_LOCKR_DISABLED(GPIOA_SWDIO) |         \
      PIN_LOCKR_DISABLED(GPIOA_SWCLK) | PIN_LOCKR_DISABLED(GPIOA_VCP_RX))

/*
 * GPIOB setup:
 *
 * PB0  - Button Left     (input pull-up).
 * PB1  - Button Down     (input pull-up).
 * PB2  - NC              (analog).
 * PB3  - Display D0/SCLK (output alternate 5).
 * PB4  - Display D/C     (output push-pull).
 * PB5  - Display D1/MOSI (output alternate 5).
 * PB6  - Button Right    (input pull-up).
 * PB7  - Button Center   (input pull-up).
 * PB8  - NC              (analog).
 * PB9  - NC              (analog).
 * PB10 - NC              (analog).
 * PB11 - NC              (analog).
 * PB12 - NC              (analog).
 * PB13 - NC              (analog).
 * PB14 - NC              (analog).
 * PB15 - NC              (analog).
 */
#define VAL_GPIOB_MODER                                                                            \
  (PIN_MODE_INPUT(GPIOB_BUTTON_LEFT) | PIN_MODE_INPUT(GPIOB_BUTTON_DOWN) |                         \
      PIN_MODE_ANALOG(GPIOB_PIN2) | PIN_MODE_ALTERNATE(GPIOB_DISPLAY_D0) |                         \
      PIN_MODE_OUTPUT(GPIOB_DISPLAY_DC) | PIN_MODE_ALTERNATE(GPIOB_DISPLAY_D1) |                   \
      PIN_MODE_INPUT(GPIOB_BUTTON_RIGHT) | PIN_MODE_INPUT(GPIOB_BUTTON_CENTER) |                   \
      PIN_MODE_ANALOG(GPIOB_PIN8) | PIN_MODE_ANALOG(GPIOB_PIN9) | PIN_MODE_ANALOG(GPIOB_PIN10) |   \
      PIN_MODE_ANALOG(GPIOB_PIN11) | PIN_MODE_ANALOG(GPIOB_PIN12) | PIN_MODE_ANALOG(GPIOB_PIN13) | \
      PIN_MODE_ANALOG(GPIOB_PIN14) | PIN_MODE_ANALOG(GPIOB_PIN15))
#define VAL_GPIOB_OTYPER                                                                 \
  (PIN_OTYPE_PUSHPULL(GPIOB_BUTTON_LEFT) | PIN_OTYPE_PUSHPULL(GPIOB_BUTTON_DOWN) |       \
      PIN_OTYPE_PUSHPULL(GPIOB_PIN2) | PIN_OTYPE_PUSHPULL(GPIOB_DISPLAY_D0) |            \
      PIN_OTYPE_PUSHPULL(GPIOB_DISPLAY_DC) | PIN_OTYPE_PUSHPULL(GPIOB_DISPLAY_D1) |      \
      PIN_OTYPE_PUSHPULL(GPIOB_BUTTON_RIGHT) | PIN_OTYPE_PUSHPULL(GPIOB_BUTTON_CENTER) | \
      PIN_OTYPE_PUSHPULL(GPIOB_PIN8) | PIN_OTYPE_PUSHPULL(GPIOB_PIN9) |                  \
      PIN_OTYPE_PUSHPULL(GPIOB_PIN10) | PIN_OTYPE_PUSHPULL(GPIOB_PIN11) |                \
      PIN_OTYPE_PUSHPULL(GPIOB_PIN12) | PIN_OTYPE_PUSHPULL(GPIOB_PIN13) |                \
      PIN_OTYPE_PUSHPULL(GPIOB_PIN14) | PIN_OTYPE_PUSHPULL(GPIOB_PIN15))
#define VAL_GPIOB_OSPEEDR                                                                          \
  (PIN_OSPEED_LOW(GPIOB_BUTTON_LEFT) | PIN_OSPEED_LOW(GPIOB_BUTTON_DOWN) |                         \
      PIN_OSPEED_LOW(GPIOB_PIN2) | PIN_OSPEED_HIGH(GPIOB_DISPLAY_D0) |                             \
      PIN_OSPEED_HIGH(GPIOB_DISPLAY_DC) | PIN_OSPEED_HIGH(GPIOB_DISPLAY_D1) |                      \
      PIN_OSPEED_LOW(GPIOB_BUTTON_RIGHT) | PIN_OSPEED_LOW(GPIOB_BUTTON_CENTER) |                   \
      PIN_OSPEED_HIGH(GPIOB_PIN8) | PIN_OSPEED_HIGH(GPIOB_PIN9) | PIN_OSPEED_HIGH(GPIOB_PIN10) |   \
      PIN_OSPEED_HIGH(GPIOB_PIN11) | PIN_OSPEED_HIGH(GPIOB_PIN12) | PIN_OSPEED_HIGH(GPIOB_PIN13) | \
      PIN_OSPEED_HIGH(GPIOB_PIN14) | PIN_OSPEED_HIGH(GPIOB_PIN15))
#define VAL_GPIOB_PUPDR                                                              \
  (PIN_PUPDR_PULLUP(GPIOB_BUTTON_LEFT) | PIN_PUPDR_PULLUP(GPIOB_BUTTON_DOWN) |       \
      PIN_PUPDR_FLOATING(GPIOB_PIN2) | PIN_PUPDR_FLOATING(GPIOB_DISPLAY_D0) |        \
      PIN_PUPDR_FLOATING(GPIOB_DISPLAY_DC) | PIN_PUPDR_FLOATING(GPIOB_DISPLAY_D1) |  \
      PIN_PUPDR_PULLUP(GPIOB_BUTTON_RIGHT) | PIN_PUPDR_PULLUP(GPIOB_BUTTON_CENTER) | \
      PIN_PUPDR_FLOATING(GPIOB_PIN8) | PIN_PUPDR_FLOATING(GPIOB_PIN9) |              \
      PIN_PUPDR_FLOATING(GPIOB_PIN10) | PIN_PUPDR_FLOATING(GPIOB_PIN11) |            \
      PIN_PUPDR_FLOATING(GPIOB_PIN12) | PIN_PUPDR_FLOATING(GPIOB_PIN13) |            \
      PIN_PUPDR_FLOATING(GPIOB_PIN14) | PIN_PUPDR_FLOATING(GPIOB_PIN15))
#define VAL_GPIOB_ODR                                                                          \
  (PIN_ODR_LOW(GPIOB_BUTTON_LEFT) | PIN_ODR_LOW(GPIOB_BUTTON_DOWN) | PIN_ODR_LOW(GPIOB_PIN2) | \
      PIN_ODR_HIGH(GPIOB_DISPLAY_D0) | PIN_ODR_HIGH(GPIOB_DISPLAY_DC) |                        \
      PIN_ODR_HIGH(GPIOB_DISPLAY_D1) | PIN_ODR_LOW(GPIOB_BUTTON_RIGHT) |                       \
      PIN_ODR_LOW(GPIOB_BUTTON_CENTER) | PIN_ODR_HIGH(GPIOB_PIN8) | PIN_ODR_HIGH(GPIOB_PIN9) | \
      PIN_ODR_HIGH(GPIOB_PIN10) | PIN_ODR_HIGH(GPIOB_PIN11) | PIN_ODR_HIGH(GPIOB_PIN12) |      \
      PIN_ODR_HIGH(GPIOB_PIN13) | PIN_ODR_HIGH(GPIOB_PIN14) | PIN_ODR_HIGH(GPIOB_PIN15))
#define VAL_GPIOB_AFRL                                                        \
  (PIN_AFIO_AF(GPIOB_BUTTON_LEFT, 0U) | PIN_AFIO_AF(GPIOB_BUTTON_DOWN, 0U) |  \
      PIN_AFIO_AF(GPIOB_PIN2, 0U) | PIN_AFIO_AF(GPIOB_DISPLAY_D0, 5U) |       \
      PIN_AFIO_AF(GPIOB_DISPLAY_DC, 0U) | PIN_AFIO_AF(GPIOB_DISPLAY_D1, 5U) | \
      PIN_AFIO_AF(GPIOB_BUTTON_RIGHT, 0U) | PIN_AFIO_AF(GPIOB_BUTTON_CENTER, 0U))
#define VAL_GPIOB_AFRH                                                                             \
  (PIN_AFIO_AF(GPIOB_PIN8, 0U) | PIN_AFIO_AF(GPIOB_PIN9, 0U) | PIN_AFIO_AF(GPIOB_PIN10, 0U) |      \
      PIN_AFIO_AF(GPIOB_PIN11, 0U) | PIN_AFIO_AF(GPIOB_PIN12, 0U) | PIN_AFIO_AF(GPIOB_PIN13, 0U) | \
      PIN_AFIO_AF(GPIOB_PIN14, 0U) | PIN_AFIO_AF(GPIOB_PIN15, 0U))
#define VAL_GPIOB_ASCR                                                                 \
  (PIN_ASCR_DISABLED(GPIOB_BUTTON_LEFT) | PIN_ASCR_DISABLED(GPIOB_BUTTON_DOWN) |       \
      PIN_ASCR_DISABLED(GPIOB_PIN2) | PIN_ASCR_DISABLED(GPIOB_DISPLAY_D0) |            \
      PIN_ASCR_DISABLED(GPIOB_DISPLAY_DC) | PIN_ASCR_DISABLED(GPIOB_DISPLAY_D1) |      \
      PIN_ASCR_DISABLED(GPIOB_BUTTON_RIGHT) | PIN_ASCR_DISABLED(GPIOB_BUTTON_CENTER) | \
      PIN_ASCR_DISABLED(GPIOB_PIN8) | PIN_ASCR_DISABLED(GPIOB_PIN9) |                  \
      PIN_ASCR_DISABLED(GPIOB_PIN10) | PIN_ASCR_DISABLED(GPIOB_PIN11) |                \
      PIN_ASCR_DISABLED(GPIOB_PIN12) | PIN_ASCR_DISABLED(GPIOB_PIN13) |                \
      PIN_ASCR_DISABLED(GPIOB_PIN14) | PIN_ASCR_DISABLED(GPIOB_PIN15))
#define VAL_GPIOB_LOCKR                                                                  \
  (PIN_LOCKR_DISABLED(GPIOB_BUTTON_LEFT) | PIN_LOCKR_DISABLED(GPIOB_BUTTON_DOWN) |       \
      PIN_LOCKR_DISABLED(GPIOB_PIN2) | PIN_LOCKR_DISABLED(GPIOB_DISPLAY_D0) |            \
      PIN_LOCKR_DISABLED(GPIOB_DISPLAY_DC) | PIN_LOCKR_DISABLED(GPIOB_DISPLAY_D1) |      \
      PIN_LOCKR_DISABLED(GPIOB_BUTTON_RIGHT) | PIN_LOCKR_DISABLED(GPIOB_BUTTON_CENTER) | \
      PIN_LOCKR_DISABLED(GPIOB_PIN8) | PIN_LOCKR_DISABLED(GPIOB_PIN9) |                  \
      PIN_LOCKR_DISABLED(GPIOB_PIN10) | PIN_LOCKR_DISABLED(GPIOB_PIN11) |                \
      PIN_LOCKR_DISABLED(GPIOB_PIN12) | PIN_LOCKR_DISABLED(GPIOB_PIN13) |                \
      PIN_LOCKR_DISABLED(GPIOB_PIN14) | PIN_LOCKR_DISABLED(GPIOB_PIN15))

/*
 * GPIOC setup:
 *
 * PC0  - PIN0          (analog).
 * PC1  - PIN1          (analog).
 * PC2  - PIN2          (analog).
 * PC3  - PIN3          (analog).
 * PC4  - PIN4          (analog).
 * PC5  - PIN5          (analog).
 * PC6  - PIN6          (analog).
 * PC7  - PIN7          (analog).
 * PC8  - PIN8          (analog).
 * PC9  - PIN9          (analog).
 * PC10 - PIN10         (analog).
 * PC11 - PIN11         (analog).
 * PC12 - PIN12         (analog).
 * PC13 - PIN13         (analog).
 * PC14 - GPIOC_LSE_IN  (analog).
 * PC15 - GPIOC_LSE_OUT (analog).
 */
#define VAL_GPIOC_MODER                                                                           \
  (PIN_MODE_ANALOG(GPIOC_PIN0) | PIN_MODE_ANALOG(GPIOC_PIN1) | PIN_MODE_ANALOG(GPIOC_PIN2) |      \
      PIN_MODE_ANALOG(GPIOC_PIN3) | PIN_MODE_ANALOG(GPIOC_PIN4) | PIN_MODE_ANALOG(GPIOC_PIN5) |   \
      PIN_MODE_ANALOG(GPIOC_PIN6) | PIN_MODE_ANALOG(GPIOC_PIN7) | PIN_MODE_ANALOG(GPIOC_PIN8) |   \
      PIN_MODE_ANALOG(GPIOC_PIN9) | PIN_MODE_ANALOG(GPIOC_PIN10) | PIN_MODE_ANALOG(GPIOC_PIN11) | \
      PIN_MODE_ANALOG(GPIOC_PIN12) | PIN_MODE_ANALOG(GPIOC_PIN13) |                               \
      PIN_MODE_ANALOG(GPIOC_ARD_D7) | PIN_MODE_ANALOG(GPIOC_ARD_D8))
#define VAL_GPIOC_OTYPER                                                  \
  (PIN_OTYPE_PUSHPULL(GPIOC_PIN0) | PIN_OTYPE_PUSHPULL(GPIOC_PIN1) |      \
      PIN_OTYPE_PUSHPULL(GPIOC_PIN2) | PIN_OTYPE_PUSHPULL(GPIOC_PIN3) |   \
      PIN_OTYPE_PUSHPULL(GPIOC_PIN4) | PIN_OTYPE_PUSHPULL(GPIOC_PIN5) |   \
      PIN_OTYPE_PUSHPULL(GPIOC_PIN6) | PIN_OTYPE_PUSHPULL(GPIOC_PIN7) |   \
      PIN_OTYPE_PUSHPULL(GPIOC_PIN8) | PIN_OTYPE_PUSHPULL(GPIOC_PIN9) |   \
      PIN_OTYPE_PUSHPULL(GPIOC_PIN10) | PIN_OTYPE_PUSHPULL(GPIOC_PIN11) | \
      PIN_OTYPE_PUSHPULL(GPIOC_PIN12) | PIN_OTYPE_PUSHPULL(GPIOC_PIN13) | \
      PIN_OTYPE_PUSHPULL(GPIOC_ARD_D7) | PIN_OTYPE_PUSHPULL(GPIOC_ARD_D8))
#define VAL_GPIOC_OSPEEDR                                                                         \
  (PIN_OSPEED_HIGH(GPIOC_PIN0) | PIN_OSPEED_HIGH(GPIOC_PIN1) | PIN_OSPEED_HIGH(GPIOC_PIN2) |      \
      PIN_OSPEED_HIGH(GPIOC_PIN3) | PIN_OSPEED_HIGH(GPIOC_PIN4) | PIN_OSPEED_HIGH(GPIOC_PIN5) |   \
      PIN_OSPEED_HIGH(GPIOC_PIN6) | PIN_OSPEED_HIGH(GPIOC_PIN7) | PIN_OSPEED_HIGH(GPIOC_PIN8) |   \
      PIN_OSPEED_HIGH(GPIOC_PIN9) | PIN_OSPEED_HIGH(GPIOC_PIN10) | PIN_OSPEED_HIGH(GPIOC_PIN11) | \
      PIN_OSPEED_HIGH(GPIOC_PIN12) | PIN_OSPEED_HIGH(GPIOC_PIN13) |                               \
      PIN_OSPEED_HIGH(GPIOC_ARD_D7) | PIN_OSPEED_HIGH(GPIOC_ARD_D8))
#define VAL_GPIOC_PUPDR                                                   \
  (PIN_PUPDR_FLOATING(GPIOC_PIN0) | PIN_PUPDR_FLOATING(GPIOC_PIN1) |      \
      PIN_PUPDR_FLOATING(GPIOC_PIN2) | PIN_PUPDR_FLOATING(GPIOC_PIN3) |   \
      PIN_PUPDR_FLOATING(GPIOC_PIN4) | PIN_PUPDR_FLOATING(GPIOC_PIN5) |   \
      PIN_PUPDR_FLOATING(GPIOC_PIN6) | PIN_PUPDR_FLOATING(GPIOC_PIN7) |   \
      PIN_PUPDR_FLOATING(GPIOC_PIN8) | PIN_PUPDR_FLOATING(GPIOC_PIN9) |   \
      PIN_PUPDR_FLOATING(GPIOC_PIN10) | PIN_PUPDR_FLOATING(GPIOC_PIN11) | \
      PIN_PUPDR_FLOATING(GPIOC_PIN12) | PIN_PUPDR_FLOATING(GPIOC_PIN13) | \
      PIN_PUPDR_FLOATING(GPIOC_ARD_D7) | PIN_PUPDR_FLOATING(GPIOC_ARD_D8))
#define VAL_GPIOC_ODR                                                                      \
  (PIN_ODR_HIGH(GPIOC_PIN0) | PIN_ODR_HIGH(GPIOC_PIN1) | PIN_ODR_HIGH(GPIOC_PIN2) |        \
      PIN_ODR_HIGH(GPIOC_PIN3) | PIN_ODR_HIGH(GPIOC_PIN4) | PIN_ODR_HIGH(GPIOC_PIN5) |     \
      PIN_ODR_HIGH(GPIOC_PIN6) | PIN_ODR_HIGH(GPIOC_PIN7) | PIN_ODR_HIGH(GPIOC_PIN8) |     \
      PIN_ODR_HIGH(GPIOC_PIN9) | PIN_ODR_HIGH(GPIOC_PIN10) | PIN_ODR_HIGH(GPIOC_PIN11) |   \
      PIN_ODR_HIGH(GPIOC_PIN12) | PIN_ODR_HIGH(GPIOC_PIN13) | PIN_ODR_HIGH(GPIOC_ARD_D7) | \
      PIN_ODR_HIGH(GPIOC_ARD_D8))
#define VAL_GPIOC_AFRL                                                                          \
  (PIN_AFIO_AF(GPIOC_PIN0, 0U) | PIN_AFIO_AF(GPIOC_PIN1, 0U) | PIN_AFIO_AF(GPIOC_PIN2, 0U) |    \
      PIN_AFIO_AF(GPIOC_PIN3, 0U) | PIN_AFIO_AF(GPIOC_PIN4, 0U) | PIN_AFIO_AF(GPIOC_PIN5, 0U) | \
      PIN_AFIO_AF(GPIOC_PIN6, 0U) | PIN_AFIO_AF(GPIOC_PIN7, 0U))
#define VAL_GPIOC_AFRH                                                                             \
  (PIN_AFIO_AF(GPIOC_PIN8, 0U) | PIN_AFIO_AF(GPIOC_PIN9, 0U) | PIN_AFIO_AF(GPIOC_PIN10, 0U) |      \
      PIN_AFIO_AF(GPIOC_PIN11, 0U) | PIN_AFIO_AF(GPIOC_PIN12, 0U) | PIN_AFIO_AF(GPIOC_PIN13, 0U) | \
      PIN_AFIO_AF(GPIOC_ARD_D7, 0U) | PIN_AFIO_AF(GPIOC_ARD_D8, 0U))
#define VAL_GPIOC_ASCR                                                                             \
  (PIN_ASCR_DISABLED(GPIOC_PIN0) | PIN_ASCR_DISABLED(GPIOC_PIN1) | PIN_ASCR_DISABLED(GPIOC_PIN2) | \
      PIN_ASCR_DISABLED(GPIOC_PIN3) | PIN_ASCR_DISABLED(GPIOC_PIN4) |                              \
      PIN_ASCR_DISABLED(GPIOC_PIN5) | PIN_ASCR_DISABLED(GPIOC_PIN6) |                              \
      PIN_ASCR_DISABLED(GPIOC_PIN7) | PIN_ASCR_DISABLED(GPIOC_PIN8) |                              \
      PIN_ASCR_DISABLED(GPIOC_PIN9) | PIN_ASCR_DISABLED(GPIOC_PIN10) |                             \
      PIN_ASCR_DISABLED(GPIOC_PIN11) | PIN_ASCR_DISABLED(GPIOC_PIN12) |                            \
      PIN_ASCR_DISABLED(GPIOC_PIN13) | PIN_ASCR_DISABLED(GPIOC_ARD_D7) |                           \
      PIN_ASCR_DISABLED(GPIOC_ARD_D8))
#define VAL_GPIOC_LOCKR                                                   \
  (PIN_LOCKR_DISABLED(GPIOC_PIN0) | PIN_LOCKR_DISABLED(GPIOC_PIN1) |      \
      PIN_LOCKR_DISABLED(GPIOC_PIN2) | PIN_LOCKR_DISABLED(GPIOC_PIN3) |   \
      PIN_LOCKR_DISABLED(GPIOC_PIN4) | PIN_LOCKR_DISABLED(GPIOC_PIN5) |   \
      PIN_LOCKR_DISABLED(GPIOC_PIN6) | PIN_LOCKR_DISABLED(GPIOC_PIN7) |   \
      PIN_LOCKR_DISABLED(GPIOC_PIN8) | PIN_LOCKR_DISABLED(GPIOC_PIN9) |   \
      PIN_LOCKR_DISABLED(GPIOC_PIN10) | PIN_LOCKR_DISABLED(GPIOC_PIN11) | \
      PIN_LOCKR_DISABLED(GPIOC_PIN12) | PIN_LOCKR_DISABLED(GPIOC_PIN13) | \
      PIN_LOCKR_DISABLED(GPIOC_ARD_D7) | PIN_LOCKR_DISABLED(GPIOC_ARD_D8))

/*
 * GPIOD setup:
 *
 * PD0  - PIN0                      (analog).
 * PD1  - PIN1                      (analog).
 * PD2  - PIN2                      (analog).
 * PD3  - PIN3                      (analog).
 * PD4  - PIN4                      (analog).
 * PD5  - PIN5                      (analog).
 * PD6  - PIN6                      (analog).
 * PD7  - PIN7                      (analog).
 * PD8  - PIN8                      (analog).
 * PD9  - PIN9                      (analog).
 * PD10 - PIN10                     (analog).
 * PD11 - PIN11                     (analog).
 * PD12 - PIN12                     (analog).
 * PD13 - PIN13                     (analog).
 * PD14 - PIN14                     (analog).
 * PD15 - PIN15                     (analog).
 */
#define VAL_GPIOD_MODER                                                                            \
  (PIN_MODE_ANALOG(GPIOD_PIN0) | PIN_MODE_ANALOG(GPIOD_PIN1) | PIN_MODE_ANALOG(GPIOD_PIN2) |       \
      PIN_MODE_ANALOG(GPIOD_PIN3) | PIN_MODE_ANALOG(GPIOD_PIN4) | PIN_MODE_ANALOG(GPIOD_PIN5) |    \
      PIN_MODE_ANALOG(GPIOD_PIN6) | PIN_MODE_ANALOG(GPIOD_PIN7) | PIN_MODE_ANALOG(GPIOD_PIN8) |    \
      PIN_MODE_ANALOG(GPIOD_PIN9) | PIN_MODE_ANALOG(GPIOD_PIN10) | PIN_MODE_ANALOG(GPIOD_PIN11) |  \
      PIN_MODE_ANALOG(GPIOD_PIN12) | PIN_MODE_ANALOG(GPIOD_PIN13) | PIN_MODE_ANALOG(GPIOD_PIN14) | \
      PIN_MODE_ANALOG(GPIOD_PIN15))
#define VAL_GPIOD_OTYPER                                                  \
  (PIN_OTYPE_PUSHPULL(GPIOD_PIN0) | PIN_OTYPE_PUSHPULL(GPIOD_PIN1) |      \
      PIN_OTYPE_PUSHPULL(GPIOD_PIN2) | PIN_OTYPE_PUSHPULL(GPIOD_PIN3) |   \
      PIN_OTYPE_PUSHPULL(GPIOD_PIN4) | PIN_OTYPE_PUSHPULL(GPIOD_PIN5) |   \
      PIN_OTYPE_PUSHPULL(GPIOD_PIN6) | PIN_OTYPE_PUSHPULL(GPIOD_PIN7) |   \
      PIN_OTYPE_PUSHPULL(GPIOD_PIN8) | PIN_OTYPE_PUSHPULL(GPIOD_PIN9) |   \
      PIN_OTYPE_PUSHPULL(GPIOD_PIN10) | PIN_OTYPE_PUSHPULL(GPIOD_PIN11) | \
      PIN_OTYPE_PUSHPULL(GPIOD_PIN12) | PIN_OTYPE_PUSHPULL(GPIOD_PIN13) | \
      PIN_OTYPE_PUSHPULL(GPIOD_PIN14) | PIN_OTYPE_PUSHPULL(GPIOD_PIN15))
#define VAL_GPIOD_OSPEEDR                                                                          \
  (PIN_OSPEED_HIGH(GPIOD_PIN0) | PIN_OSPEED_HIGH(GPIOD_PIN1) | PIN_OSPEED_HIGH(GPIOD_PIN2) |       \
      PIN_OSPEED_HIGH(GPIOD_PIN3) | PIN_OSPEED_HIGH(GPIOD_PIN4) | PIN_OSPEED_HIGH(GPIOD_PIN5) |    \
      PIN_OSPEED_HIGH(GPIOD_PIN6) | PIN_OSPEED_HIGH(GPIOD_PIN7) | PIN_OSPEED_HIGH(GPIOD_PIN8) |    \
      PIN_OSPEED_HIGH(GPIOD_PIN9) | PIN_OSPEED_HIGH(GPIOD_PIN10) | PIN_OSPEED_HIGH(GPIOD_PIN11) |  \
      PIN_OSPEED_HIGH(GPIOD_PIN12) | PIN_OSPEED_HIGH(GPIOD_PIN13) | PIN_OSPEED_HIGH(GPIOD_PIN14) | \
      PIN_OSPEED_HIGH(GPIOD_PIN15))
#define VAL_GPIOD_PUPDR                                                   \
  (PIN_PUPDR_FLOATING(GPIOD_PIN0) | PIN_PUPDR_FLOATING(GPIOD_PIN1) |      \
      PIN_PUPDR_FLOATING(GPIOD_PIN2) | PIN_PUPDR_FLOATING(GPIOD_PIN3) |   \
      PIN_PUPDR_FLOATING(GPIOD_PIN4) | PIN_PUPDR_FLOATING(GPIOD_PIN5) |   \
      PIN_PUPDR_FLOATING(GPIOD_PIN6) | PIN_PUPDR_FLOATING(GPIOD_PIN7) |   \
      PIN_PUPDR_FLOATING(GPIOD_PIN8) | PIN_PUPDR_FLOATING(GPIOD_PIN9) |   \
      PIN_PUPDR_FLOATING(GPIOD_PIN10) | PIN_PUPDR_FLOATING(GPIOD_PIN11) | \
      PIN_PUPDR_FLOATING(GPIOD_PIN12) | PIN_PUPDR_FLOATING(GPIOD_PIN13) | \
      PIN_PUPDR_FLOATING(GPIOD_PIN14) | PIN_PUPDR_FLOATING(GPIOD_PIN15))
#define VAL_GPIOD_ODR                                                                     \
  (PIN_ODR_HIGH(GPIOD_PIN0) | PIN_ODR_HIGH(GPIOD_PIN1) | PIN_ODR_HIGH(GPIOD_PIN2) |       \
      PIN_ODR_HIGH(GPIOD_PIN3) | PIN_ODR_HIGH(GPIOD_PIN4) | PIN_ODR_HIGH(GPIOD_PIN5) |    \
      PIN_ODR_HIGH(GPIOD_PIN6) | PIN_ODR_HIGH(GPIOD_PIN7) | PIN_ODR_HIGH(GPIOD_PIN8) |    \
      PIN_ODR_HIGH(GPIOD_PIN9) | PIN_ODR_HIGH(GPIOD_PIN10) | PIN_ODR_HIGH(GPIOD_PIN11) |  \
      PIN_ODR_HIGH(GPIOD_PIN12) | PIN_ODR_HIGH(GPIOD_PIN13) | PIN_ODR_HIGH(GPIOD_PIN14) | \
      PIN_ODR_HIGH(GPIOD_PIN15))
#define VAL_GPIOD_AFRL                                                                          \
  (PIN_AFIO_AF(GPIOD_PIN0, 0U) | PIN_AFIO_AF(GPIOD_PIN1, 0U) | PIN_AFIO_AF(GPIOD_PIN2, 0U) |    \
      PIN_AFIO_AF(GPIOD_PIN3, 0U) | PIN_AFIO_AF(GPIOD_PIN4, 0U) | PIN_AFIO_AF(GPIOD_PIN5, 0U) | \
      PIN_AFIO_AF(GPIOD_PIN6, 0U) | PIN_AFIO_AF(GPIOD_PIN7, 0U))
#define VAL_GPIOD_AFRH                                                                             \
  (PIN_AFIO_AF(GPIOD_PIN8, 0U) | PIN_AFIO_AF(GPIOD_PIN9, 0U) | PIN_AFIO_AF(GPIOD_PIN10, 0U) |      \
      PIN_AFIO_AF(GPIOD_PIN11, 0U) | PIN_AFIO_AF(GPIOD_PIN12, 0U) | PIN_AFIO_AF(GPIOD_PIN13, 0U) | \
      PIN_AFIO_AF(GPIOD_PIN14, 0U) | PIN_AFIO_AF(GPIOD_PIN15, 0U))
#define VAL_GPIOD_ASCR                                                                             \
  (PIN_ASCR_DISABLED(GPIOD_PIN0) | PIN_ASCR_DISABLED(GPIOD_PIN1) | PIN_ASCR_DISABLED(GPIOD_PIN2) | \
      PIN_ASCR_DISABLED(GPIOD_PIN3) | PIN_ASCR_DISABLED(GPIOD_PIN4) |                              \
      PIN_ASCR_DISABLED(GPIOD_PIN5) | PIN_ASCR_DISABLED(GPIOD_PIN6) |                              \
      PIN_ASCR_DISABLED(GPIOD_PIN7) | PIN_ASCR_DISABLED(GPIOD_PIN8) |                              \
      PIN_ASCR_DISABLED(GPIOD_PIN9) | PIN_ASCR_DISABLED(GPIOD_PIN10) |                             \
      PIN_ASCR_DISABLED(GPIOD_PIN11) | PIN_ASCR_DISABLED(GPIOD_PIN12) |                            \
      PIN_ASCR_DISABLED(GPIOD_PIN13) | PIN_ASCR_DISABLED(GPIOD_PIN14) |                            \
      PIN_ASCR_DISABLED(GPIOD_PIN15))
#define VAL_GPIOD_LOCKR                                                   \
  (PIN_LOCKR_DISABLED(GPIOD_PIN0) | PIN_LOCKR_DISABLED(GPIOD_PIN1) |      \
      PIN_LOCKR_DISABLED(GPIOD_PIN2) | PIN_LOCKR_DISABLED(GPIOD_PIN3) |   \
      PIN_LOCKR_DISABLED(GPIOD_PIN4) | PIN_LOCKR_DISABLED(GPIOD_PIN5) |   \
      PIN_LOCKR_DISABLED(GPIOD_PIN6) | PIN_LOCKR_DISABLED(GPIOD_PIN7) |   \
      PIN_LOCKR_DISABLED(GPIOD_PIN8) | PIN_LOCKR_DISABLED(GPIOD_PIN9) |   \
      PIN_LOCKR_DISABLED(GPIOD_PIN10) | PIN_LOCKR_DISABLED(GPIOD_PIN11) | \
      PIN_LOCKR_DISABLED(GPIOD_PIN12) | PIN_LOCKR_DISABLED(GPIOD_PIN13) | \
      PIN_LOCKR_DISABLED(GPIOD_PIN14) | PIN_LOCKR_DISABLED(GPIOD_PIN15))

/*
 * GPIOE setup:
 *
 * PE0  - PIN0                      (analog).
 * PE1  - PIN1                      (analog).
 * PE2  - PIN2                      (analog).
 * PE3  - PIN3                      (analog).
 * PE4  - PIN4                      (analog).
 * PE5  - PIN5                      (analog).
 * PE6  - PIN6                      (analog).
 * PE7  - PIN7                      (analog).
 * PE8  - PIN8                      (analog).
 * PE9  - PIN9                      (analog).
 * PE10 - PIN10                     (analog).
 * PE11 - PIN11                     (analog).
 * PE12 - PIN12                     (analog).
 * PE13 - PIN13                     (analog).
 * PE14 - PIN14                     (analog).
 * PE15 - PIN15                     (analog).
 */
#define VAL_GPIOE_MODER                                                                            \
  (PIN_MODE_ANALOG(GPIOE_PIN0) | PIN_MODE_ANALOG(GPIOE_PIN1) | PIN_MODE_ANALOG(GPIOE_PIN2) |       \
      PIN_MODE_ANALOG(GPIOE_PIN3) | PIN_MODE_ANALOG(GPIOE_PIN4) | PIN_MODE_ANALOG(GPIOE_PIN5) |    \
      PIN_MODE_ANALOG(GPIOE_PIN6) | PIN_MODE_ANALOG(GPIOE_PIN7) | PIN_MODE_ANALOG(GPIOE_PIN8) |    \
      PIN_MODE_ANALOG(GPIOE_PIN9) | PIN_MODE_ANALOG(GPIOE_PIN10) | PIN_MODE_ANALOG(GPIOE_PIN11) |  \
      PIN_MODE_ANALOG(GPIOE_PIN12) | PIN_MODE_ANALOG(GPIOE_PIN13) | PIN_MODE_ANALOG(GPIOE_PIN14) | \
      PIN_MODE_ANALOG(GPIOE_PIN15))
#define VAL_GPIOE_OTYPER                                                  \
  (PIN_OTYPE_PUSHPULL(GPIOE_PIN0) | PIN_OTYPE_PUSHPULL(GPIOE_PIN1) |      \
      PIN_OTYPE_PUSHPULL(GPIOE_PIN2) | PIN_OTYPE_PUSHPULL(GPIOE_PIN3) |   \
      PIN_OTYPE_PUSHPULL(GPIOE_PIN4) | PIN_OTYPE_PUSHPULL(GPIOE_PIN5) |   \
      PIN_OTYPE_PUSHPULL(GPIOE_PIN6) | PIN_OTYPE_PUSHPULL(GPIOE_PIN7) |   \
      PIN_OTYPE_PUSHPULL(GPIOE_PIN8) | PIN_OTYPE_PUSHPULL(GPIOE_PIN9) |   \
      PIN_OTYPE_PUSHPULL(GPIOE_PIN10) | PIN_OTYPE_PUSHPULL(GPIOE_PIN11) | \
      PIN_OTYPE_PUSHPULL(GPIOE_PIN12) | PIN_OTYPE_PUSHPULL(GPIOE_PIN13) | \
      PIN_OTYPE_PUSHPULL(GPIOE_PIN14) | PIN_OTYPE_PUSHPULL(GPIOE_PIN15))
#define VAL_GPIOE_OSPEEDR                                                                          \
  (PIN_OSPEED_HIGH(GPIOE_PIN0) | PIN_OSPEED_HIGH(GPIOE_PIN1) | PIN_OSPEED_HIGH(GPIOE_PIN2) |       \
      PIN_OSPEED_HIGH(GPIOE_PIN3) | PIN_OSPEED_HIGH(GPIOE_PIN4) | PIN_OSPEED_HIGH(GPIOE_PIN5) |    \
      PIN_OSPEED_HIGH(GPIOE_PIN6) | PIN_OSPEED_HIGH(GPIOE_PIN7) | PIN_OSPEED_HIGH(GPIOE_PIN8) |    \
      PIN_OSPEED_HIGH(GPIOE_PIN9) | PIN_OSPEED_HIGH(GPIOE_PIN10) | PIN_OSPEED_HIGH(GPIOE_PIN11) |  \
      PIN_OSPEED_HIGH(GPIOE_PIN12) | PIN_OSPEED_HIGH(GPIOE_PIN13) | PIN_OSPEED_HIGH(GPIOE_PIN14) | \
      PIN_OSPEED_HIGH(GPIOE_PIN15))
#define VAL_GPIOE_PUPDR                                                   \
  (PIN_PUPDR_FLOATING(GPIOE_PIN0) | PIN_PUPDR_FLOATING(GPIOE_PIN1) |      \
      PIN_PUPDR_FLOATING(GPIOE_PIN2) | PIN_PUPDR_FLOATING(GPIOE_PIN3) |   \
      PIN_PUPDR_FLOATING(GPIOE_PIN4) | PIN_PUPDR_FLOATING(GPIOE_PIN5) |   \
      PIN_PUPDR_FLOATING(GPIOE_PIN6) | PIN_PUPDR_FLOATING(GPIOE_PIN7) |   \
      PIN_PUPDR_FLOATING(GPIOE_PIN8) | PIN_PUPDR_FLOATING(GPIOE_PIN9) |   \
      PIN_PUPDR_FLOATING(GPIOE_PIN10) | PIN_PUPDR_FLOATING(GPIOE_PIN11) | \
      PIN_PUPDR_FLOATING(GPIOE_PIN12) | PIN_PUPDR_FLOATING(GPIOE_PIN13) | \
      PIN_PUPDR_FLOATING(GPIOE_PIN14) | PIN_PUPDR_FLOATING(GPIOE_PIN15))
#define VAL_GPIOE_ODR                                                                     \
  (PIN_ODR_HIGH(GPIOE_PIN0) | PIN_ODR_HIGH(GPIOE_PIN1) | PIN_ODR_HIGH(GPIOE_PIN2) |       \
      PIN_ODR_HIGH(GPIOE_PIN3) | PIN_ODR_HIGH(GPIOE_PIN4) | PIN_ODR_HIGH(GPIOE_PIN5) |    \
      PIN_ODR_HIGH(GPIOE_PIN6) | PIN_ODR_HIGH(GPIOE_PIN7) | PIN_ODR_HIGH(GPIOE_PIN8) |    \
      PIN_ODR_HIGH(GPIOE_PIN9) | PIN_ODR_HIGH(GPIOE_PIN10) | PIN_ODR_HIGH(GPIOE_PIN11) |  \
      PIN_ODR_HIGH(GPIOE_PIN12) | PIN_ODR_HIGH(GPIOE_PIN13) | PIN_ODR_HIGH(GPIOE_PIN14) | \
      PIN_ODR_HIGH(GPIOE_PIN15))
#define VAL_GPIOE_AFRL                                                                          \
  (PIN_AFIO_AF(GPIOE_PIN0, 0U) | PIN_AFIO_AF(GPIOE_PIN1, 0U) | PIN_AFIO_AF(GPIOE_PIN2, 0U) |    \
      PIN_AFIO_AF(GPIOE_PIN3, 0U) | PIN_AFIO_AF(GPIOE_PIN4, 0U) | PIN_AFIO_AF(GPIOE_PIN5, 0U) | \
      PIN_AFIO_AF(GPIOE_PIN6, 0U) | PIN_AFIO_AF(GPIOE_PIN7, 0U))
#define VAL_GPIOE_AFRH                                                                             \
  (PIN_AFIO_AF(GPIOE_PIN8, 0U) | PIN_AFIO_AF(GPIOE_PIN9, 0U) | PIN_AFIO_AF(GPIOE_PIN10, 0U) |      \
      PIN_AFIO_AF(GPIOE_PIN11, 0U) | PIN_AFIO_AF(GPIOE_PIN12, 0U) | PIN_AFIO_AF(GPIOE_PIN13, 0U) | \
      PIN_AFIO_AF(GPIOE_PIN14, 0U) | PIN_AFIO_AF(GPIOE_PIN15, 0U))
#define VAL_GPIOE_ASCR                                                                             \
  (PIN_ASCR_DISABLED(GPIOE_PIN0) | PIN_ASCR_DISABLED(GPIOE_PIN1) | PIN_ASCR_DISABLED(GPIOE_PIN2) | \
      PIN_ASCR_DISABLED(GPIOE_PIN3) | PIN_ASCR_DISABLED(GPIOE_PIN4) |                              \
      PIN_ASCR_DISABLED(GPIOE_PIN5) | PIN_ASCR_DISABLED(GPIOE_PIN6) |                              \
      PIN_ASCR_DISABLED(GPIOE_PIN7) | PIN_ASCR_DISABLED(GPIOE_PIN8) |                              \
      PIN_ASCR_DISABLED(GPIOE_PIN9) | PIN_ASCR_DISABLED(GPIOE_PIN10) |                             \
      PIN_ASCR_DISABLED(GPIOE_PIN11) | PIN_ASCR_DISABLED(GPIOE_PIN12) |                            \
      PIN_ASCR_DISABLED(GPIOE_PIN13) | PIN_ASCR_DISABLED(GPIOE_PIN14) |                            \
      PIN_ASCR_DISABLED(GPIOE_PIN15))
#define VAL_GPIOE_LOCKR                                                   \
  (PIN_LOCKR_DISABLED(GPIOE_PIN0) | PIN_LOCKR_DISABLED(GPIOE_PIN1) |      \
      PIN_LOCKR_DISABLED(GPIOE_PIN2) | PIN_LOCKR_DISABLED(GPIOE_PIN3) |   \
      PIN_LOCKR_DISABLED(GPIOE_PIN4) | PIN_LOCKR_DISABLED(GPIOE_PIN5) |   \
      PIN_LOCKR_DISABLED(GPIOE_PIN6) | PIN_LOCKR_DISABLED(GPIOE_PIN7) |   \
      PIN_LOCKR_DISABLED(GPIOE_PIN8) | PIN_LOCKR_DISABLED(GPIOE_PIN9) |   \
      PIN_LOCKR_DISABLED(GPIOE_PIN10) | PIN_LOCKR_DISABLED(GPIOE_PIN11) | \
      PIN_LOCKR_DISABLED(GPIOE_PIN12) | PIN_LOCKR_DISABLED(GPIOE_PIN13) | \
      PIN_LOCKR_DISABLED(GPIOE_PIN14) | PIN_LOCKR_DISABLED(GPIOE_PIN15))

/*
 * GPIOF setup:
 *
 * PF0  - PIN0                      (analog).
 * PF1  - PIN1                      (analog).
 * PF2  - PIN2                      (analog).
 * PF3  - PIN3                      (analog).
 * PF4  - PIN4                      (analog).
 * PF5  - PIN5                      (analog).
 * PF6  - PIN6                      (analog).
 * PF7  - PIN7                      (analog).
 * PF8  - PIN8                      (analog).
 * PF9  - PIN9                      (analog).
 * PF10 - PIN10                     (analog).
 * PF11 - PIN11                     (analog).
 * PF12 - PIN12                     (analog).
 * PF13 - PIN13                     (analog).
 * PF14 - PIN14                     (analog).
 * PF15 - PIN15                     (analog).
 */
#define VAL_GPIOF_MODER                                                                            \
  (PIN_MODE_ANALOG(GPIOF_PIN0) | PIN_MODE_ANALOG(GPIOF_PIN1) | PIN_MODE_ANALOG(GPIOF_PIN2) |       \
      PIN_MODE_ANALOG(GPIOF_PIN3) | PIN_MODE_ANALOG(GPIOF_PIN4) | PIN_MODE_ANALOG(GPIOF_PIN5) |    \
      PIN_MODE_ANALOG(GPIOF_PIN6) | PIN_MODE_ANALOG(GPIOF_PIN7) | PIN_MODE_ANALOG(GPIOF_PIN8) |    \
      PIN_MODE_ANALOG(GPIOF_PIN9) | PIN_MODE_ANALOG(GPIOF_PIN10) | PIN_MODE_ANALOG(GPIOF_PIN11) |  \
      PIN_MODE_ANALOG(GPIOF_PIN12) | PIN_MODE_ANALOG(GPIOF_PIN13) | PIN_MODE_ANALOG(GPIOF_PIN14) | \
      PIN_MODE_ANALOG(GPIOF_PIN15))
#define VAL_GPIOF_OTYPER                                                  \
  (PIN_OTYPE_PUSHPULL(GPIOF_PIN0) | PIN_OTYPE_PUSHPULL(GPIOF_PIN1) |      \
      PIN_OTYPE_PUSHPULL(GPIOF_PIN2) | PIN_OTYPE_PUSHPULL(GPIOF_PIN3) |   \
      PIN_OTYPE_PUSHPULL(GPIOF_PIN4) | PIN_OTYPE_PUSHPULL(GPIOF_PIN5) |   \
      PIN_OTYPE_PUSHPULL(GPIOF_PIN6) | PIN_OTYPE_PUSHPULL(GPIOF_PIN7) |   \
      PIN_OTYPE_PUSHPULL(GPIOF_PIN8) | PIN_OTYPE_PUSHPULL(GPIOF_PIN9) |   \
      PIN_OTYPE_PUSHPULL(GPIOF_PIN10) | PIN_OTYPE_PUSHPULL(GPIOF_PIN11) | \
      PIN_OTYPE_PUSHPULL(GPIOF_PIN12) | PIN_OTYPE_PUSHPULL(GPIOF_PIN13) | \
      PIN_OTYPE_PUSHPULL(GPIOF_PIN14) | PIN_OTYPE_PUSHPULL(GPIOF_PIN15))
#define VAL_GPIOF_OSPEEDR                                                                          \
  (PIN_OSPEED_HIGH(GPIOF_PIN0) | PIN_OSPEED_HIGH(GPIOF_PIN1) | PIN_OSPEED_HIGH(GPIOF_PIN2) |       \
      PIN_OSPEED_HIGH(GPIOF_PIN3) | PIN_OSPEED_HIGH(GPIOF_PIN4) | PIN_OSPEED_HIGH(GPIOF_PIN5) |    \
      PIN_OSPEED_HIGH(GPIOF_PIN6) | PIN_OSPEED_HIGH(GPIOF_PIN7) | PIN_OSPEED_HIGH(GPIOF_PIN8) |    \
      PIN_OSPEED_HIGH(GPIOF_PIN9) | PIN_OSPEED_HIGH(GPIOF_PIN10) | PIN_OSPEED_HIGH(GPIOF_PIN11) |  \
      PIN_OSPEED_HIGH(GPIOF_PIN12) | PIN_OSPEED_HIGH(GPIOF_PIN13) | PIN_OSPEED_HIGH(GPIOF_PIN14) | \
      PIN_OSPEED_HIGH(GPIOF_PIN15))
#define VAL_GPIOF_PUPDR                                                   \
  (PIN_PUPDR_FLOATING(GPIOF_PIN0) | PIN_PUPDR_FLOATING(GPIOF_PIN1) |      \
      PIN_PUPDR_FLOATING(GPIOF_PIN2) | PIN_PUPDR_FLOATING(GPIOF_PIN3) |   \
      PIN_PUPDR_FLOATING(GPIOF_PIN4) | PIN_PUPDR_FLOATING(GPIOF_PIN5) |   \
      PIN_PUPDR_FLOATING(GPIOF_PIN6) | PIN_PUPDR_FLOATING(GPIOF_PIN7) |   \
      PIN_PUPDR_FLOATING(GPIOF_PIN8) | PIN_PUPDR_FLOATING(GPIOF_PIN9) |   \
      PIN_PUPDR_FLOATING(GPIOF_PIN10) | PIN_PUPDR_FLOATING(GPIOF_PIN11) | \
      PIN_PUPDR_FLOATING(GPIOF_PIN12) | PIN_PUPDR_FLOATING(GPIOF_PIN13) | \
      PIN_PUPDR_FLOATING(GPIOF_PIN14) | PIN_PUPDR_FLOATING(GPIOF_PIN15))
#define VAL_GPIOF_ODR                                                                     \
  (PIN_ODR_HIGH(GPIOF_PIN0) | PIN_ODR_HIGH(GPIOF_PIN1) | PIN_ODR_HIGH(GPIOF_PIN2) |       \
      PIN_ODR_HIGH(GPIOF_PIN3) | PIN_ODR_HIGH(GPIOF_PIN4) | PIN_ODR_HIGH(GPIOF_PIN5) |    \
      PIN_ODR_HIGH(GPIOF_PIN6) | PIN_ODR_HIGH(GPIOF_PIN7) | PIN_ODR_HIGH(GPIOF_PIN8) |    \
      PIN_ODR_HIGH(GPIOF_PIN9) | PIN_ODR_HIGH(GPIOF_PIN10) | PIN_ODR_HIGH(GPIOF_PIN11) |  \
      PIN_ODR_HIGH(GPIOF_PIN12) | PIN_ODR_HIGH(GPIOF_PIN13) | PIN_ODR_HIGH(GPIOF_PIN14) | \
      PIN_ODR_HIGH(GPIOF_PIN15))
#define VAL_GPIOF_AFRL                                                                          \
  (PIN_AFIO_AF(GPIOF_PIN0, 0U) | PIN_AFIO_AF(GPIOF_PIN1, 0U) | PIN_AFIO_AF(GPIOF_PIN2, 0U) |    \
      PIN_AFIO_AF(GPIOF_PIN3, 0U) | PIN_AFIO_AF(GPIOF_PIN4, 0U) | PIN_AFIO_AF(GPIOF_PIN5, 0U) | \
      PIN_AFIO_AF(GPIOF_PIN6, 0U) | PIN_AFIO_AF(GPIOF_PIN7, 0U))
#define VAL_GPIOF_AFRH                                                                             \
  (PIN_AFIO_AF(GPIOF_PIN8, 0U) | PIN_AFIO_AF(GPIOF_PIN9, 0U) | PIN_AFIO_AF(GPIOF_PIN10, 0U) |      \
      PIN_AFIO_AF(GPIOF_PIN11, 0U) | PIN_AFIO_AF(GPIOF_PIN12, 0U) | PIN_AFIO_AF(GPIOF_PIN13, 0U) | \
      PIN_AFIO_AF(GPIOF_PIN14, 0U) | PIN_AFIO_AF(GPIOF_PIN15, 0U))
#define VAL_GPIOF_ASCR                                                                             \
  (PIN_ASCR_DISABLED(GPIOF_PIN0) | PIN_ASCR_DISABLED(GPIOF_PIN1) | PIN_ASCR_DISABLED(GPIOF_PIN2) | \
      PIN_ASCR_DISABLED(GPIOF_PIN3) | PIN_ASCR_DISABLED(GPIOF_PIN4) |                              \
      PIN_ASCR_DISABLED(GPIOF_PIN5) | PIN_ASCR_DISABLED(GPIOF_PIN6) |                              \
      PIN_ASCR_DISABLED(GPIOF_PIN7) | PIN_ASCR_DISABLED(GPIOF_PIN8) |                              \
      PIN_ASCR_DISABLED(GPIOF_PIN9) | PIN_ASCR_DISABLED(GPIOF_PIN10) |                             \
      PIN_ASCR_DISABLED(GPIOF_PIN11) | PIN_ASCR_DISABLED(GPIOF_PIN12) |                            \
      PIN_ASCR_DISABLED(GPIOF_PIN13) | PIN_ASCR_DISABLED(GPIOF_PIN14) |                            \
      PIN_ASCR_DISABLED(GPIOF_PIN15))
#define VAL_GPIOF_LOCKR                                                   \
  (PIN_LOCKR_DISABLED(GPIOF_PIN0) | PIN_LOCKR_DISABLED(GPIOF_PIN1) |      \
      PIN_LOCKR_DISABLED(GPIOF_PIN2) | PIN_LOCKR_DISABLED(GPIOF_PIN3) |   \
      PIN_LOCKR_DISABLED(GPIOF_PIN4) | PIN_LOCKR_DISABLED(GPIOF_PIN5) |   \
      PIN_LOCKR_DISABLED(GPIOF_PIN6) | PIN_LOCKR_DISABLED(GPIOF_PIN7) |   \
      PIN_LOCKR_DISABLED(GPIOF_PIN8) | PIN_LOCKR_DISABLED(GPIOF_PIN9) |   \
      PIN_LOCKR_DISABLED(GPIOF_PIN10) | PIN_LOCKR_DISABLED(GPIOF_PIN11) | \
      PIN_LOCKR_DISABLED(GPIOF_PIN12) | PIN_LOCKR_DISABLED(GPIOF_PIN13) | \
      PIN_LOCKR_DISABLED(GPIOF_PIN14) | PIN_LOCKR_DISABLED(GPIOF_PIN15))

/*
 * GPIOG setup:
 *
 * PG0  - PIN0                      (analog).
 * PG1  - PIN1                      (analog).
 * PG2  - PIN2                      (analog).
 * PG3  - PIN3                      (analog).
 * PG4  - PIN4                      (analog).
 * PG5  - PIN5                      (analog).
 * PG6  - PIN6                      (analog).
 * PG7  - PIN7                      (analog).
 * PG8  - PIN8                      (analog).
 * PG9  - PIN9                      (analog).
 * PG10 - PIN10                     (analog).
 * PG11 - PIN11                     (analog).
 * PG12 - PIN12                     (analog).
 * PG13 - PIN13                     (analog).
 * PG14 - PIN14                     (analog).
 * PG15 - PIN15                     (analog).
 */
#define VAL_GPIOG_MODER                                                                            \
  (PIN_MODE_ANALOG(GPIOG_PIN0) | PIN_MODE_ANALOG(GPIOG_PIN1) | PIN_MODE_ANALOG(GPIOG_PIN2) |       \
      PIN_MODE_ANALOG(GPIOG_PIN3) | PIN_MODE_ANALOG(GPIOG_PIN4) | PIN_MODE_ANALOG(GPIOG_PIN5) |    \
      PIN_MODE_ANALOG(GPIOG_PIN6) | PIN_MODE_ANALOG(GPIOG_PIN7) | PIN_MODE_ANALOG(GPIOG_PIN8) |    \
      PIN_MODE_ANALOG(GPIOG_PIN9) | PIN_MODE_ANALOG(GPIOG_PIN10) | PIN_MODE_ANALOG(GPIOG_PIN11) |  \
      PIN_MODE_ANALOG(GPIOG_PIN12) | PIN_MODE_ANALOG(GPIOG_PIN13) | PIN_MODE_ANALOG(GPIOG_PIN14) | \
      PIN_MODE_ANALOG(GPIOG_PIN15))
#define VAL_GPIOG_OTYPER                                                  \
  (PIN_OTYPE_PUSHPULL(GPIOG_PIN0) | PIN_OTYPE_PUSHPULL(GPIOG_PIN1) |      \
      PIN_OTYPE_PUSHPULL(GPIOG_PIN2) | PIN_OTYPE_PUSHPULL(GPIOG_PIN3) |   \
      PIN_OTYPE_PUSHPULL(GPIOG_PIN4) | PIN_OTYPE_PUSHPULL(GPIOG_PIN5) |   \
      PIN_OTYPE_PUSHPULL(GPIOG_PIN6) | PIN_OTYPE_PUSHPULL(GPIOG_PIN7) |   \
      PIN_OTYPE_PUSHPULL(GPIOG_PIN8) | PIN_OTYPE_PUSHPULL(GPIOG_PIN9) |   \
      PIN_OTYPE_PUSHPULL(GPIOG_PIN10) | PIN_OTYPE_PUSHPULL(GPIOG_PIN11) | \
      PIN_OTYPE_PUSHPULL(GPIOG_PIN12) | PIN_OTYPE_PUSHPULL(GPIOG_PIN13) | \
      PIN_OTYPE_PUSHPULL(GPIOG_PIN14) | PIN_OTYPE_PUSHPULL(GPIOG_PIN15))
#define VAL_GPIOG_OSPEEDR                                                 \
  (PIN_OSPEED_VERYLOW(GPIOG_PIN0) | PIN_OSPEED_VERYLOW(GPIOG_PIN1) |      \
      PIN_OSPEED_VERYLOW(GPIOG_PIN2) | PIN_OSPEED_VERYLOW(GPIOG_PIN3) |   \
      PIN_OSPEED_VERYLOW(GPIOG_PIN4) | PIN_OSPEED_VERYLOW(GPIOG_PIN5) |   \
      PIN_OSPEED_VERYLOW(GPIOG_PIN6) | PIN_OSPEED_VERYLOW(GPIOG_PIN7) |   \
      PIN_OSPEED_VERYLOW(GPIOG_PIN8) | PIN_OSPEED_VERYLOW(GPIOG_PIN9) |   \
      PIN_OSPEED_VERYLOW(GPIOG_PIN10) | PIN_OSPEED_VERYLOW(GPIOG_PIN11) | \
      PIN_OSPEED_VERYLOW(GPIOG_PIN12) | PIN_OSPEED_VERYLOW(GPIOG_PIN13) | \
      PIN_OSPEED_VERYLOW(GPIOG_PIN14) | PIN_OSPEED_VERYLOW(GPIOG_PIN15))
#define VAL_GPIOG_PUPDR                                                   \
  (PIN_PUPDR_FLOATING(GPIOG_PIN0) | PIN_PUPDR_FLOATING(GPIOG_PIN1) |      \
      PIN_PUPDR_FLOATING(GPIOG_PIN2) | PIN_PUPDR_FLOATING(GPIOG_PIN3) |   \
      PIN_PUPDR_FLOATING(GPIOG_PIN4) | PIN_PUPDR_FLOATING(GPIOG_PIN5) |   \
      PIN_PUPDR_FLOATING(GPIOG_PIN6) | PIN_PUPDR_FLOATING(GPIOG_PIN7) |   \
      PIN_PUPDR_FLOATING(GPIOG_PIN8) | PIN_PUPDR_FLOATING(GPIOG_PIN9) |   \
      PIN_PUPDR_FLOATING(GPIOG_PIN10) | PIN_PUPDR_FLOATING(GPIOG_PIN11) | \
      PIN_PUPDR_FLOATING(GPIOG_PIN12) | PIN_PUPDR_FLOATING(GPIOG_PIN13) | \
      PIN_PUPDR_FLOATING(GPIOG_PIN14) | PIN_PUPDR_FLOATING(GPIOG_PIN15))
#define VAL_GPIOG_ODR                                                                     \
  (PIN_ODR_HIGH(GPIOG_PIN0) | PIN_ODR_HIGH(GPIOG_PIN1) | PIN_ODR_HIGH(GPIOG_PIN2) |       \
      PIN_ODR_HIGH(GPIOG_PIN3) | PIN_ODR_HIGH(GPIOG_PIN4) | PIN_ODR_HIGH(GPIOG_PIN5) |    \
      PIN_ODR_HIGH(GPIOG_PIN6) | PIN_ODR_HIGH(GPIOG_PIN7) | PIN_ODR_HIGH(GPIOG_PIN8) |    \
      PIN_ODR_HIGH(GPIOG_PIN9) | PIN_ODR_HIGH(GPIOG_PIN10) | PIN_ODR_HIGH(GPIOG_PIN11) |  \
      PIN_ODR_HIGH(GPIOG_PIN12) | PIN_ODR_HIGH(GPIOG_PIN13) | PIN_ODR_HIGH(GPIOG_PIN14) | \
      PIN_ODR_HIGH(GPIOG_PIN15))
#define VAL_GPIOG_AFRL                                                                          \
  (PIN_AFIO_AF(GPIOG_PIN0, 0U) | PIN_AFIO_AF(GPIOG_PIN1, 0U) | PIN_AFIO_AF(GPIOG_PIN2, 0U) |    \
      PIN_AFIO_AF(GPIOG_PIN3, 0U) | PIN_AFIO_AF(GPIOG_PIN4, 0U) | PIN_AFIO_AF(GPIOG_PIN5, 0U) | \
      PIN_AFIO_AF(GPIOG_PIN6, 0U) | PIN_AFIO_AF(GPIOG_PIN7, 0U))
#define VAL_GPIOG_AFRH                                                                             \
  (PIN_AFIO_AF(GPIOG_PIN8, 0U) | PIN_AFIO_AF(GPIOG_PIN9, 0U) | PIN_AFIO_AF(GPIOG_PIN10, 0U) |      \
      PIN_AFIO_AF(GPIOG_PIN11, 0U) | PIN_AFIO_AF(GPIOG_PIN12, 0U) | PIN_AFIO_AF(GPIOG_PIN13, 0U) | \
      PIN_AFIO_AF(GPIOG_PIN14, 0U) | PIN_AFIO_AF(GPIOG_PIN15, 0U))
#define VAL_GPIOG_ASCR                                                                             \
  (PIN_ASCR_DISABLED(GPIOG_PIN0) | PIN_ASCR_DISABLED(GPIOG_PIN1) | PIN_ASCR_DISABLED(GPIOG_PIN2) | \
      PIN_ASCR_DISABLED(GPIOG_PIN3) | PIN_ASCR_DISABLED(GPIOG_PIN4) |                              \
      PIN_ASCR_DISABLED(GPIOG_PIN5) | PIN_ASCR_DISABLED(GPIOG_PIN6) |                              \
      PIN_ASCR_DISABLED(GPIOG_PIN7) | PIN_ASCR_DISABLED(GPIOG_PIN8) |                              \
      PIN_ASCR_DISABLED(GPIOG_PIN9) | PIN_ASCR_DISABLED(GPIOG_PIN10) |                             \
      PIN_ASCR_DISABLED(GPIOG_PIN11) | PIN_ASCR_DISABLED(GPIOG_PIN12) |                            \
      PIN_ASCR_DISABLED(GPIOG_PIN13) | PIN_ASCR_DISABLED(GPIOG_PIN14) |                            \
      PIN_ASCR_DISABLED(GPIOG_PIN15))
#define VAL_GPIOG_LOCKR                                                   \
  (PIN_LOCKR_DISABLED(GPIOG_PIN0) | PIN_LOCKR_DISABLED(GPIOG_PIN1) |      \
      PIN_LOCKR_DISABLED(GPIOG_PIN2) | PIN_LOCKR_DISABLED(GPIOG_PIN3) |   \
      PIN_LOCKR_DISABLED(GPIOG_PIN4) | PIN_LOCKR_DISABLED(GPIOG_PIN5) |   \
      PIN_LOCKR_DISABLED(GPIOG_PIN6) | PIN_LOCKR_DISABLED(GPIOG_PIN7) |   \
      PIN_LOCKR_DISABLED(GPIOG_PIN8) | PIN_LOCKR_DISABLED(GPIOG_PIN9) |   \
      PIN_LOCKR_DISABLED(GPIOG_PIN10) | PIN_LOCKR_DISABLED(GPIOG_PIN11) | \
      PIN_LOCKR_DISABLED(GPIOG_PIN12) | PIN_LOCKR_DISABLED(GPIOG_PIN13) | \
      PIN_LOCKR_DISABLED(GPIOG_PIN14) | PIN_LOCKR_DISABLED(GPIOG_PIN15))

/*
 * GPIOH setup:
 *
 * PH0  - PIN0                      (analog).
 * PH1  - PIN1                      (analog).
 * PH2  - PIN2                      (analog).
 * PH3  - PIN3                      (analog).
 * PH4  - PIN4                      (analog).
 * PH5  - PIN5                      (analog).
 * PH6  - PIN6                      (analog).
 * PH7  - PIN7                      (analog).
 * PH8  - PIN8                      (analog).
 * PH9  - PIN9                      (analog).
 * PH10 - PIN10                     (analog).
 * PH11 - PIN11                     (analog).
 * PH12 - PIN12                     (analog).
 * PH13 - PIN13                     (analog).
 * PH14 - PIN14                     (analog).
 * PH15 - PIN15                     (analog).
 */
#define VAL_GPIOH_MODER                                                                            \
  (PIN_MODE_ANALOG(GPIOH_PIN0) | PIN_MODE_ANALOG(GPIOH_PIN1) | PIN_MODE_ANALOG(GPIOH_PIN2) |       \
      PIN_MODE_ANALOG(GPIOH_PIN3) | PIN_MODE_ANALOG(GPIOH_PIN4) | PIN_MODE_ANALOG(GPIOH_PIN5) |    \
      PIN_MODE_ANALOG(GPIOH_PIN6) | PIN_MODE_ANALOG(GPIOH_PIN7) | PIN_MODE_ANALOG(GPIOH_PIN8) |    \
      PIN_MODE_ANALOG(GPIOH_PIN9) | PIN_MODE_ANALOG(GPIOH_PIN10) | PIN_MODE_ANALOG(GPIOH_PIN11) |  \
      PIN_MODE_ANALOG(GPIOH_PIN12) | PIN_MODE_ANALOG(GPIOH_PIN13) | PIN_MODE_ANALOG(GPIOH_PIN14) | \
      PIN_MODE_ANALOG(GPIOH_PIN15))
#define VAL_GPIOH_OTYPER                                                  \
  (PIN_OTYPE_PUSHPULL(GPIOH_PIN0) | PIN_OTYPE_PUSHPULL(GPIOH_PIN1) |      \
      PIN_OTYPE_PUSHPULL(GPIOH_PIN2) | PIN_OTYPE_PUSHPULL(GPIOH_PIN3) |   \
      PIN_OTYPE_PUSHPULL(GPIOH_PIN4) | PIN_OTYPE_PUSHPULL(GPIOH_PIN5) |   \
      PIN_OTYPE_PUSHPULL(GPIOH_PIN6) | PIN_OTYPE_PUSHPULL(GPIOH_PIN7) |   \
      PIN_OTYPE_PUSHPULL(GPIOH_PIN8) | PIN_OTYPE_PUSHPULL(GPIOH_PIN9) |   \
      PIN_OTYPE_PUSHPULL(GPIOH_PIN10) | PIN_OTYPE_PUSHPULL(GPIOH_PIN11) | \
      PIN_OTYPE_PUSHPULL(GPIOH_PIN12) | PIN_OTYPE_PUSHPULL(GPIOH_PIN13) | \
      PIN_OTYPE_PUSHPULL(GPIOH_PIN14) | PIN_OTYPE_PUSHPULL(GPIOH_PIN15))
#define VAL_GPIOH_OSPEEDR                                                                       \
  (PIN_OSPEED_HIGH(GPIOH_PIN0) | PIN_OSPEED_HIGH(GPIOH_PIN1) | PIN_OSPEED_VERYLOW(GPIOH_PIN2) | \
      PIN_OSPEED_VERYLOW(GPIOH_PIN3) | PIN_OSPEED_VERYLOW(GPIOH_PIN4) |                         \
      PIN_OSPEED_VERYLOW(GPIOH_PIN5) | PIN_OSPEED_VERYLOW(GPIOH_PIN6) |                         \
      PIN_OSPEED_VERYLOW(GPIOH_PIN7) | PIN_OSPEED_VERYLOW(GPIOH_PIN8) |                         \
      PIN_OSPEED_VERYLOW(GPIOH_PIN9) | PIN_OSPEED_VERYLOW(GPIOH_PIN10) |                        \
      PIN_OSPEED_VERYLOW(GPIOH_PIN11) | PIN_OSPEED_VERYLOW(GPIOH_PIN12) |                       \
      PIN_OSPEED_VERYLOW(GPIOH_PIN13) | PIN_OSPEED_VERYLOW(GPIOH_PIN14) |                       \
      PIN_OSPEED_VERYLOW(GPIOH_PIN15))
#define VAL_GPIOH_PUPDR                                                   \
  (PIN_PUPDR_FLOATING(GPIOH_PIN0) | PIN_PUPDR_FLOATING(GPIOH_PIN1) |      \
      PIN_PUPDR_FLOATING(GPIOH_PIN2) | PIN_PUPDR_FLOATING(GPIOH_PIN3) |   \
      PIN_PUPDR_FLOATING(GPIOH_PIN4) | PIN_PUPDR_FLOATING(GPIOH_PIN5) |   \
      PIN_PUPDR_FLOATING(GPIOH_PIN6) | PIN_PUPDR_FLOATING(GPIOH_PIN7) |   \
      PIN_PUPDR_FLOATING(GPIOH_PIN8) | PIN_PUPDR_FLOATING(GPIOH_PIN9) |   \
      PIN_PUPDR_FLOATING(GPIOH_PIN10) | PIN_PUPDR_FLOATING(GPIOH_PIN11) | \
      PIN_PUPDR_FLOATING(GPIOH_PIN12) | PIN_PUPDR_FLOATING(GPIOH_PIN13) | \
      PIN_PUPDR_FLOATING(GPIOH_PIN14) | PIN_PUPDR_FLOATING(GPIOH_PIN15))
#define VAL_GPIOH_ODR                                                                     \
  (PIN_ODR_HIGH(GPIOH_PIN0) | PIN_ODR_HIGH(GPIOH_PIN1) | PIN_ODR_HIGH(GPIOH_PIN2) |       \
      PIN_ODR_HIGH(GPIOH_PIN3) | PIN_ODR_HIGH(GPIOH_PIN4) | PIN_ODR_HIGH(GPIOH_PIN5) |    \
      PIN_ODR_HIGH(GPIOH_PIN6) | PIN_ODR_HIGH(GPIOH_PIN7) | PIN_ODR_HIGH(GPIOH_PIN8) |    \
      PIN_ODR_HIGH(GPIOH_PIN9) | PIN_ODR_HIGH(GPIOH_PIN10) | PIN_ODR_HIGH(GPIOH_PIN11) |  \
      PIN_ODR_HIGH(GPIOH_PIN12) | PIN_ODR_HIGH(GPIOH_PIN13) | PIN_ODR_HIGH(GPIOH_PIN14) | \
      PIN_ODR_HIGH(GPIOH_PIN15))
#define VAL_GPIOH_AFRL                                                                          \
  (PIN_AFIO_AF(GPIOH_PIN0, 0U) | PIN_AFIO_AF(GPIOH_PIN1, 0U) | PIN_AFIO_AF(GPIOH_PIN2, 0U) |    \
      PIN_AFIO_AF(GPIOH_PIN3, 0U) | PIN_AFIO_AF(GPIOH_PIN4, 0U) | PIN_AFIO_AF(GPIOH_PIN5, 0U) | \
      PIN_AFIO_AF(GPIOH_PIN6, 0U) | PIN_AFIO_AF(GPIOH_PIN7, 0U))
#define VAL_GPIOH_AFRH                                                                             \
  (PIN_AFIO_AF(GPIOH_PIN8, 0U) | PIN_AFIO_AF(GPIOH_PIN9, 0U) | PIN_AFIO_AF(GPIOH_PIN10, 0U) |      \
      PIN_AFIO_AF(GPIOH_PIN11, 0U) | PIN_AFIO_AF(GPIOH_PIN12, 0U) | PIN_AFIO_AF(GPIOH_PIN13, 0U) | \
      PIN_AFIO_AF(GPIOH_PIN14, 0U) | PIN_AFIO_AF(GPIOH_PIN15, 0U))
#define VAL_GPIOH_ASCR                                                                             \
  (PIN_ASCR_DISABLED(GPIOH_PIN0) | PIN_ASCR_DISABLED(GPIOH_PIN1) | PIN_ASCR_DISABLED(GPIOH_PIN2) | \
      PIN_ASCR_DISABLED(GPIOH_PIN3) | PIN_ASCR_DISABLED(GPIOH_PIN4) |                              \
      PIN_ASCR_DISABLED(GPIOH_PIN5) | PIN_ASCR_DISABLED(GPIOH_PIN6) |                              \
      PIN_ASCR_DISABLED(GPIOH_PIN7) | PIN_ASCR_DISABLED(GPIOH_PIN8) |                              \
      PIN_ASCR_DISABLED(GPIOH_PIN9) | PIN_ASCR_DISABLED(GPIOH_PIN10) |                             \
      PIN_ASCR_DISABLED(GPIOH_PIN11) | PIN_ASCR_DISABLED(GPIOH_PIN12) |                            \
      PIN_ASCR_DISABLED(GPIOH_PIN13) | PIN_ASCR_DISABLED(GPIOH_PIN14) |                            \
      PIN_ASCR_DISABLED(GPIOH_PIN15))
#define VAL_GPIOH_LOCKR                                                   \
  (PIN_LOCKR_DISABLED(GPIOH_PIN0) | PIN_LOCKR_DISABLED(GPIOH_PIN1) |      \
      PIN_LOCKR_DISABLED(GPIOH_PIN2) | PIN_LOCKR_DISABLED(GPIOH_PIN3) |   \
      PIN_LOCKR_DISABLED(GPIOH_PIN4) | PIN_LOCKR_DISABLED(GPIOH_PIN5) |   \
      PIN_LOCKR_DISABLED(GPIOH_PIN6) | PIN_LOCKR_DISABLED(GPIOH_PIN7) |   \
      PIN_LOCKR_DISABLED(GPIOH_PIN8) | PIN_LOCKR_DISABLED(GPIOH_PIN9) |   \
      PIN_LOCKR_DISABLED(GPIOH_PIN10) | PIN_LOCKR_DISABLED(GPIOH_PIN11) | \
      PIN_LOCKR_DISABLED(GPIOH_PIN12) | PIN_LOCKR_DISABLED(GPIOH_PIN13) | \
      PIN_LOCKR_DISABLED(GPIOH_PIN14) | PIN_LOCKR_DISABLED(GPIOH_PIN15))

/*===========================================================================*/
/* External declarations.                                                    */
/*===========================================================================*/

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* BOARD_H */
