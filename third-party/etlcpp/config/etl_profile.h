#ifndef __ETL_PROFILE_H__
#define __ETL_PROFILE_H__

#define ETL_LOG_ERRORS
#define ETL_VERBOSE_ERRORS

#include "etl/profiles/gcc_generic.h"
#include "etl/profiles/cpp17.h"

#endif
