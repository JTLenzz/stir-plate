add_library(freertos
    STATIC
        config/support.cpp
        freertos-kernel/portable/MemMang/heap_4.c
        freertos-kernel/event_groups.c
        freertos-kernel/list.c
        freertos-kernel/queue.c
        freertos-kernel/stream_buffer.c
        freertos-kernel/tasks.c
        freertos-kernel/timers.c)
target_include_directories(freertos
    PUBLIC
        config
        freertos-kernel/include)

add_library(freertos-m0
    STATIC EXCLUDE_FROM_ALL
        freertos-kernel/portable/GCC/ARM_CM0/port.c)
target_include_directories(freertos-m0
    PUBLIC
        freertos-kernel/portable/GCC/ARM_CM0)
target_link_libraries(freertos-m0
    PRIVATE
        freertos)

add_library(freertos-m4f
    STATIC EXCLUDE_FROM_ALL
        freertos-kernel/portable/GCC/ARM_CM4F/port.c)
target_include_directories(freertos-m4f
    PUBLIC
        freertos-kernel/portable/GCC/ARM_CM4F)
target_link_libraries(freertos-m4f
    PRIVATE
        freertos)

if (CMAKE_SYSTEM_PROCESSOR STREQUAL cortex-m0)
    target_link_libraries(freertos PUBLIC freertos-m0)
elseif (CMAKE_SYSTEM_PROCESSOR STREQUAL cortex-m4f)
    target_link_libraries(freertos PUBLIC freertos-m4f)
else()
    message(WARNING "Unable to determine FreeRTOS port for ${CMAKE_SYSTEM_PROCESSOR}. Library will not be usable.")
endif()
    
