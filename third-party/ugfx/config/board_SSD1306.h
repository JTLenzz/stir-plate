/*
 * This file is subject to the terms of the GFX License. If a copy of
 * the license was not distributed with this file, you can obtain one at:
 *
 *              http://ugfx.io/license.html
 */

#ifndef _GDISP_LLD_BOARD_H
#define _GDISP_LLD_BOARD_H

#include "hal.h"

static GFXINLINE void init_board(GDisplay *g)
{
  g->board = NULL;
}

static GFXINLINE void post_init_board(GDisplay *g)
{
  (void)g;
}

static GFXINLINE void setpin_reset(GDisplay *g, gBool state)
{
  (void)g;
  if (state) {
    palClearPad(GPIOA, GPIOA_DISPLAY_RES);
  }
  else {
    palSetPad(GPIOA, GPIOA_DISPLAY_RES);
  }
}

static GFXINLINE void acquire_bus(GDisplay *g)
{
  (void)g;
}

static GFXINLINE void release_bus(GDisplay *g)
{
  (void)g;
}

static GFXINLINE void write_cmd(GDisplay *g, gU8 cmd)
{
  (void)g;
  palClearPad(GPIOB, GPIOB_DISPLAY_DC);
  spiSelect(&SPID1);
  spiSend(&SPID1, sizeof(cmd), &cmd);
  spiUnselect(&SPID1);
}

static GFXINLINE void write_data(GDisplay *g, gU8 *data, gU16 length)
{
  (void)g;
  palSetPad(GPIOB, GPIOB_DISPLAY_DC);
  spiSelect(&SPID1);
  spiSend(&SPID1, length, data);
  spiUnselect(&SPID1);
}

#endif /* _GDISP_LLD_BOARD_H */
