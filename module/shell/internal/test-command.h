#pragma once

#include "shell-command.h"

class TEST_COMMAND final : public SHELL_COMMAND_INTF {
 public:
  explicit TEST_COMMAND();
  ~TEST_COMMAND() = default;

  void handle(SHELL const &shell, collection::span<const std::string_view> args) override;
  std::string_view name() const override;
};
