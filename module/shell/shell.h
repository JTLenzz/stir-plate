#pragma once

#include <array>
#include <string_view>

#include "internal/test-command.h"
#include "shell-command.h"
#include "static-vector.h"
#include "task-utility.h"

class SHELL_STREAM_INTF;

class SHELL {
 public:
  SHELL(UBaseType_t priority, SHELL_STREAM_INTF &stream);

  SHELL(SHELL const &) = delete;
  SHELL &operator=(SHELL const &) = delete;

  /**
   * @brief Adds a command handler to the shell module.
   * @param command The command handler to be added to the shell's command lookup.
   */
  void addCommand(SHELL_COMMAND_INTF &command);

  /**
   * @brief Removes a command handler from the shell module.
   * @param command THe command handler to be removed from the shell's command lookup.
   */
  void removeCommand(SHELL_COMMAND_INTF &command);

  /**
   * @brief Send formatted string data on the shell back to the user.
   *
   * This function is not thread-safe and is intended to be called by command handlers as part of
   * their command handling from the context of the shell task.
   *
   * @param format printf-formatted string to send to the user.
   * @param ... printf arguments.
   */
  void print(const char *format, ...) const __attribute__((format(printf, 2, 3)));

 private:
  static constexpr size_t MAX_ARGS = 8;

  /// Help command that prints all available commands.
  class HELP_COMMAND final : public SHELL_COMMAND_INTF {
   public:
    HELP_COMMAND() = delete;
    explicit HELP_COMMAND(SHELL const &shell) : SHELL_COMMAND_INTF(), my_shell(shell)
    {
    }

    void handle(SHELL const &shell, collection::span<const std::string_view> args) override;
    std::string_view name() const override;

   private:
    SHELL const &my_shell;
  };

  /// Helper object for the command container that gets the command name from a command interface
  /// allowing sorted lookup & iteration of commands.
  struct SHELL_COMMAND_MAPPER final {
    using type = std::string_view;

    type operator()(SHELL_COMMAND_INTF const &v) const
    {
      return v.name();
    }
  };

  /// Task worker function.
  void workerFunction();

  /// Parses command line arguments from the command buffer (i.e. input is @c my_command_buffer and
  /// output is @c my_parsed_args).
  void parseArguments();

  /**
   * @brief Returns if the received character should be treated as a new line character (and thus
   * allow processing of a buffered command)
   * @param c The received character.
   * @return @c true if the character should be processed as a new line. @c false otherwise.
   */
  bool newline(char c);

  /// Storage for the task data.
  STATIC_TASK<1024> my_task;

  /// Stream used for the CLI.
  SHELL_STREAM_INTF &my_stream;

  /// Temporary buffer used for reading from hardware.
  std::array<char, 64> my_read_buffer;

  /// Buffers a command to be handled by the device.
  collection::static_vector<char, 256> my_command_buffer;

  /// Arguments parsed from the incoming command passed to the appropriate command handler.
  collection::static_vector<std::string_view, MAX_ARGS> my_parsed_args;

  /// Builtin handler for listing all registered commands.
  HELP_COMMAND my_help_handler;

  /// Handler for testing the command line parsing.
  TEST_COMMAND my_test_handler;

  /// A collection of handlers for incoming commands.
  collection::intrusive_map<SHELL_COMMAND_INTF, SHELL_COMMAND_MAPPER, SHELL_COMMAND_INTF>
      my_commands;

  /// Stores the last received new line character on the shell.
  char my_last_new_line_character;
};
