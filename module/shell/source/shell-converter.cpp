/// Implementation of a simplified from_chars for floating point numbers without
/// hexadecimal support. Core algorithm is based off of a strtod implementation (copyright
/// information retained below) though the overall implementation is quite different.

/*
 * strtod.c --
 *
 *	Source code for the "strtod" library procedure.
 *
 * Copyright (c) 1988-1993 The Regents of the University of California.
 * Copyright (c) 1994 Sun Microsystems, Inc.
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 * RCS: @(#) $Id: strtod.c,v 1.1.1.4 2003/03/06 00:09:04 landonf Exp $
 */

#include "shell-converter.h"

#include <cctype>
#include <cstddef>
#include <limits>
#include <string_view>

// Table giving binary powers of 10.  Entry
// is 10^2^i.  Used to convert decimal
// exponents into floating-point numbers.
static constexpr long double powersOf10[] = {
    10., 100., 1.0e4, 1.0e8, 1.0e16, 1.0e32, 1.0e64, 1.0e128, 1.0e256};

template <typename T>
std::from_chars_result strtox(std::string_view str, T& value)
{
  bool sign;
  bool expSign = false;
  T dblExp;
  const char* p;
  int c;
  int exp = 0;      /* Exponent read from "EX" field. */
  int fracExp = 0;  /* Exponent that derives from the fractional
                     * part.  Under normal circumstatnces, it is
                     * the negative of the number of digits in F.
                     * However, if I is very long, the last digits
                     * of I get dropped (otherwise a long I with a
                     * large negative exponent could cause an
                     * unnecessary overflow on I alone).  In this
                     * case, fracExp is incremented one for each
                     * dropped digit. */
  int mantSize;     /* Number of digits in mantissa. */
  int decPt;        /* Number of mantissa digits BEFORE decimal
                     * point. */
  const char* pExp; /* Temporarily holds location of exponent
                     *in string. */
  std::errc error = std::errc();

  /*
   * Strip off leading blanks and check for a sign.
   */

  p = str.begin();
  while (std::isspace(static_cast<unsigned char>(*p)) && p != str.end()) {
    ++p;
  }

  if (*p == '-') {
    sign = true;
    ++p;
  }
  else {
    if (*p == '+') {
      ++p;
    }
    sign = false;
  }

  /*
   * Count the number of digits in the mantissa (including the decimal
   * point), and also locate the decimal point.
   */

  decPt = -1;
  for (mantSize = 0; p != str.end(); mantSize += 1) {
    c = *p;
    if (!std::isdigit(c)) {
      if ((c != '.') || (decPt >= 0)) {
        break;
      }
      decPt = mantSize;
    }
    ++p;
  }

  /*
   * Now suck up the digits in the mantissa.  Use two integers to
   * collect 9 digits each (this is faster than using floating-point).
   * If the mantissa has more than 18 digits, ignore the extras, since
   * they can't affect the value anyway.
   */

  pExp = p;
  p = str.begin();
  if (decPt < 0) {
    decPt = mantSize;
  }
  else {
    mantSize -= 1; /* One of the digits was the point. */
  }

  if (mantSize > std::numeric_limits<T>::max_digits10) {
    fracExp = decPt - std::numeric_limits<T>::max_digits10;
    mantSize = std::numeric_limits<T>::max_digits10;
  }
  else {
    fracExp = decPt - mantSize;
  }

  if (mantSize == 0) {
    value = 0.0;
    p = str.begin();
    goto done;
  }
  else {
    int frac1, frac2;
    frac1 = 0;
    for (; mantSize > 9; mantSize -= 1) {
      c = *p;
      ++p;
      if (c == '.') {
        c = *p;
        ++p;
      }
      frac1 = 10 * frac1 + (c - '0');
    }
    frac2 = 0;
    for (; mantSize > 0; mantSize -= 1) {
      c = *p;
      ++p;
      if (c == '.') {
        c = *p;
        ++p;
      }
      frac2 = 10 * frac2 + (c - '0');
    }
    value = (1.0e9f * frac1) + frac2;
  }

  /*
   * Skim off the exponent.
   */

  p = pExp;
  if (p != str.end() && ((*p == 'E') || (*p == 'e'))) {
    ++p;
    if (p != str.end() && (*p == '-')) {
      expSign = true;
      ++p;
    }
    else {
      if (p != str.end() && (*p == '+')) {
        ++p;
      }
      expSign = false;
    }

    if (!std::isdigit(static_cast<unsigned char>(*p))) {
      p = pExp;
      goto done;
    }

    while (std::isdigit(static_cast<unsigned char>(*p)) && (p != str.end())) {
      exp = exp * 10 + (*p - '0');
      ++p;
    }
  }

  if (expSign) {
    exp = fracExp - exp;
  }
  else {
    exp = fracExp + exp;
  }

  /*
   * Generate a floating-point number that represents the exponent.
   * Do this by processing the exponent one bit at a time to combine
   * many powers of 2 of 10. Then combine the exponent with the
   * fraction.
   */

  if (exp < 0) {
    expSign = true;
    exp = -exp;
  }
  else {
    expSign = false;
  }

  if (exp > std::numeric_limits<T>::max_exponent10) {
    exp = std::numeric_limits<T>::max_exponent10;
    error = std::errc::result_out_of_range;
  }

  dblExp = T(1.0);
  for (auto* d = std::begin(powersOf10); exp != 0; exp >>= 1, d += 1) {
    if (exp & 1) {
      dblExp *= static_cast<T>(*d);
    }
  }

  if (expSign) {
    value /= dblExp;
  }
  else {
    value *= dblExp;
  }

done:
  if (sign) {
    value = -value;
  }

  return {p, error};
}

template <typename T>
std::from_chars_result SHELL_CONVERTER::from_chars(const char* first, const char* end, T& value)
{
  return strtox({first, static_cast<size_t>(std::distance(first, end))}, value);
}

template std::from_chars_result SHELL_CONVERTER::from_chars<float>(
    const char* first, const char* end, float& value);

template std::from_chars_result SHELL_CONVERTER::from_chars<double>(
    const char* first, const char* end, double& value);

template std::from_chars_result SHELL_CONVERTER::from_chars<long double>(
    const char* first, const char* end, long double& value);
