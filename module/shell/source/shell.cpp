#include "shell.h"

#include <string_view>

#include "printf/printf.h"
#include "shell-stream-intf.h"

SHELL::SHELL(UBaseType_t priority, SHELL_STREAM_INTF &stream)
    : my_task([](void *self) { static_cast<decltype(this)>(self)->workerFunction(); }, "cli", this,
          priority),
      my_stream(stream),
      my_help_handler(*this),
      my_test_handler(),
      my_commands(),
      my_last_new_line_character('\0')
{
  this->addCommand(this->my_help_handler);
  this->addCommand(this->my_test_handler);
}

void SHELL::addCommand(SHELL_COMMAND_INTF &command)
{
  if ((xTaskGetSchedulerState() == taskSCHEDULER_RUNNING) &&
      (xTaskGetCurrentTaskHandle() != this->my_task.getTaskHandle())) {
    std::scoped_lock l(TASK_SCHEDULER_MUTEX::getInstance());

    this->my_commands.insert(command);
  }
  else {
    this->my_commands.insert(command);
  }
}

void SHELL::removeCommand(SHELL_COMMAND_INTF &command)
{
  auto iter = this->my_commands.s_iterator_to(command);
  if ((xTaskGetSchedulerState() == taskSCHEDULER_RUNNING) &&
      (xTaskGetCurrentTaskHandle() != this->my_task.getTaskHandle())) {
    vTaskSuspendAll();
    {
      this->my_commands.erase(iter);
    }
    xTaskResumeAll();
  }
  else {
    this->my_commands.erase(iter);
  }
}

void SHELL::print(char const *format, ...) const
{
  va_list va;
  va_start(va, format);
  vfctprintf(
      [](char character, void *channel) {
        auto stream = static_cast<SHELL_STREAM_INTF *>(channel);
        if (character == '\n') {
          // Add an implicit carriage return for all newlines.
          stream->write('\r');
        }

        // If the client code is already adding carriage returns, suppress them since they're
        // implicitly added above.
        if (character != '\r') {
          stream->write(character);
        }
      },
      &this->my_stream, format, va);
  va_end(va);
}

void SHELL::workerFunction()
{
  static constexpr std::string_view PROMPT("$ ");
  this->my_stream.write(PROMPT);

  for (;;) {
    auto bytesRead = this->my_stream.read(
        boost::as_writable_bytes(collection::span<char>(this->my_read_buffer)));

    for (auto iter = this->my_read_buffer.begin(); iter != &this->my_read_buffer[bytesRead];
         ++iter) {
      if (this->newline(*iter)) {
        this->my_stream.write("\r\n");

        if (!this->my_command_buffer.empty()) {
          this->parseArguments();

          if (!this->my_parsed_args.empty()) {
            auto handler = this->my_commands.find(this->my_parsed_args[0]);
            if (handler != this->my_commands.end()) {
              handler->handle(*this, this->my_parsed_args);
            }
            else {
              this->print("%.*s: command not found\n", this->my_parsed_args[0].size(),
                  this->my_parsed_args[0].begin());
            }
          }

          this->my_command_buffer.clear();
        }

        this->my_stream.write(PROMPT);
      }
      else if (*iter == '\b') {
        if (!this->my_command_buffer.empty()) {
          this->my_stream.write("\b \b");
          this->my_command_buffer.pop_back();
        }
      }
      else if (std::isprint(*iter) != 0) {
        this->my_stream.write(*iter);
        if (this->my_command_buffer.size() < this->my_command_buffer.max_size()) {
          this->my_command_buffer.push_back(*iter);
        }
      }
    }

    this->my_stream.flush();
  }
}

void SHELL::parseArguments()
{
  static constexpr std::string_view delimiters = " \t\n\v\f\r";
  this->my_parsed_args.clear();

  std::string_view command{this->my_command_buffer.data(), this->my_command_buffer.size()};
  auto token = command.find_first_not_of(delimiters);
  while (token != std::string_view::npos) {
    auto tokenEnd = std::min(command.find_first_of(delimiters, token), command.size());
    if (this->my_parsed_args.size() < this->my_parsed_args.max_size()) {
      // This is the same logic as string_view::substr on command, but avoids pulling in the runtime
      // bounds check logic that will abort. This check isn't necessary because we're already
      // checking the token is in bounds as our loop condition.
      this->my_parsed_args.emplace_back(&command[token], tokenEnd - token);
    }
    token = command.find_first_not_of(delimiters, tokenEnd);
  }
}

bool SHELL::newline(char c)
{
  if (!((c == '\r') || (c == '\n'))) {
    this->my_last_new_line_character = '\0';
    return false;
  }

  if ((this->my_last_new_line_character == '\0') || (c == this->my_last_new_line_character)) {
    this->my_last_new_line_character = c;
    return true;
  }
  return false;
}

void SHELL::HELP_COMMAND::handle(
    SHELL const &shell, collection::span<const std::string_view> /* args */)
{
  for (auto const &command : this->my_shell.my_commands) {
    auto name = command.name();
    shell.print("%.*s\n", name.size(), name.begin());
  }
}

std::string_view SHELL::HELP_COMMAND::name() const
{
  return "help";
}
