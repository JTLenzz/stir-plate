#include "internal/test-command.h"

#include "shell.h"

TEST_COMMAND::TEST_COMMAND() : SHELL_COMMAND_INTF()
{
}

void TEST_COMMAND::handle(SHELL const &shell, collection::span<const std::string_view> args)
{
  shell.print("Test command called with %d argument(s):\n", args.size());
  for (const auto arg : args) {
    shell.print("· %.*s\n", arg.size(), arg.begin());
  }
}

std::string_view TEST_COMMAND::name() const
{
  return "test";
}
