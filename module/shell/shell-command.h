#pragma once

#include <string_view>

#include "intrusive-set.h"
#include "span.h"

class SHELL;

class SHELL_COMMAND_INTF : public collection::intrusive_set_element<SHELL_COMMAND_INTF> {
 public:
  /**
   * @brief Command handler function.
   * @param shell The shell instance invoking the command handler.
   * @param args List of arguments provided to the command. @c args[0] will the the command name.
   */
  virtual void handle(SHELL const &shell, collection::span<const std::string_view> args) = 0;

  /**
   * Used for matching command names received on the shell to the appropriate handle function.
   *
   * @return The name of the command.
   */
  virtual std::string_view name() const = 0;

 protected:
  SHELL_COMMAND_INTF() = default;
  SHELL_COMMAND_INTF(SHELL_COMMAND_INTF const &) = delete;
  SHELL_COMMAND_INTF(SHELL_COMMAND_INTF &&) = delete;
  ~SHELL_COMMAND_INTF() = default;
};
