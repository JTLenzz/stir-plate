#pragma once

#include <etl/span.h>
#include <etl/string_view.h>

#include <cstddef>
#include <string_view>
#include <type_traits>

#include "span.h"

class SHELL_STREAM_INTF {
 public:
  /**
   * @brief Read data from the stream into the provided buffer (blocking).
   *
   * @param buffer The buffer to read data into.
   * @return size_t The number of bytes read into the buffer. Depending on the implementation of the
   * underlying stream, this may return less data than the size of the provided buffer.
   */
  virtual size_t read(collection::span<std::byte> buffer) = 0;

  /**
   * @brief Write the provided data onto the stream.
   *
   * @param buffer The data to write into the stream.
   * @return size_t The number of bytes written. Should always be the size of the buffer.
   */
  virtual size_t write(collection::span<const std::byte> buffer) = 0;

  /**
   * @brief Write helper for writing a string to the stream.
   *
   * @param str The string to write to the stream.
   * @return size_t The number of bytes written to the stream.
   */
  size_t write(std::string_view str)
  {
    auto byteSpan = collection::as_bytes(
        collection::span<const decltype(str)::value_type>{str.begin(), str.end()});
    return this->write(byteSpan);
  }

  /**
   * @brief Write helper for writing a single character stream.
   *
   * @param str The character to write to the stream.
   * @return size_t The number of bytes written to the stream.
   */
  size_t write(const char str)
  {
    return this->write(collection::as_bytes(collection::span<const char>(&str, 1)));
  }

  /**
   * @brief Hook for buffered streams to forcibly write any buffered data to the underlying stream.
   */
  virtual void flush() = 0;
};
