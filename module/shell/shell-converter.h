#include <boost/convert.hpp>
#include <boost/convert/base.hpp>
#include <charconv>
// #include "printf.h"

/**
 * @brief Helper for converting strings to numbers using Boost convert and suitable for an embedded
 * environment.
 */
struct SHELL_CONVERTER : public boost::cnv::cnvbase<SHELL_CONVERTER> {
  using this_type = SHELL_CONVERTER;
  using base_type = boost::cnv::cnvbase<this_type>;

  using base_type::operator();

 private:
  friend struct boost::cnv::cnvbase<this_type>;

  template <typename string_type, typename out_type,
      std::enable_if_t<std::is_integral_v<out_type>, void*> = nullptr>
  void str_to(boost::cnv::range<string_type> v, boost::optional<out_type>& r) const
  {
    out_type result;
    const auto [ptr, error] = std::from_chars(v.begin(), v.end(), result);
    if ((error == std::errc()) && (ptr == v.end())) {
      r = result;
    }
  }

  // Most C++ standard libraries don't define the floating point version of from_chars, so this
  // declares an even more basic version for converter use.
  template <typename T>
  static std::from_chars_result from_chars(const char* first, const char* end, T& value);

  template <typename string_type, typename out_type,
      std::enable_if_t<std::is_floating_point_v<out_type>, void*> = nullptr>
  void str_to(boost::cnv::range<string_type> v, boost::optional<out_type>& r) const
  {
    out_type result;
    const auto [ptr, error] = from_chars(v.begin(), v.end(), result);
    if ((error == std::errc()) && (ptr == v.end())) {
      r = result;
    }
  }

  template <typename char_type>
  boost::cnv::range<char_type*> to_str(int_type v, char_type* buf) const
  {
    extern int snprintf_(char* buffer, size_t count, const char* format, ...);
    auto result = snprintf_(buf, bufsize_, "%d", v);
    return {buf, buf + result};
  }

  template <typename char_type>
  boost::cnv::range<char_type*> to_str(uint_type v, char_type* buf) const
  {
    extern int snprintf_(char* buffer, size_t count, const char* format, ...);
    auto result = snprintf_(buf, bufsize_, "%u", v);
    return {buf, buf + result};
  }

  template <typename char_type>
  boost::cnv::range<char_type*> to_str(lint_type v, char_type* buf) const
  {
    extern int snprintf_(char* buffer, size_t count, const char* format, ...);
    auto result = snprintf_(buf, bufsize_, "%ld", v);
    return {buf, buf + result};
  }

  template <typename char_type>
  boost::cnv::range<char_type*> to_str(ulint_type v, char_type* buf) const
  {
    extern int snprintf_(char* buffer, size_t count, const char* format, ...);
    auto result = snprintf_(buf, bufsize_, "%lu", v);
    return {buf, buf + result};
  }

  template <typename char_type>
  boost::cnv::range<char_type*> to_str(sint_type v, char_type* buf) const
  {
    extern int snprintf_(char* buffer, size_t count, const char* format, ...);
    auto result = snprintf_(buf, bufsize_, "%hd", v);
    return {buf, buf + result};
  }

  template <typename char_type>
  boost::cnv::range<char_type*> to_str(usint_type v, char_type* buf) const
  {
    extern int snprintf_(char* buffer, size_t count, const char* format, ...);
    auto result = snprintf_(buf, bufsize_, "%hu", v);
    return {buf, buf + result};
  }

  template <typename char_type>
  boost::cnv::range<char_type*> to_str(llint_type v, char_type* buf) const
  {
    extern int snprintf_(char* buffer, size_t count, const char* format, ...);
    auto result = snprintf_(buf, bufsize_, "%lld", v);
    return {buf, buf + result};
  }

  template <typename char_type>
  boost::cnv::range<char_type*> to_str(ullint_type v, char_type* buf) const
  {
    extern int snprintf_(char* buffer, size_t count, const char* format, ...);
    auto result = snprintf_(buf, bufsize_, "%llu", v);
    return {buf, buf + result};
  }

  template <typename char_type, typename in_type,
      std::enable_if_t<std::is_floating_point_v<in_type>, void*> = nullptr>
  boost::cnv::range<char_type*> to_str(in_type v, char_type* buf) const
  {
    extern int snprintf_(char* buffer, size_t count, const char* format, ...);
    auto result = snprintf_(buf, bufsize_, "%f", v);
    return {buf, buf + result};
  }
};
