#pragma once

#include "button-intf.h"
#include "internal/button-constants.h"
#include "intrusive-forward-list.h"
#include "task-utility.h"

/**
 * @brief Task for providing button scanning for the stir plate project.
 */
class BUTTON_TASK final {
 public:
  BUTTON_TASK() = delete;
  /**
   * @brief Creates the button task for the stir plate project running at the provided priority.
   * @param taskPriority The OS priority to run this task.
   */
  explicit BUTTON_TASK(UBaseType_t taskPriority);

  /**
   * @brief Adds a button to be ticked by the task.
   * @param button The button to be run by this task.
   */
  void addButton(BUTTON_INTF& button);

 private:
  /// Periodic runtime of this task.
  static constexpr TASK_TICK_DURATION TASK_PERIOD{BUTTON_TICK_TIME};

  /// Worker function for this task.
  void workerTask();

  /// Class-local storage of task state provided to the OS.
  STATIC_TASK<256> my_task_storage;

  /// Buttons used by this project
  collection::intrusive_forward_list<BUTTON_INTF, BUTTON_INTF> my_buttons;
};
