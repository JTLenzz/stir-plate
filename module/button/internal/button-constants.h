#pragma once

#include <chrono>

/// Specifies the expected button polling interval.
static constexpr auto BUTTON_TICK_TIME = std::chrono::milliseconds{5};

/// The minimum required steady time to qualify a state change of the button.
static constexpr auto BUTTON_DEBOUNCE_TIME = std::chrono::milliseconds{50};

/// The (re)notify time of a hold event while the button is in the pressed state.
static constexpr auto BUTTON_HOLD_TIME = std::chrono::milliseconds{500};

static_assert((BUTTON_DEBOUNCE_TIME % BUTTON_TICK_TIME) == std::chrono::milliseconds::zero(),
    "Debounce filter time must be an even multiple of the button poll period, otherwise button "
    "detection time will be inaccurate.");

static_assert((BUTTON_HOLD_TIME % BUTTON_TICK_TIME) == std::chrono::milliseconds::zero(),
    "Button hold time must be an even multiple of the button poll period, otherwise hold "
    "notification times will be inaccurate.");
