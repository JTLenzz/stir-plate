#pragma once

class BUTTON_INTF;

/// Interface for objects that can observe the state of a button.
class BUTTON_OBSERVER_INTF {
 public:
  /**
   * @brief Notifies that the button has been pressed.
   * @param button The button publishing the notification.
   */
  virtual void onPress(BUTTON_INTF* button) = 0;

  /**
   * @brief Notifies that the button has been held.
   *
   * This notification will be called for each subsequent hold time until the button is released.
   *
   * @param button The button publishing the notification.
   */
  virtual void onHold(BUTTON_INTF* button) = 0;

  /**
   * @brief Notifies that the button has been released.
   * @param button The button publishing the notification.
   */
  virtual void onRelease(BUTTON_INTF* button) = 0;

 protected:
  BUTTON_OBSERVER_INTF() = default;
  ~BUTTON_OBSERVER_INTF() = default;
};
