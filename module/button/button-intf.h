#pragma once

#include "intrusive-forward-list.h"

class BUTTON_TASK;

/**
 * @brief Base level abstraction for a button input.
 */
class BUTTON_INTF : public collection::intrusive_forward_list_element<BUTTON_INTF> {
 public:
  BUTTON_INTF(BUTTON_INTF const &) = delete;
  BUTTON_INTF(BUTTON_INTF &&) = delete;

  /**
   * @brief Ticks the state machine of the button.
   *
   * This routine takes a new reading from the input pad and recalculates the button state.
   */
  virtual void tick() = 0;

 protected:
  BUTTON_INTF() = default;
  ~BUTTON_INTF() = default;
};
