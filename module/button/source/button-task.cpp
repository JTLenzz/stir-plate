#include "button-task.h"

BUTTON_TASK::BUTTON_TASK(UBaseType_t taskPriority)
    : my_task_storage([](void* instance) { static_cast<decltype(this)>(instance)->workerTask(); },
          "Buttons", this, taskPriority),
      my_buttons{}
{
}

void BUTTON_TASK::workerTask()
{
  TickType_t lastRunTime(xTaskGetTickCount());
  for (;;) {
    vTaskDelayUntil(&lastRunTime, TASK_PERIOD.count());
    for (auto& button : this->my_buttons) {
      button.tick();
    }
  }
}

void BUTTON_TASK::addButton(BUTTON_INTF& button)
{
  this->my_buttons.push_front(button);
}
