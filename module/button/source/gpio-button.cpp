#include "gpio-button.h"

#include "button-task.h"

GPIO_BUTTON::GPIO_BUTTON(CONFIG const &config, BUTTON_OBSERVER_INTF &notifiee, BUTTON_TASK &task)
    : BUTTON_INTF(), my_config(config), my_notifiee(notifiee)
{
  task.addButton(*this);
}

void GPIO_BUTTON::tick()
{
  bool pinState(!!palReadPad(this->my_config.port, this->my_config.pad) ^ this->my_config.inverted);

  if (this->my_debounce_state.add(pinState)) {
    if (this->my_debounce_state.is_repeating() || this->my_debounce_state.is_held()) {
      this->my_notifiee.onHold(this);
    }
    else if (this->my_debounce_state.is_set()) {
      this->my_notifiee.onPress(this);
    }
    else {
      this->my_notifiee.onRelease(this);
    }
  }
}
