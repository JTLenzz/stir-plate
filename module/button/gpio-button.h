#pragma once

#include "button-intf.h"
#include "button-observer-intf.h"
#include "etl/debounce.h"
#include "hal.h"
#include "internal/button-constants.h"

/**
 * @brief Button implementation for a button connected to a dedicated GPIO pin.
 */
class GPIO_BUTTON final : public BUTTON_INTF {
 public:
  /// The set of configuration options for a GPIO button.
  struct CONFIG {
    /// The port on which the button is connected.
    ioportid_t port;
    /// The pin number on which the button is connected.
    iopadid_t pad;
    /// Indicates of the input is inverted (i.e. active low).
    bool inverted;
  };

  /**
   * @brief Creates a new GPIO button object.
   * @param config The button configuration.
   * @param notifiee The object to notify on state changes.
   * @param task The task that runs the tick API for this button.
   */
  GPIO_BUTTON(CONFIG const &config, BUTTON_OBSERVER_INTF &notifiee, BUTTON_TASK &task);
  ~GPIO_BUTTON() = default;

  GPIO_BUTTON() = delete;
  GPIO_BUTTON(GPIO_BUTTON const &) = delete;
  GPIO_BUTTON(GPIO_BUTTON &&) = delete;
  GPIO_BUTTON &operator=(GPIO_BUTTON const &) = delete;
  GPIO_BUTTON &operator=(GPIO_BUTTON &&) = delete;

  void tick() override;

 private:
  /// Encapsulates the debounce state tracking of the button.
  etl::debounce<BUTTON_DEBOUNCE_TIME / BUTTON_TICK_TIME, BUTTON_HOLD_TIME / BUTTON_TICK_TIME,
      BUTTON_HOLD_TIME / BUTTON_TICK_TIME>
      my_debounce_state;
  /// The button configuration.
  CONFIG const &my_config;
  /// Object to notify for state changes of this object.
  BUTTON_OBSERVER_INTF &my_notifiee;
};