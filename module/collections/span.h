#pragma once

#include <boost/core/span.hpp>

namespace collection {
template <class T, size_t Extent = boost::dynamic_extent>
using span = boost::span<T, Extent>;

template <class T, std::size_t E>
static inline auto as_bytes(span<T, E> spn)
{
  return ::boost::as_bytes(spn);
}

template <class T, std::size_t E>
static inline auto as_writable_bytes(span<T, E> spn)
{
  return ::boost::as_writable_bytes(spn);
}

}  // namespace collection
