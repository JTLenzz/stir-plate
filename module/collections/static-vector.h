#pragma once

#include "etl/vector.h"

namespace collection {
template <class T, size_t MAX_CAPACITY>
using static_vector = etl::vector<T, MAX_CAPACITY>;
}  // namespace collection
