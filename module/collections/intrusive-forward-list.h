/**
 * @brief This file defines an intrusive forward (singly-linked) list collection.
 *
 * Intrusive collections differ from traditional collections in that actual objects (not copies) are
 * stored in the container, avoiding the need to dynamically allocate memory. Stated another way,
 * the unlike standard containers, intrusive containers are "non-owning" of their contained
 * members. As such, care needs to be taken to ensure the lifetime of the objects in a container is
 * shorter than the container itself.
 *
 * This wrapper works to simplify the otherwise verbose template definition for
 * @c boost::intrusive::slist using it in a way that is desirable for an embedded application.
 *
 * The interface should be compliant with this standard proposal:
 * http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0406r1.html
 */

#pragma once

// Defining BOOST_DISABLE_INVARIANT_ASSERT used to be sufficient to avoid assertions in the
// intrusive library, but raw assertions were added in boost 1.70 to some of the node conversion
// funtions. To avoid the bloat of assertions in the library, we have to disable all assertions
// before including.
#define BOOST_DISABLE_ASSERTS
#include <boost/intrusive/slist.hpp>

#include "internal/intrusive-tag.h"

namespace collection {
/**
 * @brief Definition of an intrusive forward list element.
 *
 * In order to be inserted into an intrusive list, a class needs to inherit from this one. This
 * class adds the necessary members necessary for being in a list that would usually be dynamically
 * allocated into a node for a standard library list.
 *
 * In order to save code space, this node type provides no protection for asserting object
 * lifetimes, and it is on the user to ensure the lifetime of the objects is shorter than that of
 * the collection (or that objects are removed from the collection prior to the collection being
 * destroyed).
 *
 * @param Tag Tag type used to change the element class definition for objects that can be inserted
 * into multiple forward lists.
 */
template <class Tag = default_intrusive_tag>
using intrusive_forward_list_element = boost::intrusive::slist_base_hook<boost::intrusive::tag<Tag>,
    boost::intrusive::link_mode<boost::intrusive::normal_link>>;

/**
 * @brief Definition of an intrusive forward list.
 * @tparam T The class contained by this list. T must derive from
 * @c intrusive_forward_list_element<Tag>.
 * @tparam Tag Tag type used for the element class tying the appropriate element data to this list.
 */
template <class T, class Tag = default_intrusive_tag>
using intrusive_forward_list =
    boost::intrusive::slist<T, boost::intrusive::base_hook<intrusive_forward_list_element<Tag>>>;
};  // namespace collection
