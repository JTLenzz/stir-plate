/**
 * @brief This file defines an intrusive set collection.
 *
 * Intrusive collections differ from traditional collections in that actual objects (not copies) are
 * stored in the container, avoiding the need to dynamically allocate memory. Stated another way,
 * the unlike standard containers, intrusive containers are "non-owning" of their contained
 * members. As such, care needs to be taken to ensure the lifetime of the objects in a container is
 * shorter than the container itself.
 *
 * This wrapper works to simplify the otherwise verbose template definition for
 * @c boost::intrusive::set using it in a way that is desirable for an embedded application.
 *
 * The interface should be compliant with this standard proposal:
 * http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2016/p0406r1.html
 */

#pragma once

// Defining BOOST_DISABLE_INVARIANT_ASSERT used to be sufficient to avoid assertions in the
// intrusive library, but raw assertions were added in boost 1.70 to some of the node conversion
// funtions. To avoid the bloat of assertions in the library, we have to disable all assertions
// before including.
#define BOOST_DISABLE_ASSERTS
#include <boost/intrusive/set.hpp>

#include "internal/intrusive-tag.h"

namespace collection {
/**
 * @brief Definition of an intrusive set element.
 *
 * In order to be inserted into an intrusive set, a class needs to inherit from this one. This
 * class adds the necessary members necessary for being in a set that would usually be dynamically
 * allocated into a node for a standard library set.
 *
 * In order to save code space, this node type provides no protection for asserting object
 * lifetimes, and it is on the user to ensure the lifetime of the objects is shorter than that of
 * the collection (or that objects are removed from the collection prior to the collection being
 * destroyed).
 *
 * @param Tag Tag type used to change the element class definition for objects that can be inserted
 * into multiple sets.
 */
template <class Tag = default_intrusive_tag>
using intrusive_set_element = boost::intrusive::set_base_hook<boost::intrusive::tag<Tag>,
    boost::intrusive::link_mode<boost::intrusive::normal_link>,
    boost::intrusive::optimize_size<true>>;

/**
 * @brief Definition of an intrusive set.
 * @tparam T The class contained by this set. T must derive from @c intrusive_set_element<Tag>.
 * @tparam Tag Tag type used for the element class tying the appropriate element data to this
 * collection.
 */
template <class T, class Tag = default_intrusive_tag, class Compare = std::less<T>>
using intrusive_set = boost::intrusive::set<T,
    boost::intrusive::base_hook<intrusive_set_element<Tag>>, boost::intrusive::compare<Compare>>;

/**
 * @brief Definition of an intrusive multiset.
 * @tparam T The class contained by this set. T must derive from @c intrusive_set_element<Tag>.
 * @tparam Tag Tag type used for the element class tying the appropriate element data to this
 * collection.
 */
template <class T, class Tag = default_intrusive_tag, class Compare = std::less<T>>
using intrusive_multiset = boost::intrusive::multiset<T,
    boost::intrusive::base_hook<intrusive_set_element<Tag>>, boost::intrusive::compare<Compare>>;

/**
 * @brief Definition of an intrusive map.
 * @tparam T The class contained by this map. T must derive from @c intrusive_set_element<Tag>.
 * @tparam KEY_FUNCTOR A default construtable type that provides the key type and an operator()
 * function taking a T that returns that type.
 * @tparam Tag Tag type used for the element class tying the appropriate element data to this
 * collection.
 */
template <class T, class KEY_FUNCTOR, class Tag = default_intrusive_tag,
    class Compare = std::less<typename KEY_FUNCTOR::type>>
using intrusive_map = boost::intrusive::set<T, boost::intrusive::key_of_value<KEY_FUNCTOR>,
    boost::intrusive::base_hook<intrusive_set_element<Tag>>, boost::intrusive::compare<Compare>>;

/**
 * @brief Definition of an intrusive multimap.
 * @tparam T The class contained by this map. T must derive from @c intrusive_set_element<Tag>.
 * @tparam KEY_FUNCTOR A default construtable type that provides the key type and an operator()
 * function taking a T that returns that type.
 * @tparam Tag Tag type used for the element class tying the appropriate element data to this
 * collection.
 */
template <class T, class KEY_FUNCTOR, class Tag = default_intrusive_tag,
    class Compare = std::less<typename KEY_FUNCTOR::type>>
using intrusive_multimap = boost::intrusive::multiset<T,
    boost::intrusive::key_of_value<KEY_FUNCTOR>,
    boost::intrusive::base_hook<intrusive_set_element<Tag>>, boost::intrusive::compare<Compare>>;
};  // namespace collection
