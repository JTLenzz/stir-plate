#pragma once

namespace collection {
/// A default element tag type used if no tag type is provided.
struct default_intrusive_tag;
}  // namespace collection
