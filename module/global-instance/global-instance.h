#pragma once

#include <new>
#include <type_traits>

/**
 * @brief Global class instance helper class that allows the programmer to control when the object
 * gets constructed, avoiding (or at least allowing the programmer to control) the static
 * initialization order fiasco.
 */
template <class T>
class GLOBAL_INSTANCE final {
 public:
  GLOBAL_INSTANCE() = default;

  // A proper destructor would call the destructor on the contained object blob if it was created,
  // however this would cause instances of this class to be registered for static destruction when
  // declared at global scope, which is ultimately unnecessary. So long as this class is used to
  // create instances that have lifetime equal to that of the program, this should be sufficient and
  // avoid pulling in additional library functions.
  ~GLOBAL_INSTANCE() = default;

  GLOBAL_INSTANCE(GLOBAL_INSTANCE const &) = delete;
  GLOBAL_INSTANCE(GLOBAL_INSTANCE &&) = delete;

  /**
   * @brief Creates the contained object using the provided constructor parameters.
   * @tparam Args The argument types.
   * @param args The arguments used to construct the object.
   * @note Not thread safe, expected to be called prior to the scheduler starting.
   */
  template <typename... Args,
      std::enable_if_t<std::is_constructible_v<T, Args &&...>, void *> = nullptr>
  void create(Args &&...args)
  {
    static_cast<void>(::new (std::addressof(this->my_storage)) T(std::forward<Args>(args)...));
  }

  /**
   * @brief Gets the constructed instance.
   * @pre Object must be created via @c create().
   * @return The contained object.
   */
  T &get()
  {
    return *std::launder(reinterpret_cast<T *>(std::addressof(this->my_storage)));
  }

  /**
   * @brief Gets the constructed instance (const qualified).
   * @pre Object must be created via @c create().
   * @return The contained object.
   */
  T const &get() const
  {
    return *std::launder(reinterpret_cast<T const *>(std::addressof(this->my_storage)));
  }

 private:
  /// RAM allocated for constructing the object into.
  std::aligned_storage_t<sizeof(T), alignof(T)> my_storage;
};
