#pragma once

#include "FreeRTOS.h"
#include "task.h"

/**
 * @brief Class designed to reduce boilerplate required for creating a statically allocated task in
 * FreeRTOS.
 * @tparam STACK_SIZE size of the task's stack in *bytes*. Will be rounded up appropriately to the
 * correct aligned size, making this a minimum size.
 */
template <configSTACK_DEPTH_TYPE STACK_SIZE>
class STATIC_TASK final {
 public:
  STATIC_TASK() = delete;
  STATIC_TASK(TaskFunction_t workerFunction, char const* taskName, void* workerFunctionInput,
      UBaseType_t taskPriority)
      : my_handle(
            xTaskCreateStatic(workerFunction, taskName, sizeof(my_stack) / sizeof(my_stack[0]),
                workerFunctionInput, taskPriority, &my_stack[0], &my_storage))
  {
    static_assert(std::extent_v<decltype(my_stack)> >= configMINIMAL_STACK_SIZE,
        "Cannot make a task with a stack smaller than the minimum stack size.");
  }

  ~STATIC_TASK()
  {
    vTaskDelete(my_handle);
  }

  TaskHandle_t getTaskHandle() const
  {
    return this->my_handle;
  }

 private:
  StaticTask_t my_storage;
  StackType_t my_stack[(STACK_SIZE + (sizeof(StackType_t) - 1)) / sizeof(StackType_t)];
  TaskHandle_t my_handle;
};
