#pragma once

#include "FreeRTOS.h"
#include "semphr.h"

class STATIC_BINARY_SEMAPHORE {
 public:
  STATIC_BINARY_SEMAPHORE() : my_handle(xSemaphoreCreateBinaryStatic(&my_semaphore_storage))
  {
  }

  ~STATIC_BINARY_SEMAPHORE()
  {
    vSemaphoreDelete(this->my_handle);
  }

  SemaphoreHandle_t getHandle()
  {
    return this->my_handle;
  }

 private:
  SemaphoreHandle_t my_handle;
  StaticSemaphore_t my_semaphore_storage;
};
