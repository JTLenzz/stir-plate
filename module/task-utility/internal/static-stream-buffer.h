#include "FreeRTOS.h"
#include "stream_buffer.h"

template <size_t BUFFER_SIZE>
class STATIC_STREAM_BUFFER {
 public:
  STATIC_STREAM_BUFFER()
      : my_handle(xStreamBufferCreateStatic(
            BUFFER_SIZE, 1, &my_storage_buffer[0], &my_stream_buffer_storage))
  {
  }

  ~STATIC_STREAM_BUFFER()
  {
    vStreamBufferDelete(this->my_handle);
  }

  StreamBufferHandle_t getHandle()
  {
    return this->my_handle;
  }

 private:
  StreamBufferHandle_t my_handle;
  uint8_t my_storage_buffer[BUFFER_SIZE];
  StaticStreamBuffer_t my_stream_buffer_storage;
};
