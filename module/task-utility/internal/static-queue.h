#pragma once

#include <optional>
#include <thread>

#include "FreeRTOS.h"
#include "queue.h"

template <class T, size_t QUEUE_SIZE>
class STATIC_QUEUE {
 public:
  static_assert(std::is_trivial_v<T>,
      "FreeRTOS queues cannot be used for non-POD types. Consider queuing a pointer instead.");

  STATIC_QUEUE()
      : my_handle(xQueueCreateStatic(QUEUE_SIZE, sizeof(T), &my_queue_buffer[0], &my_queue_storage))
  {
  }

  ~STATIC_QUEUE()
  {
    vQueueDelete(this->my_handle);
  }

  QueueHandle_t getHandle()
  {
    return this->my_handle;
  }

 private:
  QueueHandle_t my_handle;
  alignas(alignof(T)) uint8_t my_queue_buffer[QUEUE_SIZE * sizeof(T)];
  StaticQueue_t my_queue_storage;
};
