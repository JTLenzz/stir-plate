#pragma once

#include "FreeRTOS.h"
#include "semphr.h"

class STATIC_MUTEX final {
 public:
  STATIC_MUTEX() : my_mutex{xSemaphoreCreateRecursiveMutexStatic(&my_buffer)}
  {
  }

  STATIC_MUTEX(STATIC_MUTEX const &) = delete;
  STATIC_MUTEX &operator=(STATIC_MUTEX const &) = delete;

  ~STATIC_MUTEX()
  {
    vSemaphoreDelete(this->my_mutex);
  }

  SemaphoreHandle_t getHandle()
  {
    return this->my_mutex;
  }

  void lock() const
  {
    xSemaphoreTakeRecursive(this->my_mutex, portMAX_DELAY);
  }

  void unlock() const
  {
    xSemaphoreGiveRecursive(this->my_mutex);
  }

 private:
  SemaphoreHandle_t my_mutex;
  StaticSemaphore_t my_buffer;
};
