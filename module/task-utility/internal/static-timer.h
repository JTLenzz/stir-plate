#pragma once

#include "FreeRTOS.h"
#include "internal/tick-duration.h"
#include "timers.h"

class STATIC_TIMER {
 public:
  STATIC_TIMER() = delete;

  STATIC_TIMER(char const* timerName, TASK_TICK_DURATION period, bool autoReload,
      void* timerContext, TimerCallbackFunction_t callback)
      : my_handle(xTimerCreateStatic(timerName, period.count(), autoReload ? pdTRUE : pdFALSE,
            timerContext, callback, &my_storage))
  {
    // This function intentionally left blank
  }

  ~STATIC_TIMER()
  {
    xTimerDelete(this->my_handle, 0);
  }

  TimerHandle_t getHandle()
  {
    return this->my_handle;
  }

 private:
  TimerHandle_t my_handle;
  StaticTimer_t my_storage;
};
