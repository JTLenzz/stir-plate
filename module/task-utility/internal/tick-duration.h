#pragma once

#include <chrono>

#include "FreeRTOS.h"

/// Standard duration type that can be used with the time values provided by the OS.
using TASK_TICK_DURATION = std::chrono::duration<TickType_t, std::ratio<1, configTICK_RATE_HZ>>;