#pragma once

#include "FreeRTOS.h"
#include "event_groups.h"

/**
 * @brief Class designed to reduce boilerplate required for creating a statically allocated event
 * group in FreeRTOS.
 */
class STATIC_EVENT_GROUP final {
 public:
  STATIC_EVENT_GROUP() : my_handle(xEventGroupCreateStatic(&my_storage))
  {
  }

  ~STATIC_EVENT_GROUP()
  {
    vEventGroupDelete(my_handle);
  }

  EventGroupHandle_t getEventGroupHandle() const
  {
    return this->my_handle;
  }

 private:
  StaticEventGroup_t my_storage;
  EventGroupHandle_t my_handle;
};
