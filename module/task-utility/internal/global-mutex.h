#pragma once

#include <mutex>

#include "FreeRTOS.h"
#include "task.h"

/// Mutex-like object used from task context to prevent a scheduler context switch.
class TASK_SCHEDULER_MUTEX final {
 public:
  TASK_SCHEDULER_MUTEX(TASK_SCHEDULER_MUTEX const &) = delete;
  TASK_SCHEDULER_MUTEX &operator=(TASK_SCHEDULER_MUTEX const &) = delete;

  void lock() const
  {
    vTaskSuspendAll();
  }

  void unlock() const
  {
    static_cast<void>(xTaskResumeAll());
  }

  static TASK_SCHEDULER_MUTEX &getInstance();

 private:
  TASK_SCHEDULER_MUTEX() = default;
};

/// Mutex-like object used from task context used to prevent interrupts from running.
struct TASK_CRITICAL_MUTEX final {
 public:
  TASK_CRITICAL_MUTEX(TASK_CRITICAL_MUTEX const &) = delete;
  TASK_CRITICAL_MUTEX &operator=(TASK_CRITICAL_MUTEX const &) = delete;

  void lock()
  {
    taskENTER_CRITICAL();
  }

  void unlock()
  {
    taskEXIT_CRITICAL();
  }

  static TASK_CRITICAL_MUTEX &getInstance();

 private:
  TASK_CRITICAL_MUTEX() = default;
};
