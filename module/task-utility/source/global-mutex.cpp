#include "internal/global-mutex.h"

TASK_SCHEDULER_MUTEX& TASK_SCHEDULER_MUTEX::getInstance()
{
  static TASK_SCHEDULER_MUTEX instance;
  return instance;
}

TASK_CRITICAL_MUTEX& TASK_CRITICAL_MUTEX::getInstance()
{
  static TASK_CRITICAL_MUTEX instance;
  return instance;
}
