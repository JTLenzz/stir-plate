#include "internal/ugfx-display.h"

#include <algorithm>
#include <cinttypes>
#include <new>

#include "gfx.h"
#include "internal/button-observers.h"
#include "internal/fan/constant-drive-strategy.h"
#include "internal/fan/constant-rpm-strategy.h"
#include "internal/fan/fan-control.h"
#include "printf/printf.h"

UGFX_DISPLAY::UGFX_DISPLAY(UBaseType_t taskPriority, FAN_CONTROL &fanControl,
    CONSTANT_DRIVE_FAN_CONTROL_STRATEGY &constantDriveStrategy,
    CONSTANT_RPM_FAN_CONTROL_STRATEGY &constantRpmStrategy)
    : my_task([](void *instance) { static_cast<decltype(this)>(instance)->workerTask(); },
          "uGFX Display", this, taskPriority),
      my_event_queue{},
      my_refresh_timer("uGFX Refresh", std::chrono::milliseconds{100}, true, this,
          [](TimerHandle_t xTimer) {
            static_cast<decltype(this)>(pvTimerGetTimerID(xTimer))->onRefreshTimerExpired();
          }),
      my_fan_control(fanControl),
      my_constant_drive_fan_control_strategy(constantDriveStrategy),
      my_constant_rpm_fan_control_strategy(constantRpmStrategy)
{
}

void UGFX_DISPLAY::onButtonEvent(BUTTON_EVENT buttonEvent)
{
  QUEUED_DISPLAY_EVENT queuedEvent;
  queuedEvent.event = DISPLAY_EVENT::BUTTON;
  ::new (std::addressof(queuedEvent.data)) BUTTON_EVENT(buttonEvent);
  xQueueSendToBack(this->my_event_queue.getHandle(), &queuedEvent, 0);
}

void UGFX_DISPLAY::onRefreshTimerExpired()
{
  QUEUED_DISPLAY_EVENT queuedEvent;
  queuedEvent.event = DISPLAY_EVENT::REFRESH;
  xQueueSendToBack(this->my_event_queue.getHandle(), &queuedEvent, 0);
}

void UGFX_DISPLAY::workerTask()
{
  gfxInit();

  const auto font(gdispOpenFont("UI2"));
  const auto lineSpacing(
      gdispGetFontMetric(font, gFontHeight) + gdispGetFontMetric(font, gFontDescendersHeight));

  xTimerStart(this->my_refresh_timer.getHandle(), portMAX_DELAY);

  for (;;) {
    xQueueReceive(this->my_event_queue.getHandle(), &this->my_processing_event, portMAX_DELAY);
    switch (this->my_processing_event.event) {
      case DISPLAY_EVENT::BUTTON: {
        auto buttonEvent(reinterpret_cast<BUTTON_EVENT *>(&this->my_processing_event.data));
        switch (*buttonEvent) {
          case BUTTON_EVENT::CENTER_RELEASE:
            this->my_fan_control.setEnable(!this->my_fan_control.getEnable());
            break;

          case BUTTON_EVENT::UP_PRESS:
          case BUTTON_EVENT::UP_HOLD: {
            if (this->my_fan_control.getControlStrategy() ==
                std::addressof(this->my_constant_drive_fan_control_strategy)) {
              this->my_constant_drive_fan_control_strategy.setTargetDriveStrength(
                  std::min(
                      this->my_constant_drive_fan_control_strategy.getTargetDriveStrength() + 5,
                      100),
                  100);
            }
            else if (this->my_fan_control.getControlStrategy() ==
                     std::addressof(this->my_constant_rpm_fan_control_strategy)) {
              this->my_constant_rpm_fan_control_strategy.setTargetRotationsPerMinute(
                  this->my_constant_rpm_fan_control_strategy.getTargetRotationsPerMinute() + 50);
            }
            break;
          }

          case BUTTON_EVENT::DOWN_PRESS:
          case BUTTON_EVENT::DOWN_HOLD: {
            if (this->my_fan_control.getControlStrategy() ==
                std::addressof(this->my_constant_drive_fan_control_strategy)) {
              this->my_constant_drive_fan_control_strategy.setTargetDriveStrength(
                  std::max(
                      static_cast<int8_t>(
                          this->my_constant_drive_fan_control_strategy.getTargetDriveStrength()) -
                          5,
                      0),
                  100);
            }
            else if (this->my_fan_control.getControlStrategy() ==
                     std::addressof(this->my_constant_rpm_fan_control_strategy)) {
              this->my_constant_rpm_fan_control_strategy.setTargetRotationsPerMinute(std::max(
                  static_cast<int16_t>(
                      this->my_constant_rpm_fan_control_strategy.getTargetRotationsPerMinute()) -
                      50,
                  0));
            }
            break;
          }

          case BUTTON_EVENT::RIGHT_PRESS:
          case BUTTON_EVENT::LEFT_PRESS: {
            if (this->my_fan_control.getControlStrategy() ==
                std::addressof(this->my_constant_drive_fan_control_strategy)) {
              this->my_fan_control.setControlStrategy(
                  std::addressof(this->my_constant_rpm_fan_control_strategy));
            }
            else {
              this->my_fan_control.setControlStrategy(
                  std::addressof(this->my_constant_drive_fan_control_strategy));
            }
            break;
          }

          default:
            break;
        }
        break;
      }

      case DISPLAY_EVENT::REFRESH: {
        gCoord yIter(0);

        snprintf(this->my_string_scratch_buffer, sizeof(this->my_string_scratch_buffer),
            "State: %s", this->my_fan_control.getEnable() ? "Enabled" : "Disabled");
        gdispFillStringBox(0, yIter, gdispGetWidth(), lineSpacing, this->my_string_scratch_buffer,
            font, GFX_WHITE, GFX_BLACK, gJustifyLeft);
        yIter += lineSpacing;

        snprintf(this->my_string_scratch_buffer, sizeof(this->my_string_scratch_buffer),
            "Measured RPM: %" PRIu16, this->my_fan_control.getCurrentRotationsPerMinute());
        gdispFillStringBox(0, yIter, gdispGetWidth(), lineSpacing, this->my_string_scratch_buffer,
            font, GFX_WHITE, GFX_BLACK, gJustifyLeft);
        yIter += lineSpacing;

        if (this->my_fan_control.getControlStrategy() ==
            std::addressof(this->my_constant_drive_fan_control_strategy)) {
          snprintf(this->my_string_scratch_buffer, sizeof(this->my_string_scratch_buffer),
              "Target Strength: %" PRIu8 "%%",
              this->my_constant_drive_fan_control_strategy.getTargetDriveStrength());
          gdispFillStringBox(0, yIter, gdispGetWidth(), lineSpacing, this->my_string_scratch_buffer,
              font, GFX_WHITE, GFX_BLACK, gJustifyLeft);
        }
        else if (this->my_fan_control.getControlStrategy() ==
                 std::addressof(this->my_constant_rpm_fan_control_strategy)) {
          snprintf(this->my_string_scratch_buffer, sizeof(this->my_string_scratch_buffer),
              "Target RPM: %" PRIu16,
              this->my_constant_rpm_fan_control_strategy.getTargetRotationsPerMinute());
          gdispFillStringBox(0, yIter, gdispGetWidth(), lineSpacing, this->my_string_scratch_buffer,
              font, GFX_WHITE, GFX_BLACK, gJustifyLeft);
        }
        yIter += lineSpacing;

        snprintf(this->my_string_scratch_buffer, sizeof(this->my_string_scratch_buffer),
            "Current Strength: %" PRIu8 "%%", this->my_fan_control.getCurrentDriveStrength());
        gdispFillStringBox(0, yIter, gdispGetWidth(), lineSpacing, this->my_string_scratch_buffer,
            font, GFX_WHITE, GFX_BLACK, gJustifyLeft);
        yIter += lineSpacing;

        gdispFlush();
        break;
      }

      default:
        break;
    }
  }
}
