#include "fan-command.h"

#include <array>
#include <cinttypes>

#include "constant-drive-strategy.h"
#include "constant-rpm-strategy.h"
#include "fan-control.h"
#include "shell-converter.h"
#include "shell.h"

struct boost::cnv::by_default : public SHELL_CONVERTER {
};

FAN_COMMAND::FAN_COMMAND(SHELL &shell, FAN_CONTROL &fanController,
    CONSTANT_DRIVE_FAN_CONTROL_STRATEGY &constantDriveStrategy,
    CONSTANT_RPM_FAN_CONTROL_STRATEGY &constantRpmStrategy)
    : SHELL_COMMAND_INTF(),
      my_fan_controller(fanController),
      my_constant_drive_fan_control_strategy(constantDriveStrategy),
      my_constant_rpm_fan_control_strategy(constantRpmStrategy)
{
  shell.addCommand(*this);
}

void FAN_COMMAND::handle(SHELL const &shell, collection::span<const std::string_view> args)
{
  if (args.size() < 2) {
    this->printUsage(args[0], shell);
    return;
  }

  if (args[1] == "get") {
    shell.print("State: %s\nPWM Strength: %" PRIu8 "%%\nRPM: %" PRIu16 "\n",
        this->my_fan_controller.getEnable() ? "Enabled" : "Disabled",
        this->my_fan_controller.getCurrentDriveStrength(),
        this->my_fan_controller.getCurrentRotationsPerMinute());
    if (this->my_fan_controller.getControlStrategy() ==
        std::addressof(this->my_constant_drive_fan_control_strategy)) {
      shell.print("Target PWM Strength: %" PRIu8 "%%\n",
          this->my_constant_drive_fan_control_strategy.getTargetDriveStrength());
    }
    else if (this->my_fan_controller.getControlStrategy() ==
             std::addressof(this->my_constant_rpm_fan_control_strategy)) {
      shell.print("Target RPM: %" PRIu16 "\n",
          this->my_constant_rpm_fan_control_strategy.getTargetRotationsPerMinute());
      auto [Kp, Ki, Kd] =
          this->my_constant_rpm_fan_control_strategy.getFeedbackControllerParameters();
      shell.print("Feedback Parameters:\n   Kp: %.2f\n   Ki: %.2f\n   Kd: %.2f\n", Kp, Ki, Kd);
    }
    else {
      shell.print("Unknown fan control strategy\n");
    }
  }
  else if (args[1] == "enable") {
    this->my_fan_controller.setEnable(true);
  }
  else if (args[1] == "disable") {
    this->my_fan_controller.setEnable(false);
  }
  else if ((args[1] == "set-pwm") && (args.size() >= 3)) {
    if (const auto pwm{boost::convert<uint16_t>(args[2])}; pwm) {
      if (*pwm <= 100) {
        this->my_constant_drive_fan_control_strategy.setTargetDriveStrength(*pwm, 100);
        this->my_fan_controller.setControlStrategy(
            std::addressof(this->my_constant_drive_fan_control_strategy));
      }
      else {
        shell.print("Invalid PWM value: %" PRIu16 "\n", *pwm);
      }
    }
    else {
      shell.print("Error reading PWM value.\n");
    }
  }
  else if ((args[1] == "set-rpm") && (args.size() >= 3)) {
    if (const auto rpm{boost::convert<uint16_t>(args[2])}; rpm) {
      if (*rpm <= 2000) {
        this->my_constant_rpm_fan_control_strategy.setTargetRotationsPerMinute(*rpm);
        this->my_fan_controller.setControlStrategy(
            std::addressof(this->my_constant_rpm_fan_control_strategy));
      }
      else {
        shell.print("Invalid rotations per minute value: %" PRIu16 "\n", *rpm);
      }
    }
    else {
      shell.print("Error reading rotations per minute value.\n");
    }
  }
  else if ((args[1] == "set-pid") && (args.size() >= 5)) {
    std::array<float, 3> gains;

    for (decltype(gains)::size_type i = 0; i < gains.size(); ++i) {
      if (const auto result{boost::convert<float>(args[i + 2])}; result) {
        gains[i] = *result;
      }
      else {
        shell.print("Unable to parse %s gain value: %.*s\n",
            (i == 0) ? "proportional" : ((i == 1) ? "integral" : "derivative"), args[i + 2].size(),
            args[i + 2].begin());
        return;
      }
    }

    this->my_constant_rpm_fan_control_strategy.setFeedbackControllerParameters(
        gains[0], gains[1], gains[2]);
  }
  else {
    this->printUsage(args[0], shell);
  }
}

std::string_view FAN_COMMAND::name() const
{
  return "fan";
}

void FAN_COMMAND::printUsage(std::string_view command, SHELL const &shell)
{
  static char const *const usage =
      "%.*s\n"
      "Utilities for manipulating the fan.\n"
      "\n"
      "USAGE:\n"
      "    %.*s set-pwm <pwm-percent>\n"
      "    %.*s set-rpm <rpm>\n"
      "    %.*s set-pid <Kp> <Ki> <Kd>\n"
      "    %.*s get\n"
      "    %.*s enable\n"
      "    %.*s disable\n"
      "\n"
      "COMMANDS:\n"
      "    set-pwm Set the fan PWM strength.\n"
      "    set-rpm Set the target fan rotations per minute.\n"
      "    get     Get the current fan status.\n"
      "    enable  Enable fan output.\n"
      "    disable Disable fan output.\n"
      "\n"
      "ARGS:\n"
      "    <pwm-percent>  A PWM drive strength from 0 to 100 (inclusive).\n"
      "    <rpm>          A rotations per minute value from 0 to 2000 (inclusive).\n"
      "    <Kx>           A gain value for the specified PID stage (ex. 2.5).\n";
  shell.print(usage, command.size(), command.begin(), command.size(), command.begin(),
      command.size(), command.begin(), command.size(), command.begin(), command.size(),
      command.begin(), command.size(), command.begin(), command.size(), command.begin());
}
