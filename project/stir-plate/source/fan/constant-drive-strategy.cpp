#include "constant-drive-strategy.h"

#include "fan-control.h"

CONSTANT_DRIVE_FAN_CONTROL_STRATEGY::CONSTANT_DRIVE_FAN_CONTROL_STRATEGY()
    : FAN_CONTROL_STRATEGY_INTF(), my_target_pwm(CONTROL_TICK_DURATION::zero())
{
}

CONTROL_TICK_DURATION CONSTANT_DRIVE_FAN_CONTROL_STRATEGY::calculateFanSpeed(
    FAN_CONTROL const& controller)
{
  return controller.getEnable() ? this->my_target_pwm.load(std::memory_order_relaxed)
                                : MINIMUM_PWM_PULSE_WIDTH;
}

void CONSTANT_DRIVE_FAN_CONTROL_STRATEGY::setTargetDriveStrength(
    uint16_t numerator, uint16_t denominator)
{
  const auto targetPwm((CONTROL_PWM_PERIOD * numerator) / denominator);
  this->my_target_pwm.store(
      std::min(std::max(MINIMUM_PWM_PULSE_WIDTH, targetPwm), MAXIMUM_PWM_PULSE_WIDTH),
      std::memory_order_relaxed);
}

uint8_t CONSTANT_DRIVE_FAN_CONTROL_STRATEGY::getTargetDriveStrength() const
{
  return (this->my_target_pwm.load(std::memory_order_relaxed) * 100) / CONTROL_PWM_PERIOD;
}
