#include "constant-rpm-strategy.h"

#include "fan-control.h"

CONSTANT_RPM_FAN_CONTROL_STRATEGY::CONSTANT_RPM_FAN_CONTROL_STRATEGY()
    : FAN_CONTROL_STRATEGY_INTF(),
      my_target_rpm(1000),
      my_feedback_controller_mutex{},
      my_feedback_controller{}
{
  my_feedback_controller.Kp = 0.5;
  my_feedback_controller.Ki = 0.1;
  my_feedback_controller.Kd = 0.0;
  arm_pid_init_f32(&my_feedback_controller, 1);
}

CONTROL_TICK_DURATION CONSTANT_RPM_FAN_CONTROL_STRATEGY::calculateFanSpeed(
    FAN_CONTROL const& controller)
{
  std::scoped_lock l(this->my_feedback_controller_mutex);

  if (!controller.getEnable()) {
    arm_pid_reset_f32(&this->my_feedback_controller);
    return MINIMUM_PWM_PULSE_WIDTH;
  }
  else {
    const auto error =
        static_cast<int16_t>(this->my_target_rpm) - controller.getCurrentRotationsPerMinute();
    return std::min(std::max(CONTROL_TICK_DURATION(static_cast<uint32_t>(
                                 std::max(arm_pid_f32(&this->my_feedback_controller, error),
                                     static_cast<float32_t>(MINIMUM_PWM_PULSE_WIDTH.count())))),
                        MINIMUM_PWM_PULSE_WIDTH),
        MAXIMUM_PWM_PULSE_WIDTH);
  }
}

void CONSTANT_RPM_FAN_CONTROL_STRATEGY::setTargetRotationsPerMinute(uint16_t rpm)
{
  this->my_target_rpm.store(rpm, std::memory_order_relaxed);
}

uint16_t CONSTANT_RPM_FAN_CONTROL_STRATEGY::getTargetRotationsPerMinute() const
{
  return this->my_target_rpm.load(std::memory_order_relaxed);
}

void CONSTANT_RPM_FAN_CONTROL_STRATEGY::setFeedbackControllerParameters(
    float32_t proportionalGain, float32_t integralGain, float32_t derivativeGain)
{
  if (std::isfinite(proportionalGain) && std::isfinite(integralGain) &&
      std::isfinite(derivativeGain)) {
    std::scoped_lock l(this->my_feedback_controller_mutex);

    this->my_feedback_controller.Kp = proportionalGain;
    this->my_feedback_controller.Ki = integralGain;
    this->my_feedback_controller.Kd = derivativeGain;
    arm_pid_init_f32(&my_feedback_controller, 0);
  }
}

std::tuple<float32_t, float32_t, float32_t>
CONSTANT_RPM_FAN_CONTROL_STRATEGY::getFeedbackControllerParameters() const
{
  std::scoped_lock l(this->my_feedback_controller_mutex);

  return std::make_tuple(this->my_feedback_controller.Kp, this->my_feedback_controller.Ki,
      this->my_feedback_controller.Kd);
}
