#include "fan-control.h"

#include <limits>

#include "fan-control-strategy-intf.h"

FAN_CONTROL::FAN_CONTROL(
    UBaseType_t taskPriority, PWMDriver& fanControlDriver, ICUDriver& fanFeedbackDriver)
    : my_task_storage([](void* instance) { static_cast<decltype(this)>(instance)->workerTask(); },
          "Fan Control", this, taskPriority),
      my_enabled(false),
      my_fan_control_driver(fanControlDriver),
      my_fan_feedback_driver(fanFeedbackDriver),
      my_filtered_rotation_time(UINT32_C(0)),
      my_control_strategy(nullptr)
{
  my_fan_feedback_driver.parent_object = this;
}

void FAN_CONTROL::workerTask()
{
  // The tachometer input is open drain, so the pull down is active and the pull up is weak. Since
  // the PWM line can generate noise on the positive edge, configure the ICU to track the falling
  // edge instead.
  static const ICUConfig feedbackConfig{ICU_INPUT_ACTIVE_LOW, FEEDBACK_TICK_DURATION::period::den,
      [](ICUDriver* driver) {
        static_cast<decltype(this)>(driver->parent_object)->onWidthUpdate(icuGetWidthX(driver));
      },
      [](ICUDriver* driver) {
        static_cast<decltype(this)>(driver->parent_object)->onPeriodUpdate(icuGetPeriodX(driver));
      },
      [](ICUDriver* driver) {
        static_cast<decltype(this)>(driver->parent_object)->onOverflowUpdate();
      },
      ICU_CHANNEL_2, 0};
  icuStart(&this->my_fan_feedback_driver, &feedbackConfig);

  // Filter the heck out of the rising edge in hardware too, since a false positive edge will also
  // trigger a false negative edge.
  /// @todo This may be a bit much especially if we are using RPM as an input to a control loop, so
  /// I may want to double check this.
  this->my_fan_feedback_driver.tim->CCMR1 |= STM32_TIM_CCMR1_IC1F(15) | STM32_TIM_CCMR1_IC2F(15);

  // The default auto reload time is not long enough for our expected rotation time. The only
  // expected cause of overflow is if the fan is stalling or stopping. The minimum when on should be
  // about 800, so setting the overflow to one second should be more than sufficient.
  this->my_fan_feedback_driver.tim->ARR = FEEDBACK_TICK_DURATION(std::chrono::seconds{1}).count();

  icuStartCapture(&this->my_fan_feedback_driver);
  icuEnableNotifications(&this->my_fan_feedback_driver);

  pwmStart(&this->my_fan_control_driver, &CONTROL_CONFIG);
  pwmEnableChannel(&this->my_fan_control_driver, CONTROL_CHANNEL, MINIMUM_PWM_PULSE_WIDTH.count());

  TickType_t lastWakeTime = xTaskGetTickCount();

  for (;;) {
    vTaskDelayUntil(&lastWakeTime, TASK_PERIOD.count());

    // This still assumes that the strategy object lifetimes are static. So if the strategy is
    // changed after the current strategy is loaded, then it is still valid to use the strategy.
    // Said another way, the strategies are not declared dynamically and their lifetime cannot
    // expire after this task reads a valid pointer to one.
    auto strategy = this->my_control_strategy.load(std::memory_order_relaxed);

    if (strategy != nullptr) {
      pwmEnableChannel(&this->my_fan_control_driver, CONTROL_CHANNEL,
          strategy->calculateFanSpeed(*this).count());
    }
    else {
      // If no strategy is specified, then just turn off.
      pwmEnableChannel(
          &this->my_fan_control_driver, CONTROL_CHANNEL, MINIMUM_PWM_PULSE_WIDTH.count());
    }
  }
}

void FAN_CONTROL::onWidthUpdate(uint32_t /* timerCaptureValue */)
{
  // The input signal should nearly have a 50% duty cycle, so we shouldn't need to care about the
  // pulse width. It may be used as an extra point to see if the fan is speeding up or slowing down.
}

void FAN_CONTROL::onPeriodUpdate(uint32_t timerCaptureValue)
{
  // The fan provides 2 pulses per revolution, so multiply the provided result by 2 to get the full
  // rotation time.
  this->my_filtered_rotation_time.add(timerCaptureValue * 2);
}

void FAN_CONTROL::onOverflowUpdate()
{
  /// @todo Use for stall detection.
  this->my_filtered_rotation_time.clear(UINT32_C(0));
}
