#include "gpio-button.h"
#include "hal.h"

extern const SPIConfig displayConfig{false, nullptr, GPIOA, PAL_PORT_BIT(GPIOA_DISPLAY_CS),
    (SPI_CR1_MSTR | SPI_CR1_SSI | SPI_CR1_SSM | SPI_CR1_BR_1), 0};

extern const SerialConfig shellConfig{38400, 0, 0, 0};
extern const GPIO_BUTTON::CONFIG upConfig = {GPIOB, GPIOB_BUTTON_UP, true};
extern const GPIO_BUTTON::CONFIG leftConfig = {GPIOB, GPIOB_BUTTON_LEFT, true};
extern const GPIO_BUTTON::CONFIG centerConfig = {GPIOA, GPIOA_BUTTON_CENTER, true};
extern const GPIO_BUTTON::CONFIG rightConfig = {GPIOA, GPIOA_BUTTON_RIGHT, true};
extern const GPIO_BUTTON::CONFIG downConfig = {GPIOB, GPIOB_BUTTON_DOWN, true};