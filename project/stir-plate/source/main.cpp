#include <new>

#include "button-task.h"
#include "constant-drive-strategy.h"
#include "constant-rpm-strategy.h"
#include "fan-command.h"
#include "fan-control.h"
#include "global-instance.h"
#include "gpio-button.h"
#include "hal.h"
#include "internal/button-observers.h"
#include "internal/chibios-shell-stream.h"
#include "internal/ugfx-display.h"
#include "shell.h"

namespace {

/// An enumeration of task priorities. Due to how FreeRTOS works the values at the top of this
/// enumeration (lower value) will have lower priority.
enum class TASK_PRIORITY : UBaseType_t {
  IDLE = tskIDLE_PRIORITY,
  SHELL,
  DISPLAY,
  FAN,
  BUTTON,

  HIGHEST,
  MAX = configMAX_PRIORITIES,
};
static_assert(TASK_PRIORITY::HIGHEST <= TASK_PRIORITY::MAX,
    "Too many task priorities used given the current kernel configuration.");

constexpr UBaseType_t priorityToSchedulerLevel(TASK_PRIORITY p)
{
  return static_cast<std::underlying_type_t<TASK_PRIORITY>>(p);
}

GLOBAL_INSTANCE<UGFX_DISPLAY> displayTask;

GLOBAL_INSTANCE<CHIBIOS_SHELL_STREAM> stream;
GLOBAL_INSTANCE<SHELL> shell;
GLOBAL_INSTANCE<FAN_COMMAND> fanCommand;

GLOBAL_INSTANCE<FAN_CONTROL> fanControl;
GLOBAL_INSTANCE<CONSTANT_DRIVE_FAN_CONTROL_STRATEGY> constantDriveStrategy;
GLOBAL_INSTANCE<CONSTANT_RPM_FAN_CONTROL_STRATEGY> constantRpmStrategy;

GLOBAL_INSTANCE<BUTTON_TASK> buttonTask;
GLOBAL_INSTANCE<UP_BUTTON_OBSERVER> upButtonObserver;
GLOBAL_INSTANCE<LEFT_BUTTON_OBSERVER> leftButtonObserver;
GLOBAL_INSTANCE<CENTER_BUTTON_OBSERVER> centerButtonObserver;
GLOBAL_INSTANCE<RIGHT_BUTTON_OBSERVER> rightButtonObserver;
GLOBAL_INSTANCE<DOWN_BUTTON_OBSERVER> downButtonObserver;
GLOBAL_INSTANCE<GPIO_BUTTON> upButton;
GLOBAL_INSTANCE<GPIO_BUTTON> leftButton;
GLOBAL_INSTANCE<GPIO_BUTTON> centerButton;
GLOBAL_INSTANCE<GPIO_BUTTON> rightButton;
GLOBAL_INSTANCE<GPIO_BUTTON> downButton;

}  // namespace

extern const SPIConfig displayConfig;
extern const SerialConfig shellConfig;
extern const GPIO_BUTTON::CONFIG upConfig;
extern const GPIO_BUTTON::CONFIG leftConfig;
extern const GPIO_BUTTON::CONFIG centerConfig;
extern const GPIO_BUTTON::CONFIG rightConfig;
extern const GPIO_BUTTON::CONFIG downConfig;

int main()
{
  halInit();
  spiStart(&SPID1, &displayConfig);
  sdStart(&SD2, &shellConfig);

  fanControl.create(priorityToSchedulerLevel(TASK_PRIORITY::FAN), PWMD1, ICUD2);
  constantDriveStrategy.create();
  constantRpmStrategy.create();
  fanControl.get().setControlStrategy(std::addressof(constantDriveStrategy.get()));

  displayTask.create(priorityToSchedulerLevel(TASK_PRIORITY::DISPLAY), fanControl.get(),
      constantDriveStrategy.get(), constantRpmStrategy.get());

  buttonTask.create(priorityToSchedulerLevel(TASK_PRIORITY::BUTTON));

  upButtonObserver.create(displayTask.get());
  upButton.create(upConfig, upButtonObserver.get(), buttonTask.get());

  leftButtonObserver.create(displayTask.get());
  leftButton.create(leftConfig, leftButtonObserver.get(), buttonTask.get());

  centerButtonObserver.create(displayTask.get());
  centerButton.create(centerConfig, centerButtonObserver.get(), buttonTask.get());

  rightButtonObserver.create(displayTask.get());
  rightButton.create(rightConfig, rightButtonObserver.get(), buttonTask.get());

  downButtonObserver.create(displayTask.get());
  downButton.create(downConfig, downButtonObserver.get(), buttonTask.get());

  stream.create(reinterpret_cast<BaseAsynchronousChannel &>(SD2));
  shell.create(priorityToSchedulerLevel(TASK_PRIORITY::SHELL), stream.get());
  fanCommand.create(
      shell.get(), fanControl.get(), constantDriveStrategy.get(), constantRpmStrategy.get());

  osalSysEnable();
}
