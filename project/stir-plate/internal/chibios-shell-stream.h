#pragma once

#include "hal.h"
#include "shell-stream-intf.h"
#include "task-utility.h"

/**
 * @brief Implementation of the shell stream interface wrapping a ChibiOS HAL Base Asynchronous
 * channel.
 */
class CHIBIOS_SHELL_STREAM final : public SHELL_STREAM_INTF {
 public:
  explicit CHIBIOS_SHELL_STREAM(BaseAsynchronousChannel& channel) : my_channel(channel)
  {
  }

  size_t read(collection::span<std::byte> buffer) override
  {
    size_t bytesRead = 0;
    while (bytesRead == 0) {
      bytesRead = chnReadTimeout(std::addressof(this->my_channel),
          reinterpret_cast<uint8_t*>(buffer.data()), buffer.size(), BLOCKING_TIME.count());
    }
    return bytesRead;
  }

  size_t write(collection::span<const std::byte> buffer) override
  {
    return streamWrite(std::addressof(this->my_channel),
        reinterpret_cast<uint8_t const*>(buffer.data()), buffer.size());
  }

  void flush() override
  {
  }

 private:
  static constexpr TASK_TICK_DURATION BLOCKING_TIME = std::chrono::milliseconds{10};

  BaseAsynchronousChannel& my_channel;
};
