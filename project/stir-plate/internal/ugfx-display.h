#pragma once

#include <type_traits>

#include "task-utility.h"

enum class BUTTON_EVENT;
class FAN_CONTROL;
class CONSTANT_DRIVE_FAN_CONTROL_STRATEGY;
class CONSTANT_RPM_FAN_CONTROL_STRATEGY;

class UGFX_DISPLAY final {
 public:
  UGFX_DISPLAY(UBaseType_t taskPriority, FAN_CONTROL &fanControl,
      CONSTANT_DRIVE_FAN_CONTROL_STRATEGY &constantDriveStrategy,
      CONSTANT_RPM_FAN_CONTROL_STRATEGY &constantRpmStrategy);

  void onButtonEvent(BUTTON_EVENT event);

 private:
  enum class DISPLAY_EVENT { BUTTON = 0, REFRESH };

  struct QUEUED_DISPLAY_EVENT {
    DISPLAY_EVENT event;
    std::aligned_union_t<0, BUTTON_EVENT> data;
  };

  static constexpr UBaseType_t QUEUE_MAX_EVENTS = 10;

  /// Worker function for this task
  void workerTask();

  /// Function called when the display refresh timer expires.
  void onRefreshTimerExpired();

  STATIC_TASK<1024> my_task;

  /// Worker function deferred processing queue
  STATIC_QUEUE<QUEUED_DISPLAY_EVENT, QUEUE_MAX_EVENTS> my_event_queue;

  /// Local buffer for placing the event currently being processed by the task. Placed in class so
  /// RAM is allocated with the task rather than on the stack.
  QUEUED_DISPLAY_EVENT my_processing_event;

  /// Timer used to trigger a refresh of the display.
  STATIC_TIMER my_refresh_timer;

  /// Local buffer for building strings for the display.
  char my_string_scratch_buffer[32];

  /// Reference to the fan control object who's status is printed to the display.
  FAN_CONTROL &my_fan_control;

  /// Reference to the constant drive fan control strategy
  CONSTANT_DRIVE_FAN_CONTROL_STRATEGY &my_constant_drive_fan_control_strategy;

  /// Reference to the constant rpm fan control strategy
  CONSTANT_RPM_FAN_CONTROL_STRATEGY &my_constant_rpm_fan_control_strategy;
};
