#pragma once

#include "shell-command.h"

class FAN_CONTROL;
class CONSTANT_DRIVE_FAN_CONTROL_STRATEGY;
class CONSTANT_RPM_FAN_CONTROL_STRATEGY;

class FAN_COMMAND final : public SHELL_COMMAND_INTF {
 public:
  explicit FAN_COMMAND(SHELL &shell, FAN_CONTROL &fanController,
      CONSTANT_DRIVE_FAN_CONTROL_STRATEGY &constantDriveStrategy,
      CONSTANT_RPM_FAN_CONTROL_STRATEGY &constantRpmStrategy);
  ~FAN_COMMAND() = default;

  void handle(SHELL const &shell, collection::span<const std::string_view> args) override;
  std::string_view name() const override;

 private:
  static void printUsage(std::string_view command, SHELL const &shell);

  FAN_CONTROL &my_fan_controller;

  /// Reference to the constant drive fan control strategy
  CONSTANT_DRIVE_FAN_CONTROL_STRATEGY &my_constant_drive_fan_control_strategy;

  /// Reference to the constant rpm fan control strategy
  CONSTANT_RPM_FAN_CONTROL_STRATEGY &my_constant_rpm_fan_control_strategy;
};
