#pragma once

#include <algorithm>
#include <atomic>
#include <chrono>

#include "etl/cumulative_moving_average.h"
#include "fan-control-types.h"
#include "hal.h"
#include "task-utility.h"

class FAN_CONTROL_STRATEGY_INTF;

/**
 * @brief Task for controlling & monitoring the state of the fan.
 */
class FAN_CONTROL final {
 public:
  FAN_CONTROL() = delete;

  /**
   * @brief Creates a new fan control task.
   * @param taskPriority The priority of the control task.
   * @param fanControlDriver The PWM driver used for controlling the speed of the fan.
   * @param fanFeedbackDriver The input capture driver used for monitoring the rotation speed of the
   * @param powerEnableLine The output line that enables the boost supply for fan power.
   * fan.
   */
  explicit FAN_CONTROL(
      UBaseType_t taskPriority, PWMDriver& fanControlDriver, ICUDriver& fanFeedbackDriver);

  /**
   * @brief Gets the current RPM of the fan.
   * @return The current (instantaneous) rotation speed of the fan.
   * @todo Return as a QU16.16 instead?
   */
  uint16_t getCurrentRotationsPerMinute() const
  {
    auto rotationTime(FEEDBACK_TICK_DURATION{this->my_filtered_rotation_time.value()});
    return (rotationTime == FEEDBACK_TICK_DURATION::zero())
               ? 0
               : std::chrono::minutes{1} / rotationTime;
  }

  /**
   * @brief Sets the fan enable state.
   * @param enabled @c true will enable the fan output. @c false will disable the fan output.
   */
  void setEnable(bool enabled)
  {
    this->my_enabled.store(enabled, std::memory_order_relaxed);
  }

  /**
   * @brief Gets the fan state.
   * @return @c true if the fan is enabled. @c false otherwise.
   */
  bool getEnable() const
  {
    return this->my_enabled.load(std::memory_order_relaxed);
  }

  void setControlStrategy(FAN_CONTROL_STRATEGY_INTF* strategy)
  {
    this->my_control_strategy.store(strategy, std::memory_order_relaxed);
  }

  FAN_CONTROL_STRATEGY_INTF* getControlStrategy() const
  {
    return this->my_control_strategy.load(std::memory_order_relaxed);
  }

  /**
   * @brief Returns the current output drive strength as a percentage.
   * @return The current output drive on the range of [0, 100].
   */
  uint8_t getCurrentDriveStrength() const
  {
    return (CONTROL_TICK_DURATION(this->my_fan_control_driver.tim->CCR[CONTROL_CHANNEL]) * 100) /
           CONTROL_PWM_PERIOD;
  }

 private:
  /// The time between subsequent tick events.
  static constexpr TASK_TICK_DURATION TASK_PERIOD = std::chrono::milliseconds{100};

  /// Worker function for this task.
  void workerTask();

  /**
   * @brief Callback function called by the input capture driver on a pulse width capture.
   * @param timerCaptureValue The value captured by the driver.
   */
  void onWidthUpdate(uint32_t timerCaptureValue);

  /**
   * @brief Callback function called by the input capture driver on a pulse period capture.
   * @param timerCaptureValue The value captured by the driver.
   */
  void onPeriodUpdate(uint32_t timerCaptureValue);

  /**
   * @brief Callback function called by the input capture driver if a timer overflow occurs.
   */
  void onOverflowUpdate();

  /// Class-local storage of task state provided to the OS.
  STATIC_TASK<256> my_task_storage;

  /// Tracks if the output should be enabled.
  std::atomic<bool> my_enabled;

  /// The driver that controls the state of the fan.
  PWMDriver& my_fan_control_driver;

  /// The driver that monitors the state of the fan.
  ICUDriver& my_fan_feedback_driver;

  /// The filtered tachometer reading. The filtering helps remove error introduced into the reading
  /// by the rotating permanent magnets.
  etl::cumulative_moving_average<FEEDBACK_TICK_DURATION::rep, 8> my_filtered_rotation_time;

  std::atomic<FAN_CONTROL_STRATEGY_INTF*> my_control_strategy;
};
