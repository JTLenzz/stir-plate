#pragma once

#include <atomic>
#include <tuple>

#include "arm_math.h"
#include "fan-control-strategy-intf.h"
#include "task-utility.h"

class CONSTANT_RPM_FAN_CONTROL_STRATEGY final : public FAN_CONTROL_STRATEGY_INTF {
 public:
  CONSTANT_RPM_FAN_CONTROL_STRATEGY();
  ~CONSTANT_RPM_FAN_CONTROL_STRATEGY() = default;

  CONSTANT_RPM_FAN_CONTROL_STRATEGY(CONSTANT_RPM_FAN_CONTROL_STRATEGY const&) = delete;

  CONTROL_TICK_DURATION calculateFanSpeed(FAN_CONTROL const& controller) override;

  /**
   * @brief Sets the target RPM to be maintained by this strategy
   */
  void setTargetRotationsPerMinute(uint16_t rpm);

  /**
   * @brief Gets the current target RPM.
   */
  uint16_t getTargetRotationsPerMinute() const;

  /**
   * @brief Sets the PID control loop gain parameters to the values provided.
   */
  void setFeedbackControllerParameters(
      float32_t proportionalGain, float32_t integralGain, float32_t derivativeGain);

  /**
   * @brief Gets the current PID control loop gain paramenters as a three element tuple of
   * [proportional, integral, derivative] gain values.
   */
  std::tuple<float32_t, float32_t, float32_t> getFeedbackControllerParameters() const;

 private:
  /// The goal control setpoint.
  std::atomic<uint16_t> my_target_rpm;

  /// Mutex protecting the feedback controller state
  STATIC_MUTEX my_feedback_controller_mutex;

  /// The feedback controller that calculates the next controller input value.
  arm_pid_instance_f32 my_feedback_controller;
};