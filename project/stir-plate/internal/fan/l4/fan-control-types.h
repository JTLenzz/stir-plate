#pragma once

#include <chrono>

#include "hal.h"

/// A duration time in units of the tick of the feedback timer.
using FEEDBACK_TICK_DURATION = std::chrono::duration<uint32_t, std::ratio<1, STM32_TIMCLK1>>;

/// A duration time in units of the tick of the control timer.
using CONTROL_TICK_DURATION = std::chrono::duration<uint32_t, std::ratio<1, STM32_TIMCLK2>>;

/// The overflow period of the control timer. Period is chosen for a 25 kHz PWM frequency.
static constexpr CONTROL_TICK_DURATION CONTROL_PWM_PERIOD = std::chrono::microseconds{40};

/// The minimum control PWM pulse count.
static constexpr CONTROL_TICK_DURATION MINIMUM_PWM_PULSE_WIDTH = CONTROL_TICK_DURATION::zero();

/// The maximum control PWM pulse count.
static constexpr CONTROL_TICK_DURATION MAXIMUM_PWM_PULSE_WIDTH = CONTROL_PWM_PERIOD;

/// Initial hardware configuration for the control timer.
static constexpr PWMConfig CONTROL_CONFIG = {.frequency = STM32_TIMCLK2,
    .period = CONTROL_PWM_PERIOD.count(),
    .callback = nullptr,
    .channels = {{PWM_OUTPUT_DISABLED, nullptr}, {PWM_OUTPUT_ACTIVE_HIGH, nullptr},
        {PWM_OUTPUT_DISABLED, nullptr}, {PWM_OUTPUT_DISABLED, nullptr},
        {PWM_OUTPUT_DISABLED, nullptr}, {PWM_OUTPUT_DISABLED, nullptr}},
    .cr2 = 0,
    .bdtr = 0,
    .dier = 0};

/// Channel used for fan control. Note that this is the offset into the PWM config structure
/// (0-based), not the actual timer channel (1-based).
static constexpr pwmchannel_t CONTROL_CHANNEL = 1;
