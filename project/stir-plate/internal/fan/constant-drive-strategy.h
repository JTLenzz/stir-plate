#pragma once

#include <atomic>

#include "fan-control-strategy-intf.h"

class CONSTANT_DRIVE_FAN_CONTROL_STRATEGY final : public FAN_CONTROL_STRATEGY_INTF {
 public:
  CONSTANT_DRIVE_FAN_CONTROL_STRATEGY();
  ~CONSTANT_DRIVE_FAN_CONTROL_STRATEGY() = default;

  CONSTANT_DRIVE_FAN_CONTROL_STRATEGY(CONSTANT_DRIVE_FAN_CONTROL_STRATEGY const&) = delete;

  CONTROL_TICK_DURATION calculateFanSpeed(FAN_CONTROL const& controller) override;

  /**
   * @brief Sets the output control value with the provided fraction.
   * @param numerator Full drive fraction numerator.
   * @param denominator Full driver fraction denominator.
   */
  void setTargetDriveStrength(uint16_t numerator, uint16_t denominator);

  /**
   * @brief Gets the current target drive strength on the scale [0, 100].
   */
  uint8_t getTargetDriveStrength() const;

 private:
  std::atomic<CONTROL_TICK_DURATION> my_target_pwm;
};