#pragma once

#include "fan-control-types.h"

class FAN_CONTROL;

class FAN_CONTROL_STRATEGY_INTF {
 public:
  /**
   * @brief Calculate an updated fan speed.
   * @param controller Reference to the calling controller used to get current fan state.
   * @return The next PWM pulse width value that should be used to drive the fan. Should be bounded
   * between @c MINIMUM_PWM_PULSE_WIDTH & @c MAXIMUM_PWM_PULSE_WIDTH.
   */
  virtual CONTROL_TICK_DURATION calculateFanSpeed(FAN_CONTROL const& controller) = 0;
};
