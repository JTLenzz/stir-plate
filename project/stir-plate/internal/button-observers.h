#pragma once

#include "button-observer-intf.h"
#include "internal/ugfx-display.h"

/// Enumeration of all button events that are published to the display task
enum class BUTTON_EVENT {
  UP_PRESS = 0,
  UP_HOLD,
  UP_RELEASE,
  LEFT_PRESS,
  LEFT_HOLD,
  LEFT_RELEASE,
  CENTER_PRESS,
  CENTER_HOLD,
  CENTER_RELEASE,
  RIGHT_PRESS,
  RIGHT_HOLD,
  RIGHT_RELEASE,
  DOWN_PRESS,
  DOWN_HOLD,
  DOWN_RELEASE
};

/**
 * @brief Button observer implementation that serves the display task.
 * @tparam PRESS_EVENT the button event to be pushed to the display task on press.
 * @tparam HOLD_EVENT the button event to be pushed to the display task on hold(s).
 * @tparam RELEASE_EVENT the button event to be pushed to the display task on release.
 */
template <BUTTON_EVENT PRESS_EVENT, BUTTON_EVENT HOLD_EVENT, BUTTON_EVENT RELEASE_EVENT>
class DISPLAY_BUTTON_OBSERVER final : public BUTTON_OBSERVER_INTF {
 public:
  DISPLAY_BUTTON_OBSERVER() = delete;

  /**
   * @brief Creates a new button observer for the provided display task.
   * @param display The display task to push button event to on button notifications.
   */
  explicit DISPLAY_BUTTON_OBSERVER(UGFX_DISPLAY& display)
      : BUTTON_OBSERVER_INTF(), my_display(display)
  {
    // This function intentionally left blank.
  }

  ~DISPLAY_BUTTON_OBSERVER() = default;

  void onPress(BUTTON_INTF* /* button */) override
  {
    this->my_display.onButtonEvent(PRESS_EVENT);
  }

  void onHold(BUTTON_INTF* /* button */) override
  {
    this->my_display.onButtonEvent(HOLD_EVENT);
  }

  void onRelease(BUTTON_INTF* /* button */) override
  {
    this->my_display.onButtonEvent(RELEASE_EVENT);
  }

 private:
  /// The display task where button notifications are sent to.
  UGFX_DISPLAY& my_display;
};

/// Helper type for observing the "up" button.
using UP_BUTTON_OBSERVER = DISPLAY_BUTTON_OBSERVER<BUTTON_EVENT::UP_PRESS, BUTTON_EVENT::UP_HOLD,
    BUTTON_EVENT::UP_RELEASE>;

/// Helper type for observing the "left" button.
using LEFT_BUTTON_OBSERVER = DISPLAY_BUTTON_OBSERVER<BUTTON_EVENT::LEFT_PRESS,
    BUTTON_EVENT::LEFT_HOLD, BUTTON_EVENT::LEFT_RELEASE>;

/// Helper type for observing the "center" button.
using CENTER_BUTTON_OBSERVER = DISPLAY_BUTTON_OBSERVER<BUTTON_EVENT::CENTER_PRESS,
    BUTTON_EVENT::CENTER_HOLD, BUTTON_EVENT::CENTER_RELEASE>;

/// Helper type for observing the "right" button.
using RIGHT_BUTTON_OBSERVER = DISPLAY_BUTTON_OBSERVER<BUTTON_EVENT::RIGHT_PRESS,
    BUTTON_EVENT::RIGHT_HOLD, BUTTON_EVENT::RIGHT_RELEASE>;

/// Helper type for observing the "down" button.
using DOWN_BUTTON_OBSERVER = DISPLAY_BUTTON_OBSERVER<BUTTON_EVENT::DOWN_PRESS,
    BUTTON_EVENT::DOWN_HOLD, BUTTON_EVENT::DOWN_RELEASE>;
